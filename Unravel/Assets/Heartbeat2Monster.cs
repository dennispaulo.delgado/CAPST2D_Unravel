﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heartbeat2Monster : MonoBehaviour
{
    float distance;
    float distance2;
    public Enemy enemy1;
    public Enemy enemy2;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(enemy1.transform.position, transform.position);
        distance2 = Vector3.Distance(enemy2.transform.position, transform.position);

        if(distance < 70 || distance2 < 70)
        {
            audioSource.volume += Time.deltaTime * .3f;
        }
        else
        {
            audioSource.volume -= Time.deltaTime * .3f;
        }
    }
}
