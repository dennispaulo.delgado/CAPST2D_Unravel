﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChanger : MonoBehaviour
{
    public Animator animator;
    bool FadeIn = false;

    // Start is called before the first frame update
    void Start()
    {
        SimpleChangeScene.OnFadeIn += SimpleChangeScene_OnFadeIn;
    }

    // Update is called once per frame
    void Update()
    {
        FadeToLevel();
    }

    void SimpleChangeScene_OnFadeIn(object sender, EventArgs e)
    {
        FadeIn = true;
    }

    public void FadeToLevel()
    {
        animator.SetBool("FadeOut", FadeIn);
    }
}
