﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class DialogueFadeIn : MonoBehaviour
{
    [SerializeField] TextMeshPro dialogueMesh;
    bool textActive;

    private void OnEnable()
    {
        textActive = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (textActive)
        {
            
        }
    }
}
