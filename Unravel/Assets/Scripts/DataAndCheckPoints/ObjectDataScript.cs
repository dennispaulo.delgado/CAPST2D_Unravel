﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectDataScript : MonoBehaviour
{
    private DataTypeObject dtObject;
    private int instanceC = 1;
    private float counter = 2;

    void Start()
    {
        
    }
    /*
    private void Start()
    {
        if(SaveManager.instance.stageData.playerHasTravelGuideKey)
        {
            Destroy(this.gameObject);
        }
    }*/
    private void Update()
    {
        if (counter <= 0)
        {
            if (instanceC == 1)
            {
                CheckObjectData();
                instanceC = 0;
            }
        }
        else
        {
            counter -= Time.deltaTime;
        }
    }


    public void ObjectCollected()
    {
        dtObject.hasBeenCollected = true;
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManagerScript.instance.ReturnStageData().stageName)
            {
                for (int o = 0; o < SaveManager.instance.playerData.stageData[i].dObject.Count; o++)
                {
                    if (SaveManager.instance.playerData.stageData[i].dObject[o].oName == this.gameObject.name)
                    {
                        SaveManager.instance.playerData.stageData[i].dObject[o] = dtObject;
                        
                    }
                }
            }
        }
    }

    public void CheckObjectData()
    {
        if (CheckIfHasData())
        {
        }
        else
        {

            dtObject = new DataTypeObject();
            dtObject.oName = this.gameObject.name;
            dtObject.hasBeenCollected = false;
            for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
            {
                if (SaveManager.instance.playerData.stageData[i].stageName == SceneManagerScript.instance.ReturnStageData().stageName)
                {
                    SaveManager.instance.playerData.stageData[i].dObject.Add(dtObject);
                }
            }
        }
        if (dtObject.hasBeenCollected)
        {
            Destroy(this.gameObject);
        }
    }

    public bool CheckIfHasData()
    {
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManagerScript.instance.ReturnStageData().stageName)
            {
                for (int o = 0; o < SaveManager.instance.playerData.stageData[i].dObject.Count; o++)
                {
                    if (SaveManager.instance.playerData.stageData[i].dObject[o].oName == this.gameObject.name)
                    {
                        dtObject = SaveManager.instance.playerData.stageData[i].dObject[o];
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
