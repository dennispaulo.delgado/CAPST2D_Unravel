﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private bool isUsed = false;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player"&&!isUsed)
        {
            GameEvents.instance.OnSave();
            isUsed = true;
            Debug.Log("Player PassThrough Checkpoint");
        }
    }
}
