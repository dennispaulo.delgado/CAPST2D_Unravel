﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;


    public PlayerSaveData playerData;
    public StageSaveData stageData;
    public DaySequenceSaveData daySequenceData;

    public bool hasLoaded;

    public bool hasData = false;
    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //Load();
    }

    void Start()
    {
    }

    public void NewGame()
    {
        Debug.Log("NewGame");
        hasData = false;
        playerData = new PlayerSaveData();
        stageData = new StageSaveData();
        daySequenceData = new DaySequenceSaveData();
        ClearListeners();
    }

    void Update()
    {
        /*
        if(Input.GetKeyDown(KeyCode.Keypad7))
        {
            Save();
        }

        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            Load();
        }

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            DeleteSave();
        }*/
    }

    public void Save()
    {
        /*
        string dataPath = Application.persistentDataPath;
        
        var serializer = new XmlSerializer(typeof(SaveData));
        var stream = new FileStream(dataPath + "/" + activeSave.saveName + ".save", FileMode.Create);
        serializer.Serialize(stream, activeSave);
        stream.Close();
        */
        //Debug.Log("SAved Int Count"+ activeSave.stageData[0].dPlayer.sItemList.Count);
        GameEvents.instance.OnSave();
        //Debug.Log("Saved");

    }
    public void Load()
    {
        GameEvents.instance.OnLoad();
        /*
        string dataPath = Application.persistentDataPath;

        if(System.IO.File.Exists(dataPath + "/" + activeSave.saveName + ".save"))
        {
            var serializer = new XmlSerializer(typeof(SaveData));
            var stream = new FileStream(dataPath + "/" + activeSave.saveName + ".save", FileMode.Open);
            activeSave = serializer.Deserialize(stream) as SaveData;
            stream.Close();

            //Debug.Log("Loaded Inventory" + activeSave.stageData[0].dPlayer.sItemList.Count);
            Debug.Log("Loaded");
            hasLoaded = true;
        }*/


    }

    public void ClearListeners()
    {

        GameEvents.instance.ClearListeners();
    }

    public void DeleteSave()
    {
        /*
        string dataPath = Application.persistentDataPath;

        if (System.IO.File.Exists(dataPath + "/" + activeSave.saveName + ".save"))
        {
            File.Delete(dataPath + "/" + activeSave.saveName + ".save");
            Debug.Log("Deleted");
        }
        */

    }
}


[System.Serializable]
public class PlayerSaveData
{
    public Vector3 lastSavePostion;
    public Vector3 lastSaveRotation;

    public bool loadAllData = false;

    public List<Item> itemList = new List<Item>();
    public List<PlayerNotes> playerNotes = new List<PlayerNotes>();
    public List<CharacterInfo> characterInfo = new List<CharacterInfo>();
    public List<int> characterIDs = new List<int>();
    public List<Objective> playerObjective = new List<Objective>();
    public List<int> manananggalSObsIDs= new List<int>();
    public List<int> aswangSObsIDs = new List<int>();
    public List<int> mangkukulamSObsIDs = new List<int>();
    public List<int> monsterDiscoverIDs = new List<int>();
    //
    public List<StageData> stageData = new List<StageData>();
}

[System.Serializable]
public class StageSaveData
{
    public int numberOfDays = 0;

    //New
    public bool playerHasTravelGuideKey = false;
    public bool playerHasJuanHouseKey = false;
    public bool playerStopTheManananggal = false;
    public bool playerStopAswang = false;
    public bool playerStopMangkukulam = false;
    public bool hasTheIncompleteAmulet = false;
    public bool hasTheCompleteAmulet = false;
}

[System.Serializable]
public class DaySequenceSaveData
{
    public bool hasTalkedAboutMapTutorial = false;
    public bool talkedToGarcia = false;
    public bool talkedToMendoza = false;
    public bool checkedTravelHouse = false;
    public bool checkedJuansHouse = false;
    public bool firstTalkToTheChief = false;
    public bool manananggalMissionBriefing = false;
    public bool playerFinishManananggalLvl = false;
    public bool aswangMissionBriefing = false;
    public bool talkedAboutMangkukulam = false;
    public bool hasTalkedToDuwende = false;
}


/// <summary>
/// ////////////////////////////////////////////////////////////////////////////////////
/// </summary>

[System.Serializable]
public class StageData
{
    public string stageName;

    public List<DataTypeObject> dObject = new List<DataTypeObject>();
    public List<PuzzleData> dPuzzle = new List<PuzzleData>();
}


[System.Serializable]
public class DataTypeNPC
{
    public string nName;
    public int dialIndex;
    public Vector3 npcPosition;
}

[System.Serializable]
public class DataTypeObject
{
    public string oName;
    public bool hasBeenCollected;

}
[System.Serializable]
public class PuzzleData
{
    public string pName;
    public bool hasBeenSolved = false;
}
public enum TypeOfData
{
    Player,
    NPC,
    Object
}

