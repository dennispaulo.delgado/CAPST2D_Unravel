﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderForRadar : MonoBehaviour
{
    public Transform parent;
    public float radarYPosition;

    public GameObject spawnWhenDetected;
    public float timerOfSpawn;
    void Start()
    {
        parent = transform.parent;
    }

    void LateUpdate()
    {
        transform.position = new Vector3(parent.position.x, radarYPosition, parent.position.z);
    }

    public void PingMe()
    {
        GameObject ping = Instantiate(spawnWhenDetected, this.transform.position, this.spawnWhenDetected.transform.rotation);
        StartCoroutine("DeletePing", ping);
    }

    IEnumerator DeletePing(GameObject ping)
    {
        yield return new WaitForSeconds(timerOfSpawn);
        Destroy(ping);
    }
}
