﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour
{
    [Header("Radar Parts(Required)")]
    public Transform sweepTransform;
    public Transform radarBackground;
    public Transform radarTrail;
    public GameObject radarUI;

    private List<Collider> colliders;
    [Header("Radar Data(Required)")]
    public LayerMask radarMask;
    public float size;
    public float rotationSpeed;
    public float raycastSize;

    private void Awake()
    {
        Referencer.Instance.radar = this;
        Referencer.Instance.radarUI = radarUI;
        radarUI.SetActive(false);
        radarBackground.transform.localScale = new Vector3(size, size, 1);
        sweepTransform.transform.localScale = new Vector3(size*15.88f, size, 1);
        radarTrail.transform.localScale = new Vector3(0.062504f, 1, 1);
        colliders = new List<Collider>();
        this.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        float previousRot = (sweepTransform.eulerAngles.y % 360) - 180;
        sweepTransform.eulerAngles -= new Vector3(0, 0, rotationSpeed * Time.deltaTime);
        float currentRot = (sweepTransform.eulerAngles.y % 360) - 180;

        if(previousRot<0 && currentRot >= 0)
        {
            GameEvents.instance.RadarMakesNoise();
            colliders.Clear();
        }

        RaycastHit[] hits = Physics.RaycastAll(this.transform.position, sweepTransform.right, size * raycastSize, radarMask);
        Debug.DrawRay(this.transform.position, sweepTransform.right * size * raycastSize, Color.yellow);
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider)
            {
                if (!colliders.Contains(hit.collider))
                {
                    colliders.Add(hit.collider);
                    hit.collider.gameObject.GetComponent<ColliderForRadar>().PingMe();
                }
            }
        }
    }

}
