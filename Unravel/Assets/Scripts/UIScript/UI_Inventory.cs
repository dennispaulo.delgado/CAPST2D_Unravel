﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using TMPro;

public class UI_Inventory : MonoBehaviour
{
    private Inventory inventory;
    public Transform itemSlotContainer;
    public Transform itemSlotTemplate;
    public Transform itemDescription;
    private Transform btnTransform;
    private CollectBehaviour player;

    private bool isOpen = false;



    private void Awake()
    {
        //itemSlotContainer = transform.Find("itemSlotContainer");
        //itemSlotTemplate = itemSlotContainer.Find("itemSlotTemplate");
        itemDescription.GetComponent<TextMeshProUGUI>().SetText("");
        //Show();
        

    }
    private void Start()
    {
        MouseEnterExitEvents.OnMouseEnter += MouseEnterExitEvents_OnMouseEnter;
        MouseEnterExitEvents.OnMouseExit += MouseEnterExitEvents_OnMouseExit;
        GameEvents.instance.clearListeners += ClearListeners;
    }

    private void MouseEnterExitEvents_OnMouseEnter(object sender, EventArgs e)
    {
        Debug.Log("on mouse enter");
        foreach (Item item in inventory.GetInventoryList())
        {
            //Debug.Log("Item " + item.interactable.nameString);
            //itemDescription.GetComponent<TextMeshProUGUI>().SetText(item.interactable.description);
        }
    }

    private void MouseEnterExitEvents_OnMouseExit(object sender, EventArgs e)
    {
        Debug.Log("on mouse exit");
        //itemDescription.GetComponent<TextMeshProUGUI>().SetText("");
    }

    public void ClearListeners()
    {
        MouseEnterExitEvents.OnMouseEnter -= MouseEnterExitEvents_OnMouseEnter;
        MouseEnterExitEvents.OnMouseExit -= MouseEnterExitEvents_OnMouseExit;
        GameEvents.instance.clearListeners -= ClearListeners;
    }


    public void SetPlayer(CollectBehaviour player)
    {
        btnTransform = itemSlotTemplate;
        this.player = player;
    }


    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
        inventory.OnItemListChanged += Inventory_OnItemListChanged;


        //RefreshInventoryItems();
    }


    void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }


    public void RefreshInventoryItems()
    {
        foreach (Transform child in itemSlotContainer)
        {
            //Debug.Log("Checking Template");
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }
        float sX = 109.1f;
        float sY = -17.4f;
        float x = 0f;
        float y = 0f;
        float itemSlotCellSize = 130.2f;

        foreach(Item item in inventory.GetInventoryList())
        {
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.GetComponent<ItemButton>().SetReference(item, this);
            itemSlotRectTransform.gameObject.SetActive(true);

           
            //MouseEnterExitEvents mouseEnterExitEvents = btnTransform.GetComponent<MouseEnterExitEvents>();
            //mouseEnterExitEvents.OnMouseEnter += (object sender, EventArgs e) =>
            //{
            //    Debug.Log("on mouse enter");
            //    itemDescription.GetComponent<TextMeshProUGUI>().SetText(item.interactable.description, true);
            //};
           
            //mouseEnterExitEvents.OnMouseExit += (object sender, EventArgs e) =>
            //{
            //    itemDescription.GetComponent<TextMeshProUGUI>().SetText("");
            //};

            itemSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () =>
            {
                //Use Item
                Debug.Log("Use Item!!");
                inventory.UseItem(item);
            };

            itemSlotRectTransform.GetComponent<Button_UI>().MouseRightClickFunc = () =>
            {
                //Drop item
                Item duplicateItem = new Item { interactable = item.interactable, amount = item.amount };
                inventory.RemoveItem(item);
                ItemWorld.DropItem(player.transform.position,duplicateItem);
            };
            
            itemSlotRectTransform.anchoredPosition = new Vector2((x * itemSlotCellSize)+sX, (-y * itemSlotCellSize)+sY);
            Image image = itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite = item.GetSprite();

            TextMeshProUGUI uiText = itemSlotRectTransform.Find("text").GetComponent<TextMeshProUGUI>();
            if(item.amount > 1)
            {
                uiText.SetText(item.amount.ToString());

            }
            else
            {
                uiText.SetText("");
            }



            x++;
            if (x > 3)
            {
                x = 0;
                y++;
            }
        }
    }

    public bool GetIsOpen()
    {
        return isOpen;
    }

    public void SetIsOpen(bool value)
    {
        isOpen = value;
    }


    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
