﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractableManager : MonoBehaviour
{
    public static InteractableManager Instance { get; private set; }

    private TextMeshProUGUI textMeshPro;


    private Transform player;
    private Transform interactable;
    private bool isActive = false;
    public GameObject indicatorTemplate;
    private List<GameObject> indicatorList = new List<GameObject>();

    private void Awake()
    {
        Instance = this;
        textMeshPro = transform.Find("text").GetComponent<TextMeshProUGUI>();

    }
    void Start()
    {
    }

    void Update()
    {
    }


    public void IndicatorIsNearest(Transform plyr, Transform objItrctbl)
    {
        if (CheckIfExistingInteractable(objItrctbl))
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    indicatorList[i].gameObject.GetComponent<InteractableIndicator>().InteractableIsNearest(plyr, objItrctbl);
                    return;
                }
            }
        }
        else
        {
            GameObject intInd = Instantiate(indicatorTemplate, this.transform);
            intInd.GetComponent<InteractableIndicator>().SetReferences(plyr, objItrctbl, this.gameObject.transform.parent.GetComponent<RectTransform>(),this);
            indicatorList.Add(intInd);
        }
    }

    public void RemoveIndicator(Transform plyr, Transform objItrctbl)
    {
        if (CheckIfExistingInteractable(objItrctbl))
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    indicatorList[i].gameObject.GetComponent<InteractableIndicator>().RemoveIndicator();
                    return;
                }
            }
        }
    }

    public void IndicatorInRange(Transform plyr, Transform objItrctbl)
    {
        if(CheckIfExistingInteractable(objItrctbl))
        {
            for (int i = 0; i < indicatorList.Count; i++)
            {
                if (indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
                {
                    indicatorList[i].gameObject.GetComponent<InteractableIndicator>().InteractableInRange(plyr,objItrctbl);
                    return;
                }
            }
        }
        else
        {
            GameObject intInd = Instantiate(indicatorTemplate, this.transform);
            intInd.GetComponent<InteractableIndicator>().SetReferences(plyr, objItrctbl, this.gameObject.transform.parent.GetComponent<RectTransform>(), this);
            indicatorList.Add(intInd);
        }
    }

    public bool CheckIfExistingInteractable(Transform objItrctbl)
    {
        for(int i = 0; i< indicatorList.Count;i++)
        {
            if(indicatorList[i].gameObject.GetComponent<InteractableIndicator>().ReferencedInteractable() == objItrctbl)
            {
                return true;
            }
        }
        return false;
    }

    public void RemoveIndicatorInList(GameObject indi)
    {
        for(int i = 0;i<indicatorList.Count;i++)
        {
            if(indi == indicatorList[i].gameObject)
            {
                indicatorList.RemoveAt(i);
                Destroy(indi);
                return;
            }
        }    
    }

    public List<GameObject> ReturnIndicatorList()
    {
        return indicatorList;
    }

    private void SetText(string itemTipText)
    {
        textMeshPro.SetText(itemTipText);
        textMeshPro.ForceMeshUpdate();

        Vector2 textSize = textMeshPro.GetRenderedValues(false);
        Vector2 padding = new Vector2(10,10);
    }

    public void Show(string itemTipText)
    {
        gameObject.SetActive(true);
        SetText(itemTipText);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }



}
