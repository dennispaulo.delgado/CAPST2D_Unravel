﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthStatusGameWorld : MonoBehaviour
{
    [SerializeField] private bool isActive = false;
    [SerializeField] private float fadeTimer = 5f;



    // Start is called before the first frame update
    void Start()
    {
        Invoke("DisableContainer", .5f);
        HealthScript.OnDamage += HealthScript_OnDamage;
        GameEvents.instance.clearListeners += ClearListeners;
    }



    // Update is called once per frame
    void Update()
    {
        
        if (isActive)
        {
            StartTimer();
        }
        
    }

    void HealthScript_OnDamage(object sender, EventArgs e) 
    {
        fadeTimer = 5f;
        Debug.Log("Show HealthStatus");
        if (!isActive)
        {
            gameObject.SetActive(true);
            isActive = true;

        }
    }

    private void StartTimer()
    {
        fadeTimer -= Time.deltaTime;

        if(fadeTimer <= 0)
        {
            gameObject.SetActive(false);
            isActive = false;
            fadeTimer = 5f;
        }
        
    }

    public void ClearListeners()
    {
        
        HealthScript.OnDamage -= HealthScript_OnDamage;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    private void DisableContainer()
    {
        gameObject.SetActive(false);
    }

}
