﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSelect : MonoBehaviour
{

    public List<GameObject> buttonsToChange;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnMouseEnterChange()
    {
        foreach(GameObject b in buttonsToChange)
        {
            b.SetActive(true);
        }
    }
    public void OnMouseExitChange()
    {
        foreach (GameObject b in buttonsToChange)
        {
            b.SetActive(false);
        }
    }

}
