﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingUI : MonoBehaviour
{
    public CraftingSystem craftSys;
    public ItemToCraft scroll;
    public Text sName;
    public Text sDescription;
    public List<GameObject> ingredientImage;

    public Sprite coal;
    public Sprite salt;
    public Sprite chalk;
    public Sprite oil;
    public Sprite herb;
    public Sprite alawig;

    private void Start()
    {
        if (craftSys == null)
        {
            Debug.LogError("Craft Sys not Referenced");
        }
        if (sName == null || sDescription == null)
        {
            Debug.LogError(this.gameObject.name + "Missing text reference");
        }
    }

    public void CheckIngredients()
    {
        sName.text = scroll.cName;
        sDescription.text = scroll.cDescription;
        //List<Item.ItemType> inventoryCopy = new List<Item.ItemType>();
        List<Item> itemListCopy = new List<Item>();
        /*
        for (int c = 0; c < craftSys.ReturnInventory().GetInventoryList().Count; c++)
        {
            inventoryCopy.Add(craftSys.ReturnInventory().GetInventoryList()[c].interactable.itemType);
        }*/
        for (int c = 0; c < craftSys.ReturnInventory().GetInventoryList().Count; c++)
        {
            itemListCopy.Add(craftSys.ReturnInventory().GetInventoryList()[c].ReturnDeepCopy());
        }
        /*
        for (int i = 0; i < scroll.ingredients.Count; i++)
        {
            ingredientImage[i].SetActive(true);//sets how many image needed

            switch (scroll.ingredients[i])//Adds image to image gameobject
            {
                case Item.ItemType.Ash:
                    ingredientImage[i].GetComponent<Image>().sprite = ash;
                    break;
                case Item.ItemType.Oil:
                    ingredientImage[i].GetComponent<Image>().sprite = oil;
                    break;
                case Item.ItemType.Salt:
                    ingredientImage[i].GetComponent<Image>().sprite = salt;
                    break;
                case Item.ItemType.Amulet:
                    ingredientImage[i].GetComponent<Image>().sprite = amulet;
                    break;
            }
            //
            if (HaveIngredient(scroll.ingredients[i], inventoryCopy))//checks if item is available
            {
                ingredientImage[i].GetComponent<Image>().color = new Vector4(0, 1, 0, 1);
            }//
            if (HaveIngredient(scroll.ingredients[i], itemListCopy,scroll.cIngredients[i]))//checks if item is available
            {
                ingredientImage[i].GetComponent<Image>().color = new Vector4(0, 1, 0, 1);
            }
            else
            {
                ingredientImage[i].GetComponent<Image>().color = new Vector4(1, 0, 0, 1);
            }
        }*/
        for (int i = 0; i < scroll.cIngredients.Count; i++)
        {
            ingredientImage[i].SetActive(true);//sets how many image needed

            switch (scroll.cIngredients[i].ingredient)//Adds image to image gameobject
            {
                case Item.ItemType.Coal:
                    ingredientImage[i].GetComponent<Image>().sprite = coal;
                    break;
                case Item.ItemType.BottleOfOil:
                    ingredientImage[i].GetComponent<Image>().sprite = oil;
                    break;
                case Item.ItemType.Salt:
                    ingredientImage[i].GetComponent<Image>().sprite = salt;
                    break;
                case Item.ItemType.AlawigEssence:
                    ingredientImage[i].GetComponent<Image>().sprite = alawig;
                    break;
                case Item.ItemType.Herb:
                    ingredientImage[i].GetComponent<Image>().sprite = herb;
                    break;
                case Item.ItemType.Chalk:
                    ingredientImage[i].GetComponent<Image>().sprite = chalk;
                    break;
            }
            if (HaveIngredient( itemListCopy, scroll.cIngredients[i]))//checks if item is available
            {
                ingredientImage[i].GetComponent<Image>().color = new Vector4(0, 1, 0, 1);
                ingredientImage[i].transform.GetChild(0).GetComponent<Text>().text = scroll.cIngredients[i].amount.ToString();
            }
            else
            {
                ingredientImage[i].GetComponent<Image>().color = new Vector4(1, 0, 0, 1);
                ingredientImage[i].transform.GetChild(0).GetComponent<Text>().text = scroll.cIngredients[i].amount.ToString();
            }
        }
    }
    public bool HaveIngredient( List<Item> itemList, CraftIngredients c)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (c.ingredient == itemList[i].interactable.itemType)
            {
                if (itemList[i].interactable.isStackable)
                {
                    if (itemList[i].amount >= c.amount)
                    {
                        itemList[i].amount -= c.amount;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    int counter = 0;
                    for (int z = 0; z < itemList.Count; z++)
                    {
                        if(c.ingredient == itemList[i].interactable.itemType)
                        {
                            counter++;
                        }
                    }
                    if(counter>=c.amount)
                    {
                        return true;
                    }
                }
            }
        }
        /*
        for (int i = 0; i < itemList.Count; i++)
        {
            if (lookFor == itemList[i].interactable.itemType)
            {
                if (itemList[i].amount > 1)
                {
                    itemList[i].amount--;
                    return true;
                }
                else
                {
                    itemList.Remove(itemList[i]);
                    return true;
                }
            }
        }*/
        return false;
    }


    public void UnCheckIngredients()
    {
        sName.text = "";
        sDescription.text = "";
        for (int i = 0; i < ingredientImage.Count; i++)
        {
            ingredientImage[i].SetActive(false);//turns off gameobjects
        }
    }

    public void CraftScroll()
    {
        craftSys.CraftItem(scroll.craftID);
    }
}
