﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseEnterExitEvents : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public class OnMouseEnterArgs: EventArgs
    {

    }

    public static event EventHandler OnMouseEnter;
    public static event EventHandler OnMouseExit;


    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("enter");
        OnMouseEnter?.Invoke(this, EventArgs.Empty);
        this.GetComponent<ItemButton>().OnButtonEnter();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("Exit");
        OnMouseExit?.Invoke(this, EventArgs.Empty);
        this.GetComponent<ItemButton>().OnButtonExit();
    }

}
