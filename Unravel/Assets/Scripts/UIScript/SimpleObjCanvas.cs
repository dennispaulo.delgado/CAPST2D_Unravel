﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleObjCanvas : MonoBehaviour
{
    public Text txt;
    void Start()
    {
        ObjectiveEvent.instance.updateSimpleOBJUI += UpdateObjGoal;
    }

    void Update()
    {
        
    }

    public void UpdateObjGoal(string s)
    {
        txt.text = s;
    }
}
