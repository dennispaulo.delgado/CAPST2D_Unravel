﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructionCanvas : MonoBehaviour
{
    public static InstructionCanvas instance;

    public Image image;
    public Text text;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        ResetInstructionCanvas();
    }

    public void PostInstruction(Vector2 imgLoc, Vector2 txtLoc, Sprite insImg, string insTxt)
    {
        image.gameObject.SetActive(true);
        text.gameObject.SetActive(true);
        text.gameObject.GetComponent<RectTransform>().anchoredPosition = txtLoc;
        image.gameObject.GetComponent<RectTransform>().anchoredPosition = imgLoc;
    }
    public void PostInstruction(Vector2 location, string insTxt)
    {
        image.gameObject.SetActive(false);
        text.gameObject.SetActive(true);
        text.text = insTxt;
        text.gameObject.GetComponent<RectTransform>().anchoredPosition = location;
    }
    public void PostInstruction(Vector2 location, Sprite insImg)
    {
        text.gameObject.SetActive(false);
        image.gameObject.SetActive(true);
        image.gameObject.GetComponent<Animator>().Play("Tutorial", -1, 0f);
        image.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(insImg.rect.width, insImg.rect.height);
        image.sprite = insImg;
        image.gameObject.GetComponent<RectTransform>().anchoredPosition = location;
    }

    public void ResetInstructionCanvas()
    {
        text.gameObject.SetActive(false);
        image.gameObject.SetActive(false);
    }
}
