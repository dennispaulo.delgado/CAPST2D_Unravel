﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoicesButtons : MonoBehaviour
{
    public static ChoicesButtons instance;

    public Button yesButton;
    public Button noButton;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void NoChoice()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        yesButton.onClick.RemoveAllListeners();
        noButton.onClick.RemoveAllListeners();
        this.gameObject.SetActive(false);
    }
}
