﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialInstruction : MonoBehaviour
{
    public Image instructionImage; 
    private bool isActive = false;

    void Start()
    {
        instructionImage.gameObject.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Mouse0))
        {
            DeactivateSpecialInstruction();
        }
    }

    public void ActivateSpecialInstruction()
    {
        if(!isActive)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            isActive = true;
            instructionImage.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void DeactivateSpecialInstruction()
    {
        if (isActive)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            isActive = false;
            instructionImage.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

}
