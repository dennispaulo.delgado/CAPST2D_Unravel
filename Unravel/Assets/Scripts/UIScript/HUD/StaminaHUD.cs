﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaHUD : MonoBehaviour
{
    public static StaminaHUD instance;

    public GameObject staminaBar;
    public Image stamina;
    public Image bar;

    public float showDuration;
    private float defShowDur;

    private ThirdPersonMovement thrdPrsnMvmt;

    private bool isShowing = false;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance!=this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        defShowDur = showDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if(isShowing)
        {
            if(showDuration>0)
            {
                staminaBar.SetActive(true);
                showDuration -= Time.deltaTime;
                if(thrdPrsnMvmt!=null)
                {
                    UpdateStaminaBar();
                }
            }
            else
            {
                staminaBar.SetActive(false);
                isShowing = false;
            }
        }
    }

    public void UpdateStaminaBar()
    {
        stamina.fillAmount = thrdPrsnMvmt.curStamina / thrdPrsnMvmt.maxStamina;
    }

    public void ShowStaminaBar(ThirdPersonMovement mvn)
    {
        isShowing = true;
        showDuration = defShowDur;
        thrdPrsnMvmt = mvn;
    }

}
