﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlowpipeHUD : MonoBehaviour
{
    public Image ammoSprite;
    public Text ammoCount;

    public void ChangeAmmoCount(float count)
    {
        ammoCount.text = count.ToString();
    }

    public void ChangeAmmoSprite(Sprite ammoS)
    {
        ammoSprite.sprite = ammoS;
    }
}
