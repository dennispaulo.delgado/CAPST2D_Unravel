﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public static GameOver instance;

    public GameObject overlay;
    public List<ButtonSelect> buttons;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        overlay.SetActive(false);
    }

    void Update()
    {
        
    }

    public void OnGameOver()
    {
        FadeScreen.instance.ClearFadeScreen();
        overlay.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 1;
    }

    public void OnRetry()
    {
        overlay.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        UnselectButtons();
        GameManager.instance.RespawnPlayer();
    }

    private void UnselectButtons()
    {
        foreach (ButtonSelect b in buttons)
        {
            b.OnMouseExitChange();
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
