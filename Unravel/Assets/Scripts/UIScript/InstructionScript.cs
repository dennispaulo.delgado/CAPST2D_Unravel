﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum InstructionType
{
    Text,
    Visual,
    Both
}

public class InstructionScript : MonoBehaviour
{
    public List<KeyCode> keysToDestroy;
    private bool isActive;
    public InstructionScript chainInstruction;
    [Header("New")]
    public Sprite instructionImage;
    [TextArea(10,20)]
    public string instructionText;
    public Vector2 imgLoc;
    public Vector2 txtLoc;
    public InstructionType iType;
    [Header("Extra")]
    public bool hasDuration = false;
    private void Start()
    {
        isActive = false;
    }
    void Update()
    {
        if (isActive)
        {
            foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(vKey))
                {
                    for (int i = 0; i < keysToDestroy.Count; i++)
                    {
                        if (vKey == keysToDestroy[i])
                        {
                            keysToDestroy.RemoveAt(i);
                        }
                    }
                }
            }
             
            if (keysToDestroy.Count <= 0)
            {
                if(chainInstruction!=null)
                {
                    chainInstruction.gameObject.SetActive(true);
                }
                InstructionCanvas.instance.ResetInstructionCanvas();
                //txtCanvas.SetActive(false);
                Destroy(this.gameObject);
            }
        }

    }

    public void DisplayInstructionText()
    {
        Debug.Log("DisplayText");
        InstructionCanvas.instance.PostInstruction(txtLoc, instructionText);
    }

    public void DisplayInstructionImage()
    {
        InstructionCanvas.instance.PostInstruction(imgLoc, instructionImage);
    }

    public void ActivateInstruction()
    {
        isActive = true;
        switch (iType)
        {
            case InstructionType.Text:
                DisplayInstructionText();
                break;
            case InstructionType.Visual:
                DisplayInstructionImage();
                break;
        }
        if (hasDuration)
        {
            StartCoroutine("ClearInstruction");
        }
    }

    public void MakeIsActive()
    {
        isActive = true;
    }
    public void DestroyInstruction()
    {
        //txtCanvas.SetActive(false);
        Destroy(this.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("Activated");
            ActivateInstruction();
            if(hasDuration)
            {
                StartCoroutine("ClearInstruction");
            }
        }
    }
    IEnumerator ClearInstruction()
    {
        yield return new WaitForSecondsRealtime(7f);
        InstructionCanvas.instance.ResetInstructionCanvas();
        Destroy(this.gameObject);
    }
}
