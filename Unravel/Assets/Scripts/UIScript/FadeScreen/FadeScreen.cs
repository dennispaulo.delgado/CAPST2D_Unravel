﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeScreen : MonoBehaviour
{
    public static FadeScreen instance;

    private float fadeDuration;
    private float defDuration;
    public Image fadeScreen;
    private float alp = 0;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void ClearFadeScreen()
    {
        StopAllCoroutines();
        fadeScreen.gameObject.SetActive(false);
    }

    public void StartFadeOutRoutine(float duration)
    {
        fadeDuration = duration;
        defDuration = duration;
        fadeScreen.gameObject.SetActive(true);
        fadeScreen.color = new Color(0,0,0,0);
        alp = 0;
        StartCoroutine("FadeOut");
    }
    public void StartFadeInRoutine(float duration)
    {
        fadeDuration = duration;
        defDuration = duration;
        fadeScreen.gameObject.SetActive(true);
        fadeScreen.color = new Color(0, 0, 0, 1);
        alp = 1;
        GameManager.instance.gameIsPaused = true;
        StartCoroutine("FadeIn");
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSecondsRealtime(Time.deltaTime);
        alp += Time.deltaTime / defDuration;
        Color col = new Color(0, 0, 0, alp);
        fadeScreen.color = col;
        if (fadeDuration <= 0)
        {
            fadeScreen.gameObject.SetActive(false);
        }
        else
        {
            fadeDuration -= Time.deltaTime;
            StartCoroutine("FadeOut");
        }
    }
    IEnumerator FadeIn()
    {
        yield return new WaitForSecondsRealtime(Time.deltaTime);
        alp -= Time.deltaTime / defDuration;
        Color col = new Color(0, 0, 0, alp);
        fadeScreen.color = col;
        if (fadeDuration <= 0)
        {
            GameManager.instance.gameIsPaused = false;
            fadeScreen.gameObject.SetActive(false);
        }
        else
        {
            fadeDuration -= Time.deltaTime;
            StartCoroutine("FadeIn");
        }
    }
}
