﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveJCanvas : MonoBehaviour
{
    public List<PlayerNotes> notes;
    private ObjectiveHandler handler;
    [Header("NeedReference")]
    public GameObject objButton;// prefab of note buttons;
    public GameObject objJournal; // reference of the whole note journl
    public Text objContent; // text reference of the content note
    public Text objTitle; // text reference of the note title
    public Transform objContainer; // reference of the scrollable container
    public GameObject isCompText;//reference tesxt that say completed.

    public bool isOpen;

    public List<GameObject> objectButList;
    void Start()
    {
        handler = ObjectiveHandler.instance;
        if (objButton == null || objJournal == null || objContent == null || objTitle == null || objContainer == null)
        {
            Debug.LogError("Objective Canvas is Missing Reference/s");
        }
        else
        {
            isOpen = false;
            //objJournal.SetActive(false);
        }
    }

    void Update()
    {
    }

    public void ShowOrCloseObjectiveJournal()
    {
        if (isOpen)
        {
            ClearObjectiveButList();
            CloseObjectiveCanvas();
        }
        else
        {
            //objJournal.SetActive(true);
            ViewPlayerNotes();
            isOpen = true;
        }
    }

    public void ViewPlayerNotes()
    {
        if (handler == null)
        {
            handler = ObjectiveHandler.instance;
        }

        if (handler.curObjectives.Count > 0)
        {
            objContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(objContainer.GetComponent<RectTransform>().sizeDelta.x, handler.curObjectives.Count * 75);
            for (int i = 0; i < handler.curObjectives.Count; i++)
            {
                GameObject gam = Instantiate(objButton, objContainer);
                gam.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * -75, 0);
                gam.GetComponent<ObjectiveButton>().title = handler.curObjectives[i].ObjectiveName; //send the title to display
                gam.GetComponent<ObjectiveButton>().content = handler.curObjectives[i].ObjectiveDescription;// send the note content
                gam.GetComponent<ObjectiveButton>().objectiveButTitleUI.text = handler.curObjectives[i].objID.ToString();

                gam.GetComponent<ObjectiveButton>().objectiveContentUI = objContent; // reference the text component
                gam.GetComponent<ObjectiveButton>().objectiveTitleUI = objTitle; // send title text reference
                gam.GetComponent<ObjectiveButton>().comp = isCompText;
                gam.GetComponent<ObjectiveButton>().isCompleted = handler.curObjectives[i].isCompleted;

                objectButList.Add(gam);
            }
        }
    }
    public void ClearObjectiveButList()
    {
        if (objectButList.Count > 0)
        {
            for (int d = 0; d < objectButList.Count; d++)
            {
                Destroy(objectButList[d].gameObject);
            }
            objectButList.Clear();
        }
    }
    public void CloseObjectiveCanvas()
    {
        isCompText.SetActive(false);
        objTitle.text = "";
        objContent.text = "";
        isOpen = false;
        //objJournal.SetActive(false);
    }

    public void ResetObjectiveCanvas()
    {
        ClearObjectiveButList();
        CloseObjectiveCanvas();
    }

}
