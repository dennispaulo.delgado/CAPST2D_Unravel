﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharButton : MonoBehaviour
{
    private LoreCanvas lCanvas;

    public Text charButTitle;
    private CharacterInfo pChar;

    public Sprite buttonNotClicked;
    public Sprite buttonClicked;

    public void SetCharReferences(LoreCanvas lc, CharacterInfo inf)
    {
        lCanvas = lc;
        pChar = inf;
    }

    public void OnClickedChar()
    {
        lCanvas.ChangeCharDisplay(pChar);
        lCanvas.ChangeButton(this.gameObject);
    }

    public void EditRectTransform()
    {
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(this.GetComponent<RectTransform>().sizeDelta.x, charButTitle.preferredHeight);
    }
}
