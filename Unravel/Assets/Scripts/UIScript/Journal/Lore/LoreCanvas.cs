﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StoryJournalType
{
    Lore = 0 ,
    Character = 1
};

public class LoreCanvas : MonoBehaviour
{
    private PlayersData pData;
    [Header("CharacterCollection")]
    public CharacterCollection characterCollection;
    [Header("Reference")]
    public bool isOpen = false;
    public GameObject storyPanelContainer;
    public StoryJournalType curType;
    [Header("Lore")]
    public GameObject loreThings;
    public Text loreTitle;
    public Text loreContent;
    public GameObject loreButtonTemplate;
    public List<GameObject> loresList;

    [Header("Character")]
    public GameObject characterThings;
    public Image charImage;
    public Text characterTitle;
    public Text characterContent;
    public GameObject charButtonTemplate;
    public List<GameObject> characterList;

    [Header("Buttons")]
    public Image logsButton;
    public Image characterButton;
    public Sprite logsSelected;
    public Sprite logsUnSelected;
    public Sprite characterSelected;
    public Sprite characterUnSelected;

    private void Awake()
    {
        pData = PlayersData.instance;
    }
    void Start()
    {
        isOpen = false;
    }

    public void ShowOrCloseLoreJournal()
    {
        if (isOpen)
        {
            ClearStoryButtons();
            CloseLoreCanvas();
            isOpen = false;
        }
        else
        {
            ViewLoreCanvas();
            isOpen = true;
        }
    }

    public void CloseLoreCanvas()
    {

    }

    public void ViewLoreCanvas()
    {
        ViewThisStoryJournal(StoryJournalType.Lore);
    }

    public void OnButtonClickView(int styTyp)
    {
        if((StoryJournalType)styTyp != curType)
        {
            ViewThisStoryJournal((StoryJournalType)styTyp);
        }
    }

    private void ViewThisStoryJournal(StoryJournalType type)
    {
        if(pData == null)
        {
            pData = PlayersData.instance;
        }
        ClearStoryButtons();
        switch (type)
        {
            case StoryJournalType.Lore:
                LogsButtonSelected();
                loreThings.SetActive(true);
                characterThings.SetActive(false);
                curType = StoryJournalType.Lore;
                if (pData.pNotes.Count > 0)
                {
                    float loreRectSize = 0;
                    for (int l = 0; l < pData.pNotes.Count; l++)
                    {
                        GameObject loreButton = Instantiate(loreButtonTemplate, storyPanelContainer.transform);
                        loresList.Add(loreButton);
                        loreButton.GetComponent<LoreButton>().SetLoreReferences(this, pData.pNotes[l]);
                        loreButton.GetComponent<LoreButton>().loreButTitle.text = pData.pNotes[l].noteButTitle;
                        loreButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, loreRectSize * -1);
                        loreButton.GetComponent<LoreButton>().EditRectTransform();
                        storyPanelContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(storyPanelContainer.GetComponent<RectTransform>().sizeDelta.x, loreRectSize);
                        loreRectSize += loreButton.GetComponent<RectTransform>().sizeDelta.y;
                    }
                }
                break;
            case StoryJournalType.Character:
                CharacterButtonSelected();
                loreThings.SetActive(false);
                characterThings.SetActive(true);
                curType = StoryJournalType.Character;
                if (pData.charIDs.Count > 0)
                {
                    float charRectSize = 0;
                    
                    for(int c = 0; c<pData.charIDs.Count;c++)
                    {
                        for(int l = 0; l< characterCollection.charInfoCollection.Count;l++)
                        {
                            if(characterCollection.charInfoCollection[l].charID == pData.charIDs[c])
                            {
                                GameObject charButton = Instantiate(charButtonTemplate, storyPanelContainer.transform);
                                characterList.Add(charButton);
                                charButton.GetComponent<CharButton>().SetCharReferences(this, characterCollection.charInfoCollection[l]);
                                charButton.GetComponent<CharButton>().charButTitle.text = characterCollection.charInfoCollection[l].charName;
                                charButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, charRectSize * -1);
                                charButton.GetComponent<CharButton>().EditRectTransform();
                                storyPanelContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(storyPanelContainer.GetComponent<RectTransform>().sizeDelta.x, charRectSize);
                                charRectSize += charButton.GetComponent<RectTransform>().sizeDelta.y;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void CharacterButtonSelected()
    {
        characterButton.sprite = characterSelected;
        logsButton.sprite = logsUnSelected;
    }
    private void LogsButtonSelected()
    {
        characterButton.sprite = characterUnSelected;
        logsButton.sprite = logsSelected;
    }

    public void ClearStoryButtons()
    {
        //Debug.Log("Clearing Buttons ");
        ResetText();
        if (loresList.Count > 0)
        {
            for (int d = 0; d < loresList.Count; d++)
            {
                Destroy(loresList[d].gameObject);
            }
            loresList.Clear();
        }
        if(characterList.Count>0)
        {
            for (int d = 0; d < characterList.Count; d++)
            {
                Destroy(characterList[d].gameObject);
            }
            characterList.Clear();
        }
    }

    private void ResetText()
    {
        loreTitle.text = "";
        loreContent.text = ""; 
        characterTitle.text = "";
        characterContent.text = "";
        charImage.sprite = null;
    }

    public void ChangeButton(GameObject button)
    {
        if (loresList.Count > 0)
        {
            for (int d = 0; d < loresList.Count; d++)
            {
                if(button == loresList[d])
                {
                    loresList[d].GetComponent<Image>().sprite = loresList[d].GetComponent<LoreButton>().buttonClicked;
                    loresList[d].GetComponent<LoreButton>().loreButTitle.color = Color.black;
                }
                else
                {
                    loresList[d].GetComponent<Image>().sprite = loresList[d].GetComponent<LoreButton>().buttonNotClicked;
                    loresList[d].GetComponent<LoreButton>().loreButTitle.color = Color.white;
                }
            }
        }
        if (characterList.Count > 0)
        {
            for (int d = 0; d < characterList.Count; d++)
            {
                if (button == characterList[d])
                {
                    characterList[d].GetComponent<Image>().sprite = characterList[d].GetComponent<CharButton>().buttonClicked;
                    characterList[d].GetComponent<CharButton>().charButTitle.color = Color.black;
                }
                else
                {
                    characterList[d].GetComponent<Image>().sprite = characterList[d].GetComponent<CharButton>().buttonNotClicked;
                    characterList[d].GetComponent<CharButton>().charButTitle.color = Color.white;
                }
            }
        }
    }

    public void ChangeLogDisplay(PlayerNotes dLog)
    {
        loreTitle.text = dLog.noteTitle;
        loreContent.text = dLog.noteContent;
    }
    public void ChangeCharDisplay(CharacterInfo cLog)
    {
        characterTitle.text = cLog.charName;
        characterContent.text = cLog.charDescription;
        charImage.sprite = cLog.charImage;
    }


}
