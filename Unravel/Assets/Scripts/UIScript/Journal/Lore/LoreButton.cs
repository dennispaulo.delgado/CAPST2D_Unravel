﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoreButton : MonoBehaviour
{
    private LoreCanvas lCanvas;

    public Text loreButTitle;
    private PlayerNotes pLogs;

    public Sprite buttonNotClicked;
    public Sprite buttonClicked;

    public void SetLoreReferences(LoreCanvas lc,PlayerNotes log)
    {
        lCanvas = lc;
        pLogs = log;
    }

    public void OnClickedLore()
    {
        lCanvas.ChangeLogDisplay(pLogs);
        lCanvas.ChangeButton(this.gameObject);
    }

    public void EditRectTransform()
    {
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(this.GetComponent<RectTransform>().sizeDelta.x, loreButTitle.preferredHeight);
    }
}
