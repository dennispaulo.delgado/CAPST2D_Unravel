﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfoGiver : MonoBehaviour
{
    public int charID;

    public void GiveCharacterInfo()
    {
        PlayersData.instance.AddCharacterProfile(charID);
    }
    private void OnTriggerEnter(Collider other)
    {
        GiveCharacterInfo();
        this.gameObject.SetActive(false);
    }
}
