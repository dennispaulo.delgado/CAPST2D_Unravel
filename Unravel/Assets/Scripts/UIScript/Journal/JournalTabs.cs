﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalTabs : MonoBehaviour
{
    public Image image;
    public Sprite tabSelected;
    public Sprite tabUnselected;
    public Sprite tabPointeds;

    public Vector2 originalPosition;
    public Vector2 offset;


    private void Start()
    {
        image = this.GetComponent<Image>();
    }

    public void ButtonSelected()
    {
        this.GetComponent<RectTransform>().anchoredPosition = originalPosition + offset;
        image.sprite = tabSelected;
    }
    public void ButtonUnselected()
    {
        this.GetComponent<RectTransform>().anchoredPosition = originalPosition;
        image.sprite = tabUnselected;
    }

}
