﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AswObsButton : MonoBehaviour
{
    public Text obsTitleUI;

    [HideInInspector] public Text obsContentUI;
    [HideInInspector] public string content;

    public void OnButtonClick()
    {
        obsContentUI.text = content;
    }
}
