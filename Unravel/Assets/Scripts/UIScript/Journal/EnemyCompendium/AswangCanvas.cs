﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AswangCanvas : MonoBehaviour
{
    [Header("Prototyping")]
    public List<AswangInfo> aswangInfos2;
    public PlayersData dataPlay;
    [Header("MainPage")]
    public GameObject aswangJournal;
    public bool isOpen;

    [Header("NeedReference")]
    public Image aswangPortrait;
    public Text aswangName;
    public Text obsContent;
    public Transform obsContainer;
    public GameObject observations;

    public List<GameObject> obsButList;
    private int currentPage = 0;

    [Header("V2")]
    public PlayersData pData;
    private void Start()
    {
        //dataPlay = Referencer.Instance.pDatas;
        if(aswangPortrait ==null||aswangName==null||obsContent==null||obsContainer==null||observations ==null||aswangJournal == null)
        {
            Debug.LogError("Aswang Canvas is Missing Reference/s");
        }
        else
        {
            isOpen = false;
        }
    }

    private void Update()
    {
        /*
        if (Input.GetKeyUp(KeyCode.J))
        {
            //ShowOrCloseAswangJournal();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            
            AswangObservation awson = new AswangObservation();
            awson.obsTitle = "NewTitle";
            awson.obsContent = "NewContent";
            aswangInfos2[currentPage].observations.Add(awson);
             // Can Add new Observation;
        }
        if (Input.GetKeyUp(KeyCode.Backspace))
        {
            CloseAswangCanvas();
        }*/
    }

    public void ShowOrCloseAswangJournal()
    {
        if(isOpen)
        {
            ClearAswangObservationButtons();
            CloseAswangCanvas();
        }
        else
        {
            ViewAswangInformation();
            isOpen = true;
        }
    }

    public void ViewAswangInformation()
    {
        /*
        Debug.Log("ViewingAswangCanvas");
        if(dataPlay == null)
        {
            //dataPlay = Referencer.Instance.pDatas;
        }
        if (aswangInfos2.Count > 0)
        {
            ClearAswangObservationButtons();
            aswangPortrait.sprite = aswangInfos2[currentPage].aswangImage;
            aswangName.text = aswangInfos2[currentPage].aswangName;
            obsContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(obsContainer.GetComponent<RectTransform>().sizeDelta.x, aswangInfos2[currentPage].observations.Count * 55);
            switch(aswangInfos2[currentPage].aswangName)
            {
                case "Manananggal":
                    for (int i = 0; i < dataPlay.manangObservations.Count; i++)
                    {
                        GameObject but = Instantiate(observations, obsContainer);
                        but.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * -55, 0);
                        but.GetComponent<AswObsButton>().obsContentUI = obsContent;
                        but.GetComponent<AswObsButton>().content = dataPlay.manangObservations[i].obsContent;
                        but.GetComponent<AswObsButton>().obsTitleUI.text = dataPlay.manangObservations[i].obsTitle;
                        obsButList.Add(but);
                    }
                    break;
                case "Aswang":
                    for (int i = 0; i < dataPlay.aswangObservations.Count; i++)
                    {
                        GameObject but = Instantiate(observations, obsContainer);
                        but.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * -55, 0);
                        but.GetComponent<AswObsButton>().obsContentUI = obsContent;
                        but.GetComponent<AswObsButton>().content = dataPlay.aswangObservations[i].obsContent;
                        but.GetComponent<AswObsButton>().obsTitleUI.text = dataPlay.aswangObservations[i].obsTitle;
                        obsButList.Add(but);
                    }
                    break;
            }
            
        }
        */
    }
    public void CloseAswangCanvas()
    {
        obsContent.text = " ";
        currentPage = 0;
        isOpen = false;
    }
    public void ClearAswangObservationButtons()
    {
        if (obsButList.Count > 0)
        {
            for (int d = 0; d < obsButList.Count; d++)
            {
                Destroy(obsButList[d].gameObject);
            }
            obsButList.Clear();
        }
    }
    public void PageChangeButton(int pageAdd)
    {
        if (aswangInfos2.Count <= 0)
        {
            return;
        }
        else if (aswangInfos2.Count == 1)
        {
            currentPage = 0;
        }
        else if (aswangInfos2.Count > 1)
        {
            if (currentPage + pageAdd < 0)
            {
                currentPage = aswangInfos2.Count - 1;
            }
            else if (currentPage + pageAdd > aswangInfos2.Count - 1)
            {
                currentPage = 0;
            }
            else
            {
                currentPage += pageAdd;
            }
        }
        ViewAswangInformation();
    }

    //Function for resetting Aswang canvas, used for chaning tabs
    public void ResetNotesCanvas()
    {
        ClearAswangObservationButtons();
        CloseAswangCanvas();
    }

    /* Obselete
    public void ViewAswangInfo(int i)
    {
        AswangUITemplate aTemp = aTemplate.GetComponent<AswangUITemplate>();
        aTemp.ConstructAswangTemplate(aswangInfos[i]);
        ChangePageTab(i);
    }
    
    public void ChangePageTab(int p)
    {
        //0 p0= 0,1 p1 = 1,2 p2 = 2,3
        //1 p0= 1,2 p1 = 2,3 p2 = 0,1
        //2 p0= 2,3 p1 = 0,1 p2 = 1,2
        for (int i = 0; i < pages.Count; i++)
        {
            AswangCanBut pag = pages[i].GetComponent<AswangCanBut>();
            Debug.Log(pag);
            
            if ((p + 1) + i == pages.Count+1)
            {
                pag.pageNumber = 0 ;
                pag.pageNum.text = (1).ToString();
            }
            else if((p + 1) + i > pages.Count + 1)
            {
                pag.pageNumber = 1;
                pag.pageNum.text = (2).ToString();
            }
            else
            {
                pag.pageNumber = p + i;
                pag.pageNum.text = (p + 1 + i).ToString();
            }

        }
    }
    */

}
