﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AswangCanBut : MonoBehaviour
{
    public AswangCanvas aswangCanvas;
    public int pageNumber;
    public Text pageNum;

    public void ChangePage()
    {
        //aswangCanvas.ViewAswangInfo(pageNumber);
    }
    public void ChangeNumbers(int i)
    {
        pageNumber = i;
        pageNum.text = (i+1).ToString();
    }
}
