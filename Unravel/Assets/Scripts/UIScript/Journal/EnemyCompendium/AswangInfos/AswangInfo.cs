﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName ="AswangInfo")]
public class AswangInfo : ScriptableObject
{
    public int aswangID;
    public Sprite aswangImage;
    public string aswangName;
    [TextArea(15,20)]
    public string aswangDescription;
    [Header("Observations")]
    //public List<string> observations;
    public List<AswangObservation> observations;
    public int CheckCount()
    {
        return observations.Count;
    }

    public List<AswangObservation> ReturnObs()
    {
        return observations;
    }
}

