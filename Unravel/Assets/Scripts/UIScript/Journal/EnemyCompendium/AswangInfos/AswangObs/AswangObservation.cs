﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AswangObservation 
{
    public string obsTitle;
    [TextArea(5, 10)]
    public string obsContent;

    public AswangObservation ReturnDeepCopyObs()
    {
        AswangObservation aObs = new AswangObservation();
        aObs.obsTitle = this.obsTitle;
        aObs.obsContent = this.obsContent;
        return aObs;
    }
}
