﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointABTemplateUI : MonoBehaviour
{
    public Text title;
    public Text description;
    public Text isCompleted;
    public Text pointToReach;
}
