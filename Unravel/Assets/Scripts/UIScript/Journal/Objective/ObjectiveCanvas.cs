﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveCanvas : MonoBehaviour
{

    public int currentPage;
    public bool isInView = false;
    [Header("Journal Objects")]
    public ObjectiveHandler handler;
    public GameObject onGoingPage;
    public GameObject completedPage;
    public GameObject noDisplay;
    [Space(10)]
    [Header("UI Buttons")]
    public GameObject objectiveButtons;
    public GameObject onGoingButtons;
    public GameObject completedButtons;
    void Start()
    {
        if(handler == null|| onGoingPage == null)
        {
            Debug.LogError("handler missing or error");
        }
        onGoingPage.SetActive(false);
        completedPage.SetActive(false);
        onGoingButtons.SetActive(false);
        completedButtons.SetActive(false);
        noDisplay.SetActive(false);
    }

    void Update()
    {
       
    }

    public void OpenOnGoingObjectives()
    {
        if (handler.curObjectives.Count > 0)
        {
            onGoingPage.gameObject.SetActive(true);
            onGoingPage.GetComponent<OnGoingObjPage>().ViewOnGoingObjectives(handler.curObjectives[0]);
            onGoingPage.GetComponent<OnGoingObjPage>().handler = handler;
            onGoingButtons.SetActive(true);
        }
        else
        {
            noDisplay.SetActive(true);
        }
        objectiveButtons.SetActive(false);
    }

    public void OpenCompletedObjectives()
    {
        /*
        if (handler.compObjectives.Count > 0)
        {
            completedPage.gameObject.SetActive(true);
            completedPage.GetComponent<CompletedObjPage>().ViewCompletedObjectives(handler.compObjectives[0]);
            completedPage.GetComponent<CompletedObjPage>().handler = handler;
            completedButtons.SetActive(true);
        }
        else
        {
            noDisplay.SetActive(true);
        }
        objectiveButtons.SetActive(false);*/
    }

    public void CloseOnGoingObjective()
    {
        objectiveButtons.SetActive(true);
        onGoingPage.gameObject.SetActive(false);
    }
    public void ResetObjectiveCanvas()
    {
        onGoingPage.SetActive(false);
        completedPage.SetActive(false);
        objectiveButtons.SetActive(true);
        onGoingButtons.SetActive(false);
        completedButtons.SetActive(false);
        onGoingPage.SetActive(false);
        completedPage.SetActive(false);
        noDisplay.SetActive(false);
    }
}
