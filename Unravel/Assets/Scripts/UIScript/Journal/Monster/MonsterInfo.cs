﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterInfo")]
public class MonsterInfo : ScriptableObject
{
    public int monsterID;
    public Sprite monsterImage;
    public Sprite unknownImage;
    public string monsterName;
    [TextArea(15, 20)]
    public string monsterDescription;
    public List<MonsterObservation> mKnowledge;
}
