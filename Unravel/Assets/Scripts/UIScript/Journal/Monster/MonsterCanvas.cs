﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterCanvas : MonoBehaviour
{
    private PlayersData pData;
    public List<MonsterInfo> monsterInfo;
    public Sprite unknown;
    [Header("MainPage")]
    public GameObject monsterJournal;
    public bool isOpen;

    [Header("NeedReference")]
    public Image monsterPortrait;
    public Text monsterTitle;
    public Text monsterDescription;
    public Transform monObsContainer;
    public GameObject monsterObservation;

    public List<GameObject> monObsList;
    private int currentPage = 0;

    private void Awake()
    {
        pData = PlayersData.instance;
    }
    private void Start()
    {
        if (monsterPortrait == null || monsterTitle == null || monsterDescription == null || monObsContainer == null || monsterObservation == null || monsterJournal == null)
        {
            Debug.LogError("Aswang Canvas is Missing Reference/s");
        }
        else
        {
            isOpen = false;
        }
    }

    private void Update()
    {
    }
    
    public void ShowOrCloseMonsterJournal()
    {
        if (isOpen)
        {
            ClearMonsterObservationButtons();
            CloseMonsterCanvas();
        }
        else
        {
            ViewMonsterInformation();
            isOpen = true;
        }
    }
    
    public void ViewMonsterInformation()
    {
        if(monsterInfo.Count > 0)
        {
            ClearMonsterObservationButtons();
            if (CheckIfDiscovered(monsterInfo[currentPage].monsterID))
            {
                monsterPortrait.sprite = monsterInfo[currentPage].monsterImage;
                monsterPortrait.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(monsterInfo[currentPage].monsterImage.rect.width, monsterInfo[currentPage].monsterImage.rect.height);
                monsterTitle.text = monsterInfo[currentPage].monsterName;
                monsterDescription.text = monsterInfo[currentPage].monsterDescription;
            }
            else
            {
                monsterPortrait.sprite = unknown;
                monsterPortrait.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(unknown.rect.width, unknown.rect.height);
                monsterTitle.text = "???";
                monsterDescription.text = "?";
            }
            switch (monsterInfo[currentPage].monsterName)
            {
                case "Manananggal":
                    float manMonObsSize = 0;
                    for (int i = 0; i < pData.manananggalObsIDs.Count; i++)
                    {
                        for(int o = 0; o< monsterInfo[currentPage].mKnowledge.Count;o++)
                        {
                            if (pData.manananggalObsIDs[i] == monsterInfo[currentPage].mKnowledge[o].observationID)
                            {
                                GameObject obs = Instantiate(monsterObservation, monObsContainer);
                                monObsList.Add(obs);
                                obs.GetComponent<Text>().text = monsterInfo[currentPage].mKnowledge[o].obsContent;

                                obs.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, manMonObsSize * -1, 0);

                                manMonObsSize += obs.GetComponent<Text>().preferredHeight;
                                monObsContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(monObsContainer.GetComponent<RectTransform>().sizeDelta.x, manMonObsSize);
                            }
                        }
                    }
                    break;
                case "Aswang":
                    float aswMonObsSize = 0;

                    for (int i = 0; i < pData.aswangObsIDs.Count; i++)
                    {
                        for (int o = 0; o < monsterInfo[currentPage].mKnowledge.Count; o++)
                        {
                            if (pData.aswangObsIDs[i] == monsterInfo[currentPage].mKnowledge[o].observationID)
                            {
                                GameObject obs = Instantiate(monsterObservation, monObsContainer);
                                monObsList.Add(obs);
                                obs.GetComponent<Text>().text = monsterInfo[currentPage].mKnowledge[o].obsContent;

                                obs.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, aswMonObsSize * -1, 0);

                                aswMonObsSize += obs.GetComponent<Text>().preferredHeight;
                                monObsContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(monObsContainer.GetComponent<RectTransform>().sizeDelta.x, aswMonObsSize);
                            }
                        }
                    }
                   
                    break;
                case "Mangkukulam":
                    float mangkukulmaMonObsSize = 0;

                    for (int i = 0; i < pData.aswangObsIDs.Count; i++)
                    {
                        for (int o = 0; o < monsterInfo[currentPage].mKnowledge.Count; o++)
                        {
                            if (pData.mangkukulamObsID[i] == monsterInfo[currentPage].mKnowledge[o].observationID)
                            {
                                GameObject obs = Instantiate(monsterObservation, monObsContainer);
                                monObsList.Add(obs);
                                obs.GetComponent<Text>().text = monsterInfo[currentPage].mKnowledge[o].obsContent;

                                obs.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, mangkukulmaMonObsSize * -1, 0);

                                mangkukulmaMonObsSize += obs.GetComponent<Text>().preferredHeight;
                                monObsContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(monObsContainer.GetComponent<RectTransform>().sizeDelta.x, mangkukulmaMonObsSize);
                            }
                        }
                    }
                    
                    break;
            }

        }
    }

    public bool CheckIfDiscovered(int i)
    {
        if(pData.monsterIDDiscovered.Count<=0)
        {
            return false;
        }
        for(int d= 0; d<pData.monsterIDDiscovered.Count;d++)
        {
            if(i == pData.monsterIDDiscovered[d])
            {
                return true;
            }
        }
        return false;
    }

    public void CloseMonsterCanvas()
    {
        monsterTitle.text = " ";
        monsterDescription.text = " ";
        currentPage = 0;
        isOpen = false;
    }
    public void ClearMonsterObservationButtons()
    {
        if (monObsList.Count > 0)
        {
            for (int d = 0; d < monObsList.Count; d++)
            {
                Destroy(monObsList[d].gameObject);
            }
            monObsList.Clear();
        }
    }
    public void PageChangeButton(int pageAdd)
    {
        if (monsterInfo.Count <= 0)
        {
            return;
        }
        else if (monsterInfo.Count == 1)
        {
            currentPage = 0;
        }
        else if (monsterInfo.Count > 1)
        {
            if (currentPage + pageAdd < 0)
            {
                currentPage = monsterInfo.Count - 1;
            }
            else if (currentPage + pageAdd > monsterInfo.Count - 1)
            {
                currentPage = 0;
            }
            else
            {
                currentPage += pageAdd;
            }
        }
        ViewMonsterInformation();
    }

    //Function for resetting Aswang canvas, used for chaning tabs
    public void ResetNotesCanvas()
    {
        ClearMonsterObservationButtons();
        CloseMonsterCanvas();
    }
    

}
