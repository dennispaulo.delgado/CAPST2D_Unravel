﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MonsterObservation
{
    public int observationID;
    public string obsTitle;
    [TextArea(5, 10)]
    public string obsContent;

    public MonsterObservation ReturnDeepCopyObs()
    {
        MonsterObservation mObs = new MonsterObservation();
        mObs.obsTitle = this.obsTitle;
        mObs.obsContent = this.obsContent;
        return mObs;
    }
}
