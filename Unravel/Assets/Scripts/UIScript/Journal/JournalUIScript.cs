﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalUIScript : MonoBehaviour
{
    public static event EventHandler OnToggleJournal;

    [Header("New")]
    public GameObject generalPage;
    public GameObject monsterPage;
    public GameObject lorePage;
    public GameObject mapPage;

    public GameObject journalCanvas;

    [Header("JournalTabs")]
    public JournalTabs generalTab;
    public JournalTabs monsterTab;
    public JournalTabs loreTab;
    public JournalTabs mapTab;

    public bool isOpen = false;
    [Header("AudioSource")]
    public AudioClip turnPage;
    public AudioClip openJournal;
    public AudioClip closeJournal;

    void Start()
    {
        
        Referencer.Instance.journalUI = this;
        journalCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Tab)&&!GameManager.instance.gameIsPaused)
        {
            ViewOrPutAwayJournal();
        }
    }

    ///////////////////////////////New///////////////////////////////////
    public void OpenCanvasTab(int index)
    {
        this.GetComponent<AudioSource>().clip = turnPage;
        this.GetComponent<AudioSource>().Play();
        switch (index)
        {
            case 0:
                OpenGeneralPage();
                CloseMonsterPage();
                CloseLorePage();
                CloseMapPage();
                break;
            case 1:
                CloseGeneralPage();
                OpenMonsterPage();
                CloseLorePage();
                CloseMapPage();
                break;
            case 2:
                CloseGeneralPage();
                CloseMonsterPage();
                OpenLorePage();
                CloseMapPage();
                break;
            case 3:
                CloseGeneralPage();
                CloseMonsterPage();
                CloseLorePage();
                OpenMapPage();
                break;
        }
    }
    public void TurnOffButton(Button but)
    {

    }

    public void TurnPageSound()
    {
        this.GetComponent<AudioSource>().clip = turnPage;
        this.GetComponent<AudioSource>().Play();
    }

    public void ViewOrPutAwayJournal()
    {
        if(isOpen)
        {
            Debug.Log("Closing");
            if(PlayersData.instance.sceneHasPlayer)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                Time.timeScale = 1;
                journalCanvas.SetActive(false);
                OnToggleJournal?.Invoke(this, EventArgs.Empty);
                isOpen = false;
                this.GetComponent<AudioSource>().clip = closeJournal;
                this.GetComponent<AudioSource>().Play();
                CloseJournal();
            }
            else
            {
                journalCanvas.SetActive(false);
                OnToggleJournal?.Invoke(this, EventArgs.Empty);
                isOpen = false;
                this.GetComponent<AudioSource>().clip = closeJournal;
                this.GetComponent<AudioSource>().Play();
                CloseJournal();
            }    
        }
        else
        {
            Debug.Log("Opening");
            if (PlayersData.instance.sceneHasPlayer)
            {
                Time.timeScale = 0.1f;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                journalCanvas.SetActive(true);
                OnToggleJournal?.Invoke(this, EventArgs.Empty);
                isOpen = true;
                this.GetComponent<AudioSource>().clip = openJournal;
                this.GetComponent<AudioSource>().Play();
                OpenJournal();
            }
            else
            {
                journalCanvas.SetActive(true);
                OnToggleJournal?.Invoke(this, EventArgs.Empty);
                isOpen = true;
                this.GetComponent<AudioSource>().clip = openJournal;
                this.GetComponent<AudioSource>().Play();
                OpenJournal();
            }
        }
    }

    public void ViewOrPutAwayMapOnJournal()
    {
        if (isOpen)
        {
            if(!mapPage.activeSelf)
            {
                OpenCanvasTab(3);
            }
        }
        else
        {
            Time.timeScale = 0.1f;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            journalCanvas.SetActive(true);
            OnToggleJournal?.Invoke(this, EventArgs.Empty);
            isOpen = true;
            this.GetComponent<AudioSource>().clip = openJournal;
            this.GetComponent<AudioSource>().Play();
            OpenCanvasTab(3);
        }
    }

    private void OpenJournal()
    {
        OpenGeneralPage();
        CloseLorePage();
        CloseMonsterPage();
        CloseMapPage();
    }
    private void CloseJournal()
    {
        CloseGeneralPage();
        CloseLorePage();
        CloseMonsterPage();
        CloseMapPage();
    }
    public void OpenMonsterPage()
    {
        monsterPage.SetActive(true);
        monsterTab.ButtonSelected();
        //AswangCanvas
        monsterPage.GetComponent<MonsterCanvas>().ShowOrCloseMonsterJournal();
    }
    public void CloseMonsterPage()
    {
        monsterPage.SetActive(false);
        monsterTab.ButtonUnselected();
        monsterPage.GetComponent<MonsterCanvas>().ResetNotesCanvas();
    }
    public void OpenLorePage()
    {
        lorePage.SetActive(true);
        loreTab.ButtonSelected();
        lorePage.GetComponent<LoreCanvas>().ShowOrCloseLoreJournal();
    }
    public void CloseLorePage()
    {
        lorePage.SetActive(false);
        loreTab.ButtonUnselected();
        lorePage.GetComponent<LoreCanvas>().ClearStoryButtons();
    }
    public void OpenGeneralPage()
    {
        generalPage.SetActive(true);
        generalTab.ButtonSelected();
        //generalPage.GetComponent<ObjectiveJCanvas>().ShowOrCloseObjectiveJournal();
    }
    public void CloseGeneralPage()
    {
        generalPage.SetActive(false);
        generalTab.ButtonUnselected();
        //generalPage.GetComponent<ObjectiveJCanvas>().ResetObjectiveCanvas();
    }

    public void OpenMapPage()
    {
        mapTab.ButtonSelected();
        mapPage.SetActive(true);
    }

    public void CloseMapPage()
    {
        mapTab.ButtonUnselected();
        mapPage.SetActive(false);
    }
}
