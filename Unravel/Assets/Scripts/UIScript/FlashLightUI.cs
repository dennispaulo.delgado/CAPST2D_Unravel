﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashLightUI : MonoBehaviour
{
    public Image flashLightbatBar;
    public Flashlight flashlight;
    // Start is called before the first frame update
    void Start()
    {
        flashlight = Referencer.Instance.flashlight;
    }

    // Update is called once per frame
    void Update()
    {
        if(flashlight == null)
        {
            flashlight = Referencer.Instance.flashlight;
        }
        flashLightbatBar.fillAmount = flashlight.flaCurBattery / flashlight.flaMaxBattery;
    }
}
