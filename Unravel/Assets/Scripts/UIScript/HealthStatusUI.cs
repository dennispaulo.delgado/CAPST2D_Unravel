﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class HealthStatusUI : MonoBehaviour
{
    public static EventHandler OnDelayStatusFinish;

    [SerializeField] List<UILineRenderer> healthStatuses;
    [SerializeField] UILineRenderer visibleHealthStatus;

    private void Awake()
    {
        HealthScript.OnGreatHealth += HealthScript_OnGreatHealth;
        HealthScript.OnGoodHealth += HealthScript_OnGoodHealth;
        HealthScript.OnFatalHealth += HealthScript_OnFatalHealth;

        StartCoroutine(DelayStatus());
    }


    // Start is called before the first frame update
    void Start()
    {
        //HealthScript.OnGreatHealth += HealthScript_OnGreatHealth;
        //HealthScript.OnGoodHealth += HealthScript_OnGoodHealth;
        //HealthScript.OnFatalHealth += HealthScript_OnFatalHealth;

        //StartCoroutine(DelayStatus());
        //Invoke("ResetStatus", 1f);
    }

    void HealthScript_OnGreatHealth(object sender, EventArgs e)
    {
        foreach(UILineRenderer status in healthStatuses)
        {
            if(status.gameObject.tag == "Green")
            {
                visibleHealthStatus = status;
                status.gameObject.SetActive(true);
            }
            else
            {
                status.gameObject.SetActive(false);
            }

        }
    }

    void HealthScript_OnGoodHealth(object send, EventArgs e)
    {
        foreach (UILineRenderer status in healthStatuses)
        {
            if (status.gameObject.tag == "Yellow")
            {
                visibleHealthStatus = status;
                status.gameObject.SetActive(true);
            }
            else
            {
                status.gameObject.SetActive(false);
            }
        }
    }

    void HealthScript_OnFatalHealth(object send, EventArgs e)
    {
        foreach (UILineRenderer status in healthStatuses)
        {
            if (status.gameObject.tag == "Red")
            {
                visibleHealthStatus = status;
                status.gameObject.SetActive(true);
            }
            else
            {
                status.gameObject.SetActive(false);
            }
        }
    }


    private void ResetStatus()
    {
        Debug.Log("ResetStatus");
        foreach (UILineRenderer status in healthStatuses)
        {
            status.gameObject.SetActive(false);
        }

       
    }

    IEnumerator DelayStatus()
    {
        
        yield return new WaitForSeconds(1f);
        ResetStatus();
        OnDelayStatusFinish?.Invoke(this, EventArgs.Empty);
    }

    private void OnDestroy()
    {
        HealthScript.OnGreatHealth -= HealthScript_OnGreatHealth;
        HealthScript.OnGoodHealth -= HealthScript_OnGoodHealth;
        HealthScript.OnFatalHealth -= HealthScript_OnFatalHealth;
    }




    // Update is called once per frame
    void Update()
    {
        
    }
}
