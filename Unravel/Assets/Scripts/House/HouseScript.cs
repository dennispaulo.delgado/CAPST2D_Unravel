﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HouseScript : MonoBehaviour
{
    public UnityEvent OnStartDialogue;
    public UnityEvent OnFinishDialogue;

    public DialogueSet dialSet;
    public CollectiveHouseInteract colHouse;

    public bool hasTalkedTo = false;

    public void KnockOnDoor()
    {
        if (!hasTalkedTo)
        {
            colHouse.InteractWithAHouse(this);
            hasTalkedTo = true; 
        }
        else
        {
            if(dialSet!=null)
            {
                ActivateHouseInteraction();
            }
        }
    }

    public void ActivateHouseInteraction()
    {
        GameManager.instance.equipmentHandIsBusy = true;
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSet);
        OnStartDialogue.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
        GameManager.instance.equipmentHandIsBusy = false;
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }
}
