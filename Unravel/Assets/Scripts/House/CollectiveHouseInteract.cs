﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class CollectiveHouseInteract : MonoBehaviour
{

    public List<HouseInteract> houseList;
    public int index = 0;

    public void InteractWithAHouse(HouseScript house)
    {
        house.dialSet = houseList[index].dialSet;
        ActivateHouseInteraction(houseList[index]);
    }


    private void ActivateHouseInteraction(HouseInteract hI)
    {
        GameManager.instance.equipmentHandIsBusy = true;
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(hI.dialSet);
        hI.OnStartDialogue.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
        GameManager.instance.gameIsPaused = true;
        Time.timeScale = 0f;
    }
    private void OnDialogueComplete()
    {
        houseList[index].OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        index++;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
        GameManager.instance.equipmentHandIsBusy = false;
        GameManager.instance.gameIsPaused = false;
        Time.timeScale = 1f;
    }
    private void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }
}

[System.Serializable]
public class HouseInteract
{
    public DialogueSet dialSet;
    public UnityEvent OnStartDialogue;
    public UnityEvent OnFinishDialogue;
}
