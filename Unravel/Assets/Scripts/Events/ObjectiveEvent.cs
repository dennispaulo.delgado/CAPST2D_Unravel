﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveEvent : MonoBehaviour
{
    public static ObjectiveEvent instance;
    public delegate void OnAnswerTest(string message);
    public event OnAnswerTest onQuestEvent;

    public delegate void OnPointID(int pID);
    public event OnPointID onArriveGoal;

    public event Action evalObjective;

    public event Action evalObjHandler;

    public delegate void OnConstructObjPage(Objective obj);
    public event OnConstructObjPage constructObjPage;

    public delegate void OnUpdateObjectiveBook(int i);
    public event OnUpdateObjectiveBook updateObjectiveBook;

    public delegate void OnSimpleObjUI(string s);
    public event OnSimpleObjUI updateSimpleOBJUI;

    public delegate void OnAddObjective(ObjectiveSlot s);
    public event OnAddObjective addObjective;

    //Simplest quest, check the ID if you completed something.
    public delegate void OnQuestArrived(int questID);
    public event OnQuestArrived onQuestArrived;
    //
    public delegate void OnGotItem(Item.ItemType type);
    public event OnGotItem onCheckItemType;
    //
    public event Action evalCorpseBlessed;
    //
    public event Action addedObjective;

    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }

    //Functions to call when action
    public void QuestTrigger(string t)
    {
        if(onQuestEvent != null)
        {
            onQuestEvent(t);
        }
    }
    public void AddedObjective()
    {
        if (addedObjective != null)
        {
            Debug.Log("Added Objective");
            addedObjective();
        }
    }
    public void PointTrigger(int id)
    {
        if (onArriveGoal != null)
        {
            onArriveGoal(id);
        }
        //Debug.Log("ArriveEvent");
    }

    //Using this
    public void OnQuestArrivedS(int id)
    {
        if (onQuestArrived != null)
        {
            onQuestArrived(id);
        }
        //Debug.Log("ArriveEvent");
    }

    public void OnCheckItemType(Item.ItemType type)
    {
        if(onCheckItemType !=null)
        {
            onCheckItemType(type);
        }
    }

    public void EvaluateCorpseBlessed()
    {
        if(evalCorpseBlessed !=null)
        {
            evalCorpseBlessed();
        }
    }

    /// <summary>
    /// /////
    /// </summary>
    public void CheckObjectives()
    {
        if(evalObjective!=null)
        {
            evalObjective();
        }
    }
    public void CheckObjHandler()
    {
        if (evalObjHandler != null)
        {
            evalObjHandler();
        }
    }
    public void ConstructObjPageUI(Objective obj)
    {
        if (constructObjPage != null)
        {
            constructObjPage(obj);
        }
    }
    public void UpdateObjectiveBook(int i)
    {
        if (updateObjectiveBook != null)
        {
            updateObjectiveBook(i);
        }
    }
    public void UpdateSimpleObjUI(string s)
    {
        if (updateSimpleOBJUI != null)
        {
            updateSimpleOBJUI(s);
        }
    }
    public void AddObjectiveSlot(ObjectiveSlot s)
    {
        if (addObjective != null)
        {
            addObjective(s);
        }
    }
}
