﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents instance;
    public event Action radarMakesNoise;

    public event Action onSave;
    public event Action onLoad;
    public event Action clearListeners;

    //public delegate void OnPuzzleComplete(int id);
    //public event OnPuzzleComplete onPuzzleComplete;

    public event Action onPuzzleComplete;

    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }

    public void OnCheckPuzzleComplete()
    {
        if (onPuzzleComplete != null)
        {
            onPuzzleComplete();
        }
    }

    public void ClearListeners()
    {
        if (clearListeners != null)
        {
            clearListeners();
        }
    }

    public void OnSave()
    {
        if (onSave != null)
        {
            onSave();
        }
    }

    public void OnLoad()
    {
        if (onLoad != null)
        {
            onLoad();
        }
    }

    public void RadarMakesNoise()
    {
        if (radarMakesNoise != null)
        {
            radarMakesNoise();
        }
    }
}
