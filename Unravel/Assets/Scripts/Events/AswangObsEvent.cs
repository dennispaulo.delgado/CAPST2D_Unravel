﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AswangObsEvent : MonoBehaviour
{
    public static AswangObsEvent instance;
    //public event Action onMananangalSeePlayerLH;

    //public delegate void OnManananggalSeePlayerLowH(AswangObservation aswObs);
    public event Action onManananggalSeePlayerLH;

    //public delegate void OnPlayerSeeManananggalNoLow(AswangObservation aswObs);
    public event Action onPlayerSeeManananggalNoLB;

    //public delegate void OnPlayerDiscoverManananggalW(AswangObservation aswObs);
    public event Action onPlayerSeeManananggalWeakN;

    public event Action onPlayerSawAswangInvi;

    public event Action onPlayerEnterFamilyEstate;

    public event Action onPlayerSeeAswangEatCorpse;

    public event Action onPlayerDiscoverWeakness;

    public delegate void AddManananggalObsID(int id);
    public event AddManananggalObsID onAddManananggalObs;

    public delegate void AddAswangObsID(int id);
    public event AddAswangObsID onAddAswangObs;

    public delegate void AddMangkukulamObsID(int id);
    public event AddMangkukulamObsID onAddMangkukulamObs;


    public delegate void AddMonsterDiscoverID(int id);
    public event AddMonsterDiscoverID onAddMonsterDiscover;

    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }
    public void OnAddManananggalObs(int t)
    {
        if (onAddManananggalObs != null)
        {
            onAddManananggalObs(t);
        }
    }
    public void OnAddAswangObs(int t)
    {
        if (onAddAswangObs != null)
        {
            onAddAswangObs(t);
        }
    }
    public void OnAddMangkukulamObs(int t)
    {
        if (onAddMangkukulamObs != null)
        {
            onAddMangkukulamObs(t);
        }
    }

    public void OnAddMonsterDiscoverObs(int t)
    {
        if (onAddMonsterDiscover != null)
        {
            onAddMonsterDiscover(t);
        }
    }

    public void OnManananggalSeePlayerLowHealth()
    {
        if (onManananggalSeePlayerLH != null)
        {
            onManananggalSeePlayerLH();
        }
    }

    public void OnPlayerSeeManananggalNoLowerBody()
    {
        if (onPlayerSeeManananggalNoLB != null)
        {
            onPlayerSeeManananggalNoLB();
        }
    }

    public void OnPlayerDiscoverManananggalWeakness()
    {
        if (onPlayerSeeManananggalWeakN != null)
        {
            onPlayerSeeManananggalWeakN();
        }
    }

    public void OnPlayerSawAswangInvisible()
    {
        if(onPlayerSawAswangInvi !=null)
        {
            onPlayerSawAswangInvi();
        }
    }

    public void OnPlayerEnterFamilyEstate()
    {
        if (onPlayerEnterFamilyEstate != null)
        {
            onPlayerEnterFamilyEstate();
        }
    }

    public void OnPlayerSeeAswangEatingCorpses()
    {
        if (onPlayerSeeAswangEatCorpse != null)
        {
            onPlayerSeeAswangEatCorpse();
        }
    }

    public void OnPlayerDiscoveredAswangWeakness()
    {
        if (onPlayerDiscoverWeakness != null)
        {
            onPlayerDiscoverWeakness();
        }
    }

}
