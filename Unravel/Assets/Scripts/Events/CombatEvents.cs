﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatEvents : MonoBehaviour
{
    public static CombatEvents instance;
    public event Action onPlayerSave;


    public event Action onMangkukulamStunned;
    public event Action onMangkukulamUnstunned;
    public event Action onTotemDestroyed;

    public delegate void MangkukulamCastingRitual(float prog);
    public event MangkukulamCastingRitual onMangkukulamCastingRitual;

    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }
    public void MangkukulamCastingRitualProgress(float prog)
    {
        if (onMangkukulamCastingRitual != null)
        {
            onMangkukulamCastingRitual(prog);
        }
    } 

    public void OnMangkukulamStun()
    {
        if(onMangkukulamStunned != null)
        {
            onMangkukulamStunned();
        }
    }
    public void OnMangkukulamUnstun()
    {
        if (onMangkukulamUnstunned != null)
        {
            onMangkukulamUnstunned();
        }
    }

    public void OnTotemDestroyed()
    {
        if(onTotemDestroyed !=null)
        {
            onTotemDestroyed();
        }
    }

    public void OnPlayerSave()
    {
        if (onPlayerSave != null)
        {
            onPlayerSave();
        }
    }
}
