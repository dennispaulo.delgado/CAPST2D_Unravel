﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemIndicatorScript : MonoBehaviour
{
    
    private bool isActive = false;
    private Transform player;
    [Header("Required Variables")]
    public Sprite itemIndi;
    public Sprite grabIndi;
    public GameObject item;
    [Header("References for Indicator")]
    public GameObject visual;
    public SpriteRenderer backGround;
    public SpriteRenderer icon;
    public SpriteRenderer interact;
    [Header("Attribute Variables")]
    public float indicatorRange;
    // Start is called before the first frame update
    void Start()
    {
        //this.transform.position = new Vector3(item.transform.position.x, item.transform.position.y+3, item.transform.position.z);
        visual = this.transform.GetChild(0).gameObject;
        visual.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            this.transform.position = item.transform.position;
            Vector3 targetVector = Camera.main.transform.position - this.transform.position;

            float newAngle = Mathf.Atan2(targetVector.z, targetVector.x) * Mathf.Rad2Deg;

            this.transform.rotation = Quaternion.Euler(0, -1 * newAngle, 0);
            CheckPlayerRange();
        }
    }

    public void ItemInRange(Transform nPlayer)
    {
        isActive = true;
        player = nPlayer;
        visual.SetActive(true); 
        if (item.GetComponent<InteractableTypeHolder>()!=null)
        {
            icon.sprite = item.GetComponent<InteractableTypeHolder>().interactableType.sprite2D;
        }
        else
        {
            icon.sprite = item.GetComponent<SimpleItem>().itemToGet.interactable.sprite2D;
        }
        interact.gameObject.SetActive(false);
    }

    public void ItemIsNearest(Transform nPlayer)
    {
        isActive = true;
        player = nPlayer;
        visual.SetActive(true);
        if (item.GetComponent<InteractableTypeHolder>() != null)
        {
            icon.sprite = item.GetComponent<InteractableTypeHolder>().interactableType.sprite2D;
        }
        else
        {
            icon.sprite = item.GetComponent<SimpleItem>().itemToGet.interactable.sprite2D;
        }
        interact.gameObject.SetActive(true);
    }

    public void CheckPlayerRange()
    {
        float distance = Vector3.Distance(this.transform.position, player.position);
        if(distance>indicatorRange)
        {
            player = null;
            visual.SetActive(false);
            isActive = false;
        }
    }
}
