﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcePalmVisuals : MonoBehaviour
{
    public Vector3 direction;
    public float duration;
    public float speed;
    void Start()
    {
        
    }

    void Update()
    {
        if(duration<=0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            transform.position += direction * speed * Time.deltaTime;
            duration -= Time.deltaTime;
        }
    }
}
