﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ApplyFrost : MonoBehaviour
{
    public Material frostMat;
    public Material[] matz;
    public List<Renderer> skinRender;
    private List<Material> originalRenderer = new List<Material>();
    private List<RendererMaterials> originalMats = new List<RendererMaterials>();

    private bool isFrosted = false;
    private void Awake()
    {

    }
    

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            GetFrosted();
        }
        if(Input.GetKeyUp(KeyCode.Backspace))
        {
            GetDefrosted();
        }
    }

    public void GetFrosted()
    {
        if (!isFrosted)
        {
            for (int i = 0; i < this.skinRender.Count; i++)
            {
                RendererMaterials rMat = new RendererMaterials();
                rMat.rMaterials = new List<Material>(skinRender[i].materials);
                originalMats.Add(rMat);
                List<Material> matMat = new List<Material>();
                for (int y = 0; y < skinRender[i].materials.Length; y++)
                {
                    //originalRenderer.Add(skinRender[i].materials[y]);
                    Material m = new Material(frostMat);
                    m.SetColor("Color_2FEB75C", skinRender[i].materials[y].color);
                    matMat.Add(m);
                }
                skinRender[i].materials = matMat.ToArray();
            }
            isFrosted = true;
        }
    }

    public void GetDefrosted()
    {
        if (isFrosted)
        {
            for (int i = 0; i < this.skinRender.Count; i++)
            {
                //this.skinRender[i].materials = originalRenderer.ToArray();
                this.skinRender[i].materials = originalMats[i].rMaterials.ToArray();
            }
            isFrosted = false;
        }
    }
}
