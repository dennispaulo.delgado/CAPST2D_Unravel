﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class RendererMaterials
{
    public List<Material> rMaterials;
}
