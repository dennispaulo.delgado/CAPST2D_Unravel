﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    
    [SerializeField]
    private AudioClip[] clips_grass;
    [SerializeField]
    private AudioClip[] clips_concrete;
    [SerializeField]
    private AudioClip[] clips_wood;

    [SerializeField] private bool isWooden = false;
    [SerializeField] private bool isConcrete = false;


    private AudioSource audioSource;
    


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        FloorChecker.OnEnterWoodenFloor += FloorChecker_OnEnterWoodenFloor;
        FloorChecker.OnEnterConcreteFloor += FloorChecker_OnEnterConcreteFloor;
        FloorChecker.OnExitWoodenFloor += FloorChecker_OnExitWoodenFloor;
        FloorChecker.OnExitConcreteFloor += FloorChecker_OnExitConcreteFloor;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    public void ClearListeners()
    {
        FloorChecker.OnEnterWoodenFloor -= FloorChecker_OnEnterWoodenFloor;
        FloorChecker.OnEnterConcreteFloor -= FloorChecker_OnEnterConcreteFloor;
        FloorChecker.OnExitWoodenFloor -= FloorChecker_OnExitWoodenFloor;
        FloorChecker.OnExitConcreteFloor -= FloorChecker_OnExitConcreteFloor;
        //PuzzleInteract.OnPuzzleInteract -= PuzzleInteract_OnPuzzleInteract;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    void FloorChecker_OnExitConcreteFloor(object sender, EventArgs e)
    {
        isConcrete = false;
    }


    void FloorChecker_OnExitWoodenFloor(object sender, EventArgs e)
    {
        isWooden = false;
    }


    void FloorChecker_OnEnterConcreteFloor(object sender, EventArgs e)
    {
        isConcrete = true;
    }


    void FloorChecker_OnEnterWoodenFloor(object sender, EventArgs e)
    {
        isWooden = true;
    }


    private void Step()
    {
        if (isWooden)
        {
            AudioClip clip = GetRandomClipWooden();
            audioSource.clip = clip;
            audioSource.PlayOneShot(clip);
        }else if (isConcrete) 
        {
            AudioClip clip = GetRandomClipConcrete();
            audioSource.clip = clip;
            audioSource.PlayOneShot(clip);
        }
        else
        {
            AudioClip clip = GetRandomClipGrass();
            audioSource.clip = clip;
            audioSource.PlayOneShot(clip);
        }

       



    }

    private AudioClip GetRandomClipGrass()
    {
        return clips_grass[UnityEngine.Random.Range(0, clips_grass.Length)];
    }

    private AudioClip GetRandomClipWooden()
    {
        return clips_wood[UnityEngine.Random.Range(0, clips_wood.Length)];
    }

    private AudioClip GetRandomClipConcrete()
    {
        return clips_concrete[UnityEngine.Random.Range(0, clips_concrete.Length)];
    }
}
