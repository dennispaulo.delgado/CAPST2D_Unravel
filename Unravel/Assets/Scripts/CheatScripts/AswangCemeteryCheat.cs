﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AswangCemeteryCheat : MonoBehaviour
{
    private bool isOpen = false;
    public GameObject uiSheet;
    void Start()
    {
        uiSheet.SetActive(false);
        isOpen = false;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F5))
        {
            OpenOrCloseCheats();
        }
    }

    private void OpenOrCloseCheats()
    {
        if (isOpen)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1;
            uiSheet.SetActive(false);
            isOpen = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
            uiSheet.SetActive(true);
            isOpen = true;
        }
    }
}
