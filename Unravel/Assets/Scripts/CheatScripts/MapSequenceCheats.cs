﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSequenceCheats : MonoBehaviour
{
    private bool isOpen = false;
    public GameObject uiSheet;
    void Start()
    {
        uiSheet.SetActive(false);
        isOpen = false;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F5))
        {
            OpenOrCloseCheats();
        }
    }

    private void OpenOrCloseCheats()
    {
        if (isOpen)
        {
            Time.timeScale = 1;
            uiSheet.SetActive(false);
            isOpen = false;
        }
        else
        {
            Time.timeScale = 0;
            uiSheet.SetActive(true);
            isOpen = true;
        }
    }

    public void GetTourGuideHouseKey()
    {
        SaveManager.instance.stageData.playerHasTravelGuideKey = true;
    }
    public void GetJuanHouseKey()
    {
        SaveManager.instance.stageData.playerHasJuanHouseKey = true;
    }
    public void DefeatManananggalAndAswang()
    {
        SaveManager.instance.stageData.playerStopTheManananggal = true;
        SaveManager.instance.stageData.playerStopAswang = true;
        SaveManager.instance.daySequenceData.talkedAboutMangkukulam = true;
        SaveManager.instance.stageData.hasTheIncompleteAmulet = true;
    }
}
