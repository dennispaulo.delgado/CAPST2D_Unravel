﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSFX : MonoBehaviour
{

    [SerializeField] AudioClip universalPickupSound;
    [SerializeField] AudioClip keyPickupSound;

    private AudioSource audioSource;



    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void Pickup()
    {
        audioSource.clip = universalPickupSound;
        audioSource.PlayOneShot(universalPickupSound);
    }
}
