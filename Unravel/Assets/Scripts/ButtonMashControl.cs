﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMashControl : MonoBehaviour
{
    static public EventHandler OnButtonMashFinished;

    [SerializeField] private float barAmount = 0;
    [SerializeField] private float inputAmount = .5f;
    [SerializeField] private KeyCode key;
    private bool enableMash = false;

    // Start is called before the first frame update
    void Start()
    {
        SpecialSkill.OnManananggalSpecial += SpecialSkill_OnManananggalSpecial;
        GameEvents.instance.clearListeners += ClearListeners;
    }

    void SpecialSkill_OnManananggalSpecial(object sender, EventArgs e)
    {
        enableMash = true;
    }

    public void ClearListeners()
    {
        SpecialSkill.OnManananggalSpecial -= SpecialSkill_OnManananggalSpecial;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    // Update is called once per frame
    void Update()
    {
        if (enableMash)
        {

            barAmount -= Time.deltaTime/2;
            if(barAmount <= 0)
            {
                barAmount = 0;
            }

            if (Input.GetKeyDown(key))
            {
                Debug.Log("Mashing");
                barAmount += inputAmount;
            }

            if(barAmount >= 5)
            {
                OnButtonMashFinished?.Invoke(this, EventArgs.Empty);
                enableMash = false;
                barAmount = 0;
            }
        }

    }
}
