﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SimpleChangeScene : MonoBehaviour
{
    public static event EventHandler OnFadeIn;

    [SerializeField] RectTransform transitionBlack;

    Image fadeEffect;

    private void Awake()
    {
        fadeEffect = transitionBlack.GetComponent<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("Tutorial Level");
        }
    }

    public void ChangeSceneBaseOnString(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void TransitionFadeIn()
    {
        OnFadeIn?.Invoke(this, EventArgs.Empty);
        
    }

}
