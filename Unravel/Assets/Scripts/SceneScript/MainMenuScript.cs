﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public GameObject continueBut;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 1;

        if (continueBut != null)
        {
            if (SaveManager.instance.hasData && SaveManager.instance.daySequenceData.firstTalkToTheChief )
            {
                continueBut.SetActive(true);
            }
            else
            {
                continueBut.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartNewGame(string sceneName)
    {
        SaveManager.instance.NewGame();
        StartCoroutine("FadeOut", sceneName);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void GoToThisScene(string sceneName)
    {
        StartCoroutine("FadeOut", sceneName);
    }
    IEnumerator FadeOut(string sceneName)
    {
        yield return new WaitForSeconds(1.4f);
        SceneManager.LoadScene(sceneName);
    }

}
