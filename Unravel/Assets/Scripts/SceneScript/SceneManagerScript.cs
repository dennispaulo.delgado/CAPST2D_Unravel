﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public static SceneManagerScript instance;

    private StageData stageData;

    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }
    void Start()
    {
        GameEvents.instance.onSave += CheckScene;
        CheckForStageData();
        FadeScreen.instance.StartFadeInRoutine(2);
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            ChangeSceneToDay();
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            ChangeSceneToNight();
        }*/
    }


    private void CheckForStageData()
    {
        if (CheckIfStageHasData())
        {
            //Debug.Log("has data");
            StageHasData();
        }
        else
        {
            //Debug.Log("new  data");
            stageData = new StageData();
            stageData.stageName = SceneManager.GetActiveScene().name;
            SaveManager.instance.playerData.stageData.Add(stageData); 
        }
    }

    private bool CheckIfStageHasData()
    {
        for(int i = 0;i<SaveManager.instance.playerData.stageData.Count;i++)
        {
            if(SaveManager.instance.playerData.stageData[i].stageName == SceneManager.GetActiveScene().name)
            {
                return true;
            }
        }
        return false;
    }

    private void StageHasData()
    {
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManager.GetActiveScene().name)
            {
                stageData = SaveManager.instance.playerData.stageData[i];
            }
        }
    }

    public StageData ReturnStageData()
    {
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManager.GetActiveScene().name)
            {
                return stageData;
            }
        }
        return null;
    }

    public void ChangeSceneToDay()
    {
        SaveManager.instance.Save();
        SaveManager.instance.ClearListeners();
        SaveManager.instance.playerData.loadAllData = false;
        SceneManager.LoadScene("AlphaBuild_Day");
    }
    public void ChangeSceneToNight()
    {
        SaveManager.instance.Save();
        SaveManager.instance.ClearListeners();
        SaveManager.instance.playerData.loadAllData = false;
        SceneManager.LoadScene(0);
    }

    public void ChangeSceneBaseOnString(string sceneName)
    {
        SaveManager.instance.Save();
        SaveManager.instance.ClearListeners();
        SceneManager.LoadScene(sceneName);
    }

    public string ReturnSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void CheckScene()
    {
        for(int i = 0; i < SaveManager.instance.playerData.stageData.Count;i++)
        {
            if(SaveManager.instance.playerData.stageData[i].stageName == stageData.stageName)
            {
                SaveManager.instance.playerData.stageData[i] = stageData;
            }
        }
        /*
        for(int i = 0; i< SaveManager.instance.activeSave.stageData.Count;i++)
        {
            if(SceneManager.GetActiveScene().name == SaveManager.instance.activeSave.stageData[i].stageName)
            {
                Debug.Log("HasSameScene");
                SaveManager.instance.hasLoaded = true;
                return;
            }
        }
        string sceneName = SceneManager.GetActiveScene().name;
        StageData data = new StageData();
        data.stageName = sceneName;
        SaveManager.instance.activeSave.stageData.Add(data);
        */
    }
    public void RestartScene()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
