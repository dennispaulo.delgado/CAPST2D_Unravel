﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool hasTimeLimit;
    public float timeLeft;

    public Vector3 respawnPoint;

    public bool gameIsPaused = false;
    public bool equipmentHandIsBusy = false;

    private GameObject player;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        /*
        //Load is has save
        if(SaveManager.instance.hasLoaded)
        {
            //respawnPoint = SaveManager.instance.activeSave.respawnPosition;
        }*/
        Time.timeScale = 1;
    }

    private void Update()
    {
        if (hasTimeLimit)
        {
            if (timeLeft <= 0)
            {
                SurvivedTheStage();
                SceneManagerScript.instance.ChangeSceneToDay();
            }
            else
            {
                timeLeft -= Time.deltaTime;
            }
        }
    }

    public void SurvivedTheStage()
    {
        
    }

    public void OnPlayerDeath(GameObject plyr)
    {
        
        plyr.GetComponent<ThirdPersonMovement>().canMove = false;
        player = plyr;
        FadeScreen.instance.StartFadeOutRoutine(1.5f);
        StartCoroutine("DespawnPlayer", plyr);
    }
    
    public void RespawnPlayer()
    {
        

        FadeScreen.instance.StartFadeInRoutine(1f);
        player.SetActive(true);
        player.GetComponent<ThirdPersonMovement>().canMove = true;
    }

    IEnumerator DespawnPlayer(GameObject player)
    {

        yield return new WaitForSeconds(2f);
        GameEvents.instance.OnLoad();
        player.GetComponent<HealthScript>().ClearBurning();
        player.GetComponent<HealthScript>().HealUnit(player.GetComponent<HealthScript>().maxHealth);
        player.SetActive(false);
        GameOver.instance.OnGameOver();
    }
    IEnumerator RespawnPlayer(GameObject player)
    {
        yield return new WaitForSeconds(2f);
        player.SetActive(true);
        player.GetComponent<HealthScript>().ClearBurning();
        player.GetComponent<ThirdPersonMovement>().canMove = true;
        GameEvents.instance.OnLoad();
    }
}
