﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseElements;
    public List<ButtonSelect> buttons;
    private bool isPaused = false;
    void Start()
    {
        pauseElements.SetActive(false);
        isPaused = GameManager.instance.gameIsPaused;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            PauseOrResumeGame();
        }
    }

    public void PauseOrResumeGame()
    {
        if (isPaused)
        {
            if(PlayersData.instance.sceneHasPlayer)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            Time.timeScale = 1;
            isPaused = false;
            pauseElements.SetActive(false);
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
            isPaused = true;
            pauseElements.SetActive(true);
            UnselectButtons();
        }
    }

    private void UnselectButtons()
    {
        foreach(ButtonSelect b in buttons)
        {
            b.OnMouseExitChange();
        }
    }

    public void MainMenuButton()
    {
        GameEvents.instance.ClearListeners();
        SceneManager.LoadScene("MainMenuProto");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
