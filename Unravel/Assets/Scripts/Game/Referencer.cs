﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Referencer : Singleton<Referencer>
{
    public JournalUIScript journalUI;
    public TogglePhone aimToPhone;
    public PhoneUI phoneUI;
    public CollectBehaviour colBeh;
    public Flashlight flashlight;
    public PlayerLoadout playerLoadout;
    public Radar radar;
    public GameObject radarUI;
    public MiniMapUI miniMapUI;
}
