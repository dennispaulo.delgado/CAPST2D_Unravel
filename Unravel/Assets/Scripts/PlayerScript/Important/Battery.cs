﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Battery
{
    public float currentCharge;
    public float maxCharge;
}
