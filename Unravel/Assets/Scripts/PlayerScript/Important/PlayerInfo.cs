﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    public static PlayerInfo instance;

    [Header("Manananggal")]
    public bool mImageDiscover = true;
    public bool mNameDiscover = true;
    public bool mDescriptionDiscover = true;
    [Header("Manananggal Strengths")]
    public bool mStrengthDiscover1 = true;
    public bool mStrengthDiscover2 = true;
    public bool mStrengthDiscover3 = true;
    [Header("Manananggal Weaknesses")]
    public bool mWeaknessesDiscover1 = true;
    public bool mWeaknessesDiscover2 = true;
    public bool mWeaknessesDiscover3 = true;
    [Header("Mangkukulam")]
    public bool mangImageDiscover = true;
    public bool mangNameDiscover = true;
    public bool mangDescriptionDiscover = true;
    [Header("Mangkukulam Strengths")]
    public bool mangStrengthDiscover1 = true;
    public bool mangStrengthDiscover2 = true;
    public bool mangStrengthDiscover3 = true;
    [Header("Mangkukulam Weaknesses")]
    public bool mangWeaknessesDiscover1 = true;
    public bool mangWeaknessesDiscover2 = true;
    public bool mangWeaknessesDiscover3 = true;
    [Header("Ghoul")]
    public bool gImageDiscover = true;
    public bool gNameDiscover = true;
    public bool gDescriptionDiscover = true;
    [Header("Ghoul Strengths")]
    public bool gStrengthDiscover1 = true;
    public bool gStrengthDiscover2 = true;
    public bool gStrengthDiscover3 = true;
    [Header("Ghoul Weaknesses")]
    public bool gWeaknessesDiscover1 = true;
    public bool gWeaknessesDiscover2 = true;
    public bool gWeaknessesDiscover3 = true;

    private void Start()
    {
        instance = this;
    }
}
