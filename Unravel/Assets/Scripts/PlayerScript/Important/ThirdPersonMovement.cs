﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public Animator animator;
    public Flashlight flashlight;

    public float wlkSpeed = 5f;
    public float sprntSpeed = 8f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public bool canSprint = true;
    public bool isSprinting = false;
    public bool canMove = true;

    private float horizontal;
    private float vertical;
    [SerializeField] private float verticalDragSpeed;
    [SerializeField] private float horizontalDragSpeed;
    [SerializeField] private float walkDragSpeed = 1;
    

    public GameObject instructionCanvas;

    [Header("Stamina")]
    public float curStamina;
    public float maxStamina;
    public float staminaConsume;
    public float staminaRegen;
    private bool hasInfiniteStamina = false;

    // Start is called before the first frame update
    void Start()
    {
        Puzzle.OnPuzzleTypeShelf += Puzzle_OnPuzzleTypeShelf;
        Puzzle.OnPuzzleTypePressure += Puzzle_OnPuzzleTypePressure;
        PuzzleInteract.OnRelease += PuzzleInteract_OnRelease;
        
    }

    private void PuzzleInteract_OnRelease(object sender, PuzzleInteract.OnReleaseArgs e)
    {
        verticalDragSpeed = e.speed;
        horizontalDragSpeed = e.speed;
        walkDragSpeed = e.speed;
        canSprint = true;
    }

    private void Puzzle_OnPuzzleTypePressure(object sender, Puzzle.OnPuzzleTypePressureArgs e)
    {
        verticalDragSpeed = e.verticalMovement;
        horizontalDragSpeed = e.horizontalMovement;
        walkDragSpeed = e.walkMovement;
        canSprint = e.canSprint;
        
    }

    private void Puzzle_OnPuzzleTypeShelf(object sender, Puzzle.OnPuzzleTypeShelfArgs e)
    {
        verticalDragSpeed = e.verticalMovement;
        horizontalDragSpeed = e.horizontalMovement;
        walkDragSpeed = e.walkMovement;
        canSprint = e.canSprint;
    }

    public void CancelSprint()
    {
        canSprint = false;
        isSprinting = false;
    }

    public void RegenerateStamina()
    {
        if(curStamina<maxStamina)
        {
            curStamina += staminaRegen * Time.deltaTime;
        }
        if(curStamina>maxStamina)
        {
            curStamina = maxStamina;
        }
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal * horizontalDragSpeed, 0f, vertical * verticalDragSpeed).normalized;

        //Debug.Log(direction);


        if (Input.GetKeyDown(KeyCode.LeftShift) && canSprint == true)
        {
            if (curStamina > 0)
            {
                isSprinting = true;
            }
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            isSprinting = false;
        }
        if (direction.magnitude>=0.1f&&canMove)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg+cam.eulerAngles.y; //Atan2 is to get the angle based on the characters direction.
            //Debug.Log("TAngle : " + targetAngle);
            if(!isSprinting)
            {
                RegenerateStamina();
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, cam.eulerAngles.y, ref turnSmoothVelocity, turnSmoothTime); //this is to smoothen the rotation.
                transform.rotation = Quaternion.Euler(0f, angle, 0f);// executes the rotation.

                //Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;//gets the direction based on the atan2
                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                controller.Move(new Vector3(moveDir.x, -1f, moveDir.z).normalized * (wlkSpeed * walkDragSpeed) * Time.deltaTime);//executes movement
            }
            else if(isSprinting)
            {
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                //controller.Move(moveDir.normalized * sprntSpeed * Time.deltaTime);
                controller.Move(new Vector3(moveDir.x, -1f, moveDir.z).normalized * sprntSpeed * Time.deltaTime);
                if (curStamina > 0)
                {
                    PlayerConsumingStamina();
                }
                else
                {
                    isSprinting = false;
                }
            }

        }
        else
        {
            controller.Move(new Vector3(0f, -1f, 0f).normalized * sprntSpeed * Time.deltaTime);
            RegenerateStamina();
        }
        //HandleAnimation(direction);
        HandleAnimation_2(direction);
    }

    public void GiveInfiniteStaminaOrNot()
    {
        if(hasInfiniteStamina)
        {
            hasInfiniteStamina = false;
        }
        else
        {
            hasInfiniteStamina = true;
        }
    }

    private void PlayerConsumingStamina()
    {
        if (!hasInfiniteStamina)
        {
            curStamina -= staminaConsume * Time.deltaTime;
        }
        StaminaHUD.instance.ShowStaminaBar(this);
    }    

    public void CanPlayerMove(bool resMove)
    {
        canMove = resMove;
    }

    public void HandleAnimation(Vector3 dir)
    {

        if (canMove)
        {
            animator.SetFloat("Speed", dir.magnitude);
            if (isSprinting)
            {
                animator.SetFloat("Speed", dir.magnitude);
                animator.SetBool("isSprinting", true);
                animator.SetBool("isStrafe", false);
            }
            else
            {
                animator.SetBool("isSprinting", false);
                if (dir.z != 0 && dir.x != 0)
                {
                    if ((dir.z >= .7 && dir.x >= .7) || (dir.z <= -0.7 && dir.x >= 0.7))
                    {
                        animator.SetFloat("Speed", 1);
                        animator.SetBool("isStrafe", true);
                    }
                    else
                    {
                        animator.SetFloat("Speed", -1);
                        animator.SetBool("isStrafe", true);
                    }
                }
                else if (dir.z > 0.1)
                {
                    animator.SetFloat("Speed", dir.z);
                    animator.SetBool("isStrafe", false);
                }
                else if (dir.z < -0.1)
                {
                    animator.SetFloat("Speed", dir.z);
                    animator.SetBool("isStrafe", false);
                }
                else if (dir.x > 0.1 && !isSprinting)
                {
                    animator.SetFloat("Speed", dir.x);
                    animator.SetBool("isStrafe", true);
                }
                else if (dir.x < -0.1 && !isSprinting)
                {
                    animator.SetFloat("Speed", dir.x);
                    animator.SetBool("isStrafe", true);
                }
                else
                {
                    animator.SetFloat("Speed", dir.magnitude);
                    animator.SetBool("isStrafe", false);
                }
            }
        }
        else
        {
            animator.SetFloat("Speed", 0);
            animator.SetBool("isStrafe", false);
            animator.SetBool("isSprinting", false);
        }
    }

    public void HandleAnimation_2(Vector3 dir)
    {
        if (canMove)
        {
            animator.SetFloat("Horizontal", dir.x);
            animator.SetFloat("Vertical", dir.z);

            if (isSprinting && (vertical > 0 || vertical < 0))
            {
                animator.SetFloat("Vertical", 1.5f);
            }
        }
        
    }


   
}
