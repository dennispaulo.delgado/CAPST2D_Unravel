﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public bool isTesting = false;

    private Light lightSource;
    private SphereCollider sCollider;
    public LayerMask maskSurface;

    public float consume;
    public float flaMaxBattery;
    public float flaCurBattery;

    public int howManyFlicker;
    private int defFlicker;
    public bool isOn;
    public bool flickering = false;
    void Start()
    {
        Referencer.Instance.flashlight = this;
        lightSource = GetComponent<Light>();
        sCollider = GetComponent<SphereCollider>();
        defFlicker = howManyFlicker;
        sCollider.enabled = false;
    }


    void Update()
    {
        if(isOn&&!flickering&&!isTesting)
        {
            LightCollider();
            flaCurBattery -= consume * Time.deltaTime;
            if(flaCurBattery <= 0)
            {
                flaCurBattery = 0;
                flickering = true;
                StartCoroutine("FlickerToOff");
            }
        }
        else if(!isOn)
        {
            lightSource.enabled = false;
        }
    }

    public void DrainBattery()
    {
        flaCurBattery = 5f;
    }

    public void LightCollider()
    {
        RaycastHit hit;
        Physics.Raycast(this.transform.position, this.transform.forward, out hit, lightSource.range, maskSurface);
        Debug.DrawRay(this.transform.position, this.transform.forward* lightSource.range, Color.yellow);
        if(hit.collider)
        {
            sCollider.enabled = true;
            float distance = Vector3.Distance(this.transform.position, hit.point);
            //Debug.Log("Distance : " + distance);
            //Debug.Log("Collider : " + hit.collider.name);
            sCollider.center = new Vector3(0, 0, distance - 1.39f);
            //Debug.Log("Radius : " + (distance / (lightSource.range - 1.39f)) * 8.6f);
            sCollider.radius = (distance / (lightSource.range - 1.39f)) * 8.6f;
        }
        else
        {
            sCollider.enabled = false;
        }

    }

    public void ReloadFlashlight()
    {
        Inventory inv = Referencer.Instance.colBeh.GetInventory();
        for (int i = 0; i < inv.GetInventoryList().Count; i++)
        {
            if (inv.GetInventoryList()[i].interactable.itemType == Item.ItemType.Battery)
            {
                Item nItem = new Item();
                nItem = inv.GetInventoryList()[i].ReturnDeepCopy();
                flaCurBattery = nItem.interactable.curBatCharge;
                flaMaxBattery = nItem.interactable.maxBatCharge;
                inv.RemoveItem(inv.GetInventoryList()[i]);
                break;
            }
        }
        if (flickering)
        {
            StopCoroutine("FlickerToOff");
            if (isOn)
            {
                lightSource.enabled = true;
            }
            else
            {
                lightSource.enabled = false;
            }
            howManyFlicker = defFlicker;
            flickering = false;
        }
    }

    public void FlashLightSwitch()
    {
        if (flaCurBattery > 0&&!flickering)
        {
            if (lightSource.enabled)
            {
                isOn = false;
                lightSource.enabled = false;
            }
            else
            {
                isOn = true;
                lightSource.enabled = true;
            }
        }
    }

    IEnumerator FlickerToOff()
    {
        yield return new WaitForSeconds(0.5f);
        if (lightSource.enabled)
        {
            lightSource.enabled = false;
        }
        else
        {
            lightSource.enabled = true;
        }
        if (howManyFlicker<=0)
        {
            lightSource.enabled = false;
            howManyFlicker = defFlicker;
            isOn = false;
            flickering = false;
            sCollider.enabled = false;
        }
        else
        {
            howManyFlicker -= 1;
            StartCoroutine("FlickerToOff");
        }
    }
}
