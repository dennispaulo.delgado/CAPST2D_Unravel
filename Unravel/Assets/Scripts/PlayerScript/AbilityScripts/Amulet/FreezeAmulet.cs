﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Amulet/FreezeAmulet")]
public class FreezeAmulet : Amulet
{
    public GameObject amuletEffect;
    private Transform projectileDestination;
    private Transform origin;
    public override void GetPlayerAbilities(GameObject obj)
    {
        //projectileDestination = obj.transform.GetComponent<PlayerLoadout>().projectileDirection;
        origin = obj.transform;  //GetComponent<PlayerLoadout>().firePointAmulet;
        TriggerScroll();
    }

    public override void TriggerScroll()
    {
        GameObject freeze = Instantiate(amuletEffect, origin.position, Quaternion.identity);
        //Vector3 direction = projectileDestination.position - origin.position;
        freeze.GetComponent<Freeze>().playerDirection = origin.parent.eulerAngles;
        //freeze.GetComponent<Freeze>().direction = origin.transform.forward + direction.normalized;
    }
}
