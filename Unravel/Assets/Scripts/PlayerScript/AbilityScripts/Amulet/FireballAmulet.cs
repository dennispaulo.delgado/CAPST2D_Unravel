﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Amulet/FireballAmulet")]
public class FireballAmulet : Amulet
{
    public GameObject fireBall;
    private Transform projectileDestination;
    private Transform origin;

    public override void GetPlayerAbilities(GameObject obj)
    {
        projectileDestination = obj.transform.GetComponent<PlayerLoadout>().projectileDirection;
        origin = obj.transform.GetComponent<PlayerLoadout>().firePointAmulet;
        TriggerScroll();
    }

    public override void TriggerScroll()
    {
        GameObject fBall = Instantiate(fireBall, origin.position, origin.rotation);
        Vector3 direction = projectileDestination.position - origin.position;
        //fBall.GetComponent<Fireball>().direction = origin.transform.forward + direction.normalized;
    }
}
