﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(menuName = "Abilities/ForceAbility")]
public class ForceAbility : Ability
{
    Transform origin;
    public float length;
    public float radius;
    public float power;
    [SerializeField] public LayerMask maskLayer;
    public GameObject particle;

    public override void GetPlayerAbilities(GameObject obj)
    {
        origin = obj.transform;
        TriggerAbility();
    }

    public override void TriggerAbility()
    {
        GameObject p = Instantiate(particle, origin.transform.position, origin.transform.rotation);
        p.GetComponent<ForcePalmVisuals>().direction = origin.transform.forward;
        Vector3 p1 = origin.transform.position;
        Vector3 p2 = p1 + origin.transform.forward* length;
        RaycastHit[] hits = Physics.CapsuleCastAll(p1, p2, radius, origin.transform.forward, length, maskLayer);
        if(hits.Length>0)
        {
            for(int i = 0; i<hits.Length;i++)
            {
                /*
                Vector3 pos = new Vector3(hits[i].transform.position.x * hits[i].transform.GetComponent<BoxCollider>().center.x,
                    hits[i].transform.position.y * hits[i].transform.GetComponent<BoxCollider>().center.y,
                    hits[i].transform.position.z * hits[i].transform.GetComponent<BoxCollider>().center.z);
                GameObject p = Instantiate(particle, pos, hits[i].transform.rotation);
                */
                
                Destroy(hits[i].collider.gameObject);
                /*
                Rigidbody rb = hits[i].collider.gameObject.GetComponent<Rigidbody>();
                rb.AddForce(origin.transform.forward* power);
                Debug.Log(origin.transform.forward * power);
                */
            }
        }
        canUseAblty = false;
    }

    public override void PassiveAbility(GameObject obj)
    {
    }
}
