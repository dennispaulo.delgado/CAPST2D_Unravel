﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public string aName = "ability name";
    public float aCooldown; // cooldownduration, the time that is counting down.
    public float dCooldown; // default cooldown
    public float cCooldown; // currenttime if change type cooldown will be saved to here; 
    public float manaCost;
    public bool canUseAblty;
    public bool isComplicated;
    public AudioClip aSound;

    public abstract void PassiveAbility(GameObject obj);
    public abstract void GetPlayerAbilities(GameObject obj);//To Get a Reference Object
    public abstract void TriggerAbility();//Execute Skill
    
}
