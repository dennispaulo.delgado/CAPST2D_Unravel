﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleForceScript : MonoBehaviour
{
    public Transform cam;
    public CinemachineFreeLook cinema;
    public GameObject objHandled;
    public Vector3 objHandledSize;
    public Transform player;
    public Rigidbody rb;
    private CharacterController controller;


    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public bool canPlace;
    private bool forcePlace;
    private Collider[] snapCol;
    public LayerMask snapLayer;

    private ObsForceObj objectForceScript;

    public bool isActive = false;
    public Vector3 orgPosition = new Vector3(0, 0, 1.4f);
    public Vector3 distanceLimit = new Vector3(5,5,5);

    public Vector2 topRig;
    public Vector2 midRig;
    public Vector2 botRig;
    public Vector2 midOffSet;
    public Vector2 topNbotOffset;

    private Vector2 ogTopRig;
    private Vector2 ogMidRig;
    private Vector2 ogBotRig;
    private Vector2 ogMidOffSet;
    private Vector2 ogTopNbotOffset;
    private float ogCineSpeed;
    void OnDrawGizmos()
    {
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        controller.enabled = false;
    }

    void Update()
    {
        if (isActive)
        {
            Debug.Log("Look : " + cinema.m_LookAt);
            Debug.Log("Follow : " + cinema.m_Follow);
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
            float zinput = Input.GetAxisRaw("Mouse Y");
            Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;
            Vector3 zDirection = new Vector3(0, zinput, 0).normalized;
            if (direction.magnitude >= 0.1)
            {
                
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y; 
                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                //transform.Translate(moveDir.x * 10 * Time.deltaTime, 0, moveDir.z * 10 * Time.deltaTime);
                controller.Move(new Vector3(moveDir.x, 0, moveDir.z).normalized * 10 * Time.deltaTime);
            }
            if (zDirection.magnitude >= 0.1)
            {
                transform.Translate(0, zDirection.y * 10 * Time.deltaTime, 0);
            }

            transform.position = new Vector3(Mathf.Clamp(transform.position.x, player.transform.position.x - distanceLimit.x, player.transform.position.x + distanceLimit.x),
                Mathf.Clamp(transform.position.y, player.transform.position.y - distanceLimit.y, player.transform.position.y + distanceLimit.y),
                Mathf.Clamp(transform.position.z, player.transform.position.z - distanceLimit.z, player.transform.position.z + distanceLimit.z));

            snapCol = Physics.OverlapBox(this.transform.position, objHandledSize / 2, objHandled.transform.rotation, snapLayer);
            if (snapCol.Length > 0)
            {
                Transform closes = snapCol[0].transform;
                if (snapCol.Length > 1)
                {
                    for (int i = 1; i < snapCol.Length; i++)
                    {
                        if (Vector3.Distance(this.transform.position, closes.position) > Vector3.Distance(this.transform.position, snapCol[i].transform.position))
                        {
                            closes = snapCol[i].transform;
                        }
                    }
                }
                forcePlace = true;
                objectForceScript.forcePlace = forcePlace;
                canPlace = true;
                objHandled.transform.position = closes.position;
            }
            else
            {
                forcePlace = false;
                objectForceScript.forcePlace = forcePlace;
                objHandled.transform.position = Vector3.MoveTowards(objHandled.transform.position, this.transform.position, 3f);
            }

            if(!forcePlace)
            {
                canPlace = objectForceScript.canPlace;
            }
        }
    }
   
    public void ChainObject(Transform camera, GameObject objH)
    {
        controller.enabled = true;
        objectForceScript = objH.GetComponent<ObsForceObj>();
        objectForceScript.isActive = true;
        objectForceScript.BeginChain();
        rb.isKinematic = true;
        isActive = true; // activate objecthandler

        cam = camera;
        objHandled = objH;//to control object transform

        SetCinamachineValues();
        CinemachineIn();
        this.transform.position = objHandled.transform.position;
    }

    public void SetCinamachineValues()
    {
        ogTopRig = new Vector2(cinema.m_Orbits[0].m_Height, cinema.m_Orbits[0].m_Radius);
        ogMidRig = new Vector2(cinema.m_Orbits[1].m_Height, cinema.m_Orbits[1].m_Radius);
        ogBotRig = new Vector2(cinema.m_Orbits[2].m_Height, cinema.m_Orbits[2].m_Radius);
        ogMidOffSet = new Vector2(cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.x,
            cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y);
        ogTopNbotOffset = new Vector2(cinema.GetRig(0).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y,
            cinema.GetRig(2).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y);
        ogCineSpeed = cinema.m_YAxis.m_MaxSpeed;
    }

    public void CinemachineIn()
    {
        cinema.m_LookAt = this.gameObject.transform;
        cinema.m_Follow = this.gameObject.transform;
        cinema.m_YAxis.Value = 0.8f;
        cinema.m_YAxis.m_MaxSpeed = 0f;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = midOffSet.y;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.x = midOffSet.x;
        cinema.m_Orbits[0].m_Height = topRig.x;
        cinema.m_Orbits[0].m_Radius = topRig.y;
        cinema.m_Orbits[1].m_Height = midRig.y;
        cinema.m_Orbits[1].m_Radius = midRig.y;
        cinema.m_Orbits[2].m_Height = botRig.y;
        cinema.m_Orbits[2].m_Radius = botRig.y;
    }

    public void CinemachineOut()
    {

        cinema.m_YAxis.m_MaxSpeed = ogCineSpeed;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = ogMidOffSet.y;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.x = ogMidOffSet.x;
        cinema.GetRig(0).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = ogTopNbotOffset.x;
        cinema.GetRig(2).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = ogTopNbotOffset.y;
        cinema.m_LookAt = player;
        cinema.m_Follow = player;
        cinema.m_Orbits[0].m_Height = ogTopRig.x;
        cinema.m_Orbits[0].m_Radius = ogTopRig.y;
        cinema.m_Orbits[1].m_Height = ogMidRig.y;
        cinema.m_Orbits[1].m_Radius = ogMidRig.y;
        cinema.m_Orbits[2].m_Height = ogBotRig.y;
        cinema.m_Orbits[2].m_Radius = ogBotRig.y;
    }

    public void UnChainObject()
    {
        controller.enabled = false;
        objectForceScript.isActive = false;
        objectForceScript.EndChain();
        isActive = false;
        //StartCoroutine("RestoreGravity");
        //StartCoroutine("RemoveVelocity");
        CinemachineOut();
        cam = null;
        objHandled = null;
        objectForceScript = null;

        this.transform.localPosition = orgPosition;
    }
    IEnumerator RemoveVelocity()
    {
        yield return new WaitForSeconds(.1f);
        objHandled.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    IEnumerator RestoreGravity()
    {
        yield return new WaitForSeconds(5f);
        objHandled.GetComponent<Rigidbody>().useGravity = true;
        objHandled.GetComponent<Rigidbody>().freezeRotation = false;
        objHandled = null;
    }

}
