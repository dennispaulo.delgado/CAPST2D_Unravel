﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/DefyGravityAbility")]
public class DefyGravityAbility : Ability
{
    [SerializeField] public LayerMask maskLayer;
    Transform cam;
    private PlayerLoadout ablty;
    public override void GetPlayerAbilities(GameObject obj)
    {
        cam = obj.GetComponent<ThirdPersonMovement>().cam.transform;
        ablty = obj.GetComponent<PlayerLoadout>();
        TriggerAbility();
    }

    public override void PassiveAbility(GameObject obj)
    {
    }

    public override void TriggerAbility()
    {
        //ablty.DefyGravity(cam, maskLayer);//purged
    }
}
