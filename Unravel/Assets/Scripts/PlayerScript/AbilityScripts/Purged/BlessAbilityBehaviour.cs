﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlessAbilityBehaviour : MonoBehaviour
{
    static public event EventHandler OnBlindUnit;

    [SerializeField] private Transform pfBlindEffect;

    private Transform pulseTransform;
    private float range;
    private float rangeMax;
    private bool isActive;
    private List<Collider> alreadyPingedColliderList;

    private void Awake()
    {
        isActive = false;
        pulseTransform = transform.Find("pulse");
        alreadyPingedColliderList = new List<Collider>();
        Hide();
    }

    // Start is called before the first frame update
    void Start()
    {
        rangeMax = 100f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            Pulse();
        }



    }

    private void Pulse()
    {
        float pulseRate = 20f;
        range += pulseRate * Time.deltaTime;
        if (range > rangeMax)
        {
            range = 0f;
            alreadyPingedColliderList.Clear();
        }

        pulseTransform.localScale = new Vector3(range, range, range);

        RaycastHit[] hitArray = Physics.SphereCastAll(transform.position, range / 2f, Vector3.forward);

        foreach (RaycastHit hit in hitArray)
        {
            if (hit.collider != null)
            {
                //hit something
                if (!alreadyPingedColliderList.Contains(hit.collider))
                {
                    if (hit.collider.GetComponent<Enemy>())
                    {
                        alreadyPingedColliderList.Add(hit.collider);
                        Debug.Log("I blinded an enemy");
                        //Transform blindEffectTransform = Instantiate(pfBlindEffect, hit.transform.position, Quaternion.identity);
                        //PingBehvaiour blindPing = blindEffectTransform.GetComponent<PingBehvaiour>();
                        OnBlindUnit?.Invoke(this, EventArgs.Empty);
                        //pulsePing.SetDisappearTimer(rangeMax / pulseRate);

                    }

                }
            }
        }



    }

    public void Show()
    {
        SetIsActive(true);
        pulseTransform.gameObject.SetActive(true);
    }

    public void Hide()
    {
        SetIsActive(false);
        range = 0;
        alreadyPingedColliderList.Clear();
        pulseTransform.gameObject.SetActive(false);
    }

    public bool GetIsActive()
    {
        return isActive;
    }

    public void SetIsActive(bool active)
    {
        isActive = active;
    }
}

