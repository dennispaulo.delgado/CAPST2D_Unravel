﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/HealAbility")]
public class HealAbility : Ability
{
    private PlayerLoadout ablty;
    public GameObject healAbilityPrefab;
    public GameObject healParticlePrefab;
    private GameObject particle;
    public float howMuchHeal;
    public float howLongHeal;

    public override void GetPlayerAbilities(GameObject obj)
    {
        ablty = obj.GetComponent<PlayerLoadout>();
        ablty.isBusy = true;
        particle = Instantiate(healParticlePrefab, obj.transform.position, obj.transform.rotation, obj.transform);
        canUseAblty = false;
        TriggerAbility();
    }

    public override void TriggerAbility()
    {
        //ablty.CastHeal(particle,healAbilityPrefab,howMuchHeal,howLongHeal);
    }

    public override void PassiveAbility(GameObject obj)
    {
    }
}
