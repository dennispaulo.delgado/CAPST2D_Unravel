﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLoadout : MonoBehaviour
{
    public bool isBusy = false;
    public bool isTesting = false;

    //public GameObject highLighted;
    //public Material hLightedOrigMat;
    private CollectBehaviour colBeh;
    private Inventory pInventory;

    [Tooltip("In Right hand Mixamo Rig")] public Transform firePointAmulet;
    [Tooltip("Crosshair Target in Camera")] public Transform projectileDirection;
    public EquipableLoadout currentEquipped;
    public EquipableLoadout NoLoad;

    void Start()
    {
        Referencer.Instance.playerLoadout = this;
        colBeh = transform.parent.GetComponent<CollectBehaviour>();
        pInventory = colBeh.GetInventory();
        currentEquipped.GetReference(this.gameObject);
        currentEquipped.OnEquipped();
    }

    
    public void ReloadEquipment()
    {
        currentEquipped.OnReloadEquipment();
    }

    public void ExecuteEquipment()
    {
        currentEquipped.OnExecute();
    }

    public void OnToggle()
    {
        currentEquipped.OnUse();
    }

    public Inventory ReturnInventory()
    {
        return pInventory;
    }
    

}
