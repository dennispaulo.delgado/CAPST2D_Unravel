﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using System;

public class Fireball : MonoBehaviour
{
    public static EventHandler OnFireballHit;
    public static EventHandler OnFireballShot;

    public float duration;
    private Vector3 direction;
    private Vector3 particleDirection;
    public Vector3 particleSpeed = new Vector3(-10,0.5f,0);
    public float speed;
    public VisualEffect vfx;
    [SerializeField] public LayerMask maskLayer;
    public float power;
    public float blastRadius;
    public GameObject explosionPrefab;

    private Transform player;
    [SerializeField] private Gun myGun;
    private Transform firePoint;
    private void Awake()
    {
        player = FindObjectOfType<ThirdPersonAimControl>().transform;
        myGun = player.transform.Find("WeaponSlot").transform.Find("WeaponHolder").transform.Find("WeaponPivot").GetComponentInChildren<Gun>();

        firePoint = myGun.transform.Find("firePoint");

        direction = myGun.GetProjectileDirection().position - firePoint.transform.position;
        direction = direction.normalized;
    }
    void Start()
    {
        OnFireballShot?.Invoke(this, EventArgs.Empty);

        particleDirection = new Vector3(direction.x* particleSpeed.x, direction.y+ particleSpeed.y, direction.z*particleSpeed.z);
        vfx.SetVector3("Force", particleDirection);
    }

    void Update()
    {
        if (duration <= 0)
        {
            Explosion();
        }
        else
        {
            transform.position += direction * speed * Time.deltaTime;
        }
    }

    public void Explosion()
    {

        Collider[] col = Physics.OverlapSphere(this.transform.position, blastRadius, maskLayer);
        if(col.Length>0)
        {
            foreach (Collider c in col)
            {
                //Debug.Log("Fireball detected collision" + c.gameObject.name);
                if (c.GetComponent<Enemy>()!=null)
                {
                    //Debug.Log("Fireball Detected Enemy");
                    OnFireballHit?.Invoke(this, EventArgs.Empty);
                }
                if(c.GetComponent<HitPuzzle>()!=null)
                {
                    c.GetComponent<HitPuzzle>().PuzzleHit();
                }
            }
        }
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }

    public Vector3 ForceAwayFromOrigin(Transform colPos)
    {
        Vector3 m = this.transform.position - colPos.position;

        return -m.normalized;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Explosion();
    }
}
