﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{
    private float howLong;
    public float howLongTimer;
    public float howMuch;

    private HealthScript hScript;

    void Start()
    {
        howLong = howLongTimer;
    }

    void Update()
    {
        if(hScript != null&&howLongTimer>0)
        {
            float healPerSecond = (howMuch / howLong);
            hScript.HealUnit(healPerSecond*Time.deltaTime);
        }
        else if(howLongTimer <= 0)
        {
            Destroy(this.gameObject);
        }
        howLongTimer -= Time.deltaTime;
    }

    public void SetPrivates(HealthScript h,Transform t)
    {
        hScript = h;
        this.transform.GetComponent<FollowTargetScript>().target = t;
    }

    public void Constructer(float duration, float amount)
    {
        howLong = duration;
        howMuch = amount;
        howLongTimer = howLong;
    }
}
