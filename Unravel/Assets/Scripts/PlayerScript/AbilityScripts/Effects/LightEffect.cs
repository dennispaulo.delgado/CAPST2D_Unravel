﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using System;

public class LightEffect : MonoBehaviour
{
    public static EventHandler OnLightHit;

    public float duration;
    public float effectRadius;
    public VisualEffect vfx;
    private List<Collider> colliders;
    [SerializeField] public LayerMask maskLayer;
    [SerializeField] public LayerMask whatIsGround;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, effectRadius);
    }
    void Start()
    {
        vfx.SetFloat("RadiusEffect", effectRadius);
    }

    public void SetPosition()
    {
        RaycastHit r;
        Physics.Raycast(this.transform.position, Vector3.down, out r, 10f, whatIsGround);
        if(r.collider)
        {
            this.transform.position = r.point;
        }
    }

    public void LightEnemies()
    {
        Collider[] col = Physics.OverlapSphere(this.transform.position, effectRadius,maskLayer);
        foreach(Collider c in col)
        {
            /*
            if(!colliders.Contains(c))
            {
                colliders.Add(c);
                //Stun Function
            }*/

            if (c.GetComponent<Enemy>())
            {
                OnLightHit?.Invoke(this, EventArgs.Empty);
            }
        }
    }

    void Update()
    {
        if (duration > 0)
        {
            LightEnemies();
            duration -= Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
