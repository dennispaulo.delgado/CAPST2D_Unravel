﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Animations.Rigging;
using Cinemachine;


public class OnAim : UnityEvent<ThirdPersonAimControl> { }


public class ThirdPersonAimControl : MonoBehaviour
{
    private Vector2 m_Camera;
    [SerializeField] private float mouseSensitivity = 1;
    private Transform cam;
    [SerializeField] private Animator anim;
    [SerializeField] private Transform followTarget;
    [SerializeField] private Transform weaponSlot;
    [SerializeField] private Transform weaponDummy;
    [SerializeField] private Transform crosshairUI;
    private Transform spineRotation;
    [SerializeField] private Gun weapon;
    public bool isWeaponActive = false;
    public bool isAmuletActive = false;
    private CinemachineFreeLook thirdPersonCamera;
    private CinemachineVirtualCamera aimCamera;
    [SerializeField] private bool isAiming = false;
    private bool isActive = false;
    private float rigBuild = 3f;

    [SerializeField] private Rig rigBodyAim;
    [SerializeField] private Rig rigWeaponAim;



    private void Awake()
    {

        //CinemachineCore.GetInputAxis = GetAxisCustom;
        Cursor.lockState = CursorLockMode.Locked;
        //followTarget = transform.Find("FollowTarget");

        weapon = GetComponentInChildren<Gun>();
        cam = FindObjectOfType<Camera>().GetComponent<Transform>();
        thirdPersonCamera = FindObjectOfType<ThirdPersonView>().GetComponent<CinemachineFreeLook>();
        aimCamera = FindObjectOfType<AimViewCam>().GetComponent<CinemachineVirtualCamera>();

       //aimCamera.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!isWeaponActive)
        {
            //weapon.gameObject.SetActive(false);
            weapon.transform.GetChild(0).gameObject.SetActive(false);
        }
        Gun existingWeapon = GetComponentInChildren<Gun>();
        if (existingWeapon)
        {
            Equip(existingWeapon);
        }
        //rigWeaponAim.weight = 0f;
        //rigBodyAim.weight = 0f;

        isAiming = false;
       
        crosshairUI.gameObject.SetActive(false);
        aimCamera.m_Priority = 9;


    }

    // Update is called once per frame
    void Update()
    {
        rigBuild -= Time.deltaTime;
        if(rigBuild <= 0)
        {
            if (!isActive)
            {
                //gameObject.GetComponent<RigBuilder>().enabled = true;
                isActive = true;
            }

        }

        if (isWeaponActive)
        {
            if (isAiming)
            {
                
                crosshairUI.gameObject.SetActive(true);
                Aim();
            }
            else
            {
                //weapon.gameObject.SetActive(false);
                weapon.transform.GetChild(0).gameObject.SetActive(false);
            }
            
        }

        if (isAmuletActive)
        {
            if (isAiming)
            {
                crosshairUI.gameObject.SetActive(true);
                Aim();
            }
        }




        /*
        if (weapon)
        {
            if (isWeaponActive)
            {
                if (isAiming)
                {

                    Aim();
                }

                if (Input.GetMouseButtonDown(1))
                {

                    ToggleAim();
                }

                if (Input.GetMouseButtonDown(0))
                {
                    if (isAiming)
                    {
                        weapon.Shoot();
                    }
                }

                if (Input.GetKeyDown(KeyCode.R))
                {
                    weapon.Reload();
                }
            }
            else
            {
                rigBodyAim.weight = 0;
                rigWeaponAim.weight = 0;
            }
        }*/




    }

    public bool ReturnIsAiming()
    {
        return isAiming;
    }

    public void SetIsAiming(bool value)
    {
        isAiming = value;
    }

    public void ToggleAim()
    {
        if (!isAiming)
        {
            //rigBodyAim.weight = 1;
            //rigWeaponAim.weight = 1;
            anim.SetLayerWeight(1, 1);
            //weapon.gameObject.SetActive(true);
            weapon.transform.GetChild(0).gameObject.SetActive(true);
            weaponDummy.gameObject.SetActive(false);
            isAiming = true;
            transform.rotation = Quaternion.Euler(0f, thirdPersonCamera.m_XAxis.Value, 0f);
            aimCamera.m_Priority = 11;
            this.gameObject.GetComponent<ThirdPersonMovement>().CancelSprint();
           
        }
        else
        {
            anim.SetLayerWeight(1, 0);
            crosshairUI.gameObject.SetActive(false);
            //rigBodyAim.weight = 0;
            //rigWeaponAim.weight = 0;
            //weapon.gameObject.SetActive(false);
            weapon.transform.GetChild(0).gameObject.SetActive(false);
            weaponDummy.gameObject.SetActive(true);
            isAiming = false;
            aimCamera.m_Priority = 9;
            this.gameObject.GetComponent<ThirdPersonMovement>().canSprint = true;
        }



    }

    public void ToggleOutOfAim()
    {
        anim.SetLayerWeight(1, 0);
        crosshairUI.gameObject.SetActive(false);
        //rigBodyAim.weight = 0;
        //rigWeaponAim.weight = 0;
        isAiming = false;
        aimCamera.m_Priority = 9;
        this.gameObject.GetComponent<ThirdPersonMovement>().canSprint = true;
    }

    private void Aim()
    {
        anim.SetFloat("AimLook", aimCamera.transform.rotation.x);

        var rot = followTarget.localRotation.eulerAngles;
        rot.x -= Input.GetAxis("Mouse Y") * mouseSensitivity;

        if(rot.x > 180)
        {
            rot.x -= 360;
        }
        rot.x = Mathf.Clamp(rot.x, -80, 80);
        followTarget.localRotation = Quaternion.Slerp(followTarget.localRotation, Quaternion.Euler(rot), .5f);

        rot = transform.eulerAngles;
        rot.y += Input.GetAxis("Mouse X") * mouseSensitivity;
       
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(rot), .5f);

        

    }


    public Vector2 CameraInput
    {
        get
        {
            return m_Camera * mouseSensitivity;
        }
    }

    public float GetAxisCustom(string axisName)
    {
        if (axisName == "CameraX")
            return CameraInput.x;
        else if (axisName == "CameraY")
            return CameraInput.y;
        return 0;
    }

    public Gun GetWeapon()
    {
        return weapon;
    }

    public bool GetIsAiming()
    {

        return isAiming;
    }


    public void Equip(Gun newWeapon)
    {
        //if (weapon)
        //{
        //    Destroy(weapon.gameObject);
        //}

        weapon = newWeapon;
        //rigWeaponAim.weight = 0f;
        //rigBodyAim.weight = 0f;

    }

    public Animator GetAnimator()
    {
        return anim;
    }


}
