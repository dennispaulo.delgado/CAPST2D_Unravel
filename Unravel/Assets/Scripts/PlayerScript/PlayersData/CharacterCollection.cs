﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterCollection")]
public class CharacterCollection : ScriptableObject
{
    public List<CharacterInfo> charInfoCollection;
}
