﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersData : MonoBehaviour
{
    public static PlayersData instance;

    public List<PlayerNotes> pNotes;
    public List<PlayerLogs> pLogs;
    [Header("CharacterInfoID's")]
    public List<int> charIDs;
    [Header("Audio")]
    public AudioClip scribble;
    [Header("Monster Data")]
    public List<int> monsterIDDiscovered;
    public List<int> manananggalObsIDs;
    public List<int> aswangObsIDs;
    public List<int> mangkukulamObsID;
    [Header("Requirement")]
    public bool sceneHasPlayer;
    public GameObject player;
    public AudioSource journalSoundSource;
    [Header("DaySquence")] 
    [SerializeField] private UI_Inventory uiInventory;

    public Inventory inventory;

    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        CheckPlayerData();
        AswangObsEvent.instance.onAddManananggalObs += AddManananggalObservationID;
        AswangObsEvent.instance.onAddAswangObs += AddAswangObservationID;
        AswangObsEvent.instance.onAddManananggalObs += AddMangkukulamObservationID;
        AswangObsEvent.instance.onAddMonsterDiscover += AddMonsterDiscoverID;

        GameEvents.instance.onSave += SavePlayer;
        GameEvents.instance.onLoad += LoadPlayer;
        GameEvents.instance.clearListeners += ClearThisListeners;
    }

    private void CheckPlayerData()
    {
        if (sceneHasPlayer)
        {
            GivePlayerDatas();
        }
        else
        {
            inventory = new Inventory();
            uiInventory.SetInventory(inventory);
            GetPlayerDatas();
        }
    }

    
    private void GivePlayerDatas()
    {
        Debug.Log(SaveManager.instance.playerData);
        Debug.Log(SaveManager.instance);
        Debug.Log(SaveManager.instance.playerData.itemList);
        if (SaveManager.instance.playerData.itemList.Count>0)
        {
            GivePlayerInventory();
        }
        GivePlayerNotes();
        GivePlayerCharacterInfo();
        GivePlayerMonsterObservation();
    }
    //for scene where no player
    private void GetPlayerDatas()
    {
        if (SaveManager.instance.playerData.itemList.Count > 0)
        {
            GetPlayerInventory();
        }
        GivePlayerNotes();
        GivePlayerCharacterInfo();
        GivePlayerMonsterObservation();
    }
    public void SavePlayerInventory()
    {
        CollectBehaviour colBeh = player.GetComponent<CollectBehaviour>();
        List<Item> itemListCopy = new List<Item>();
        for (int c = 0; c < colBeh.GetInventory().GetInventoryList().Count; c++)
        {
            itemListCopy.Add(colBeh.GetInventory().GetInventoryList()[c].ReturnDeepCopy());
        }
        SaveManager.instance.playerData.itemList = itemListCopy;
    }
    public void SavePlayerNotesAndObservations()
    {
        ///SavePlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesSave = new List<PlayerNotes>();
        for (int n = 0; n < pData.pNotes.Count; n++)
        {
            pNotesSave.Add(pData.pNotes[n].ReturnDeepCopyNotes());
        }
        SaveManager.instance.playerData.playerNotes = pNotesSave;
        //SavePlayerCharInfo
        SaveManager.instance.playerData.characterIDs = charIDs;
        ///////////////////
        ///ManananggalObs
        List<int> copyManang = new List<int>(manananggalObsIDs);
        SaveManager.instance.playerData.manananggalSObsIDs = copyManang;
        //AswangObs
        List<int> copyAswang = new List<int>(aswangObsIDs);
        SaveManager.instance.playerData.aswangSObsIDs = copyAswang;
        //MangkukulamObs
        List<int> copyMangkukulam = new List<int>(mangkukulamObsID);
        SaveManager.instance.playerData.mangkukulamSObsIDs = copyMangkukulam;
        //MonsterDiscover
        List<int> copyMonster = new List<int>(monsterIDDiscovered);
        SaveManager.instance.playerData.monsterDiscoverIDs = copyMonster;
    }
    public void GivePlayerInventory()
    {
        CollectBehaviour colBeh = player.GetComponent<CollectBehaviour>(); 
        colBeh.GetInventory().ChangeInvItemList(SaveManager.instance.playerData.itemList);
        colBeh.ReturnUIInventory().RefreshInventoryItems();
    }
    //For Scene whereas no player
    public void GetPlayerInventory()
    {
        inventory.ChangeInvItemList(SaveManager.instance.playerData.itemList);
        uiInventory.RefreshInventoryItems();
    }
    public void GivePlayerNotes()
    {
        pNotes = SaveManager.instance.playerData.playerNotes;
    }
    public void GivePlayerCharacterInfo()
    {
        //pChar = SaveManager.instance.playerData.characterInfo;
        charIDs = SaveManager.instance.playerData.characterIDs;
    }
    public void GivePlayerMonsterObservation()
    {
        aswangObsIDs = SaveManager.instance.playerData.aswangSObsIDs;
        manananggalObsIDs = SaveManager.instance.playerData.manananggalSObsIDs;
        mangkukulamObsID = SaveManager.instance.playerData.mangkukulamSObsIDs;
        monsterIDDiscovered = SaveManager.instance.playerData.monsterDiscoverIDs;
    }
    public List<PlayerNotes> ReturnNotes()
    {
        return pNotes;
    }
    public void AddPlayerNotes(PlayerNotes notes)
    {
        pNotes.Add(notes);
        ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.LoreUpdated);
    }

    public void ClearThisListeners()
    {
        GameEvents.instance.onSave -= SavePlayer;
        GameEvents.instance.onLoad -= LoadPlayer;
        GameEvents.instance.clearListeners -= ClearThisListeners;
        AswangObsEvent.instance.onAddManananggalObs -= AddManananggalObservationID;
        AswangObsEvent.instance.onAddAswangObs -= AddAswangObservationID;
        AswangObsEvent.instance.onAddMangkukulamObs -= AddMangkukulamObservationID;
        AswangObsEvent.instance.onAddMonsterDiscover -= AddMonsterDiscoverID;
    }

    public void LoadPlayer()
    {
        if (SaveManager.instance.playerData.loadAllData)
        {
            //Load Position
            player.gameObject.transform.position = SaveManager.instance.playerData.lastSavePostion;
            player.gameObject.transform.eulerAngles = SaveManager.instance.playerData.lastSaveRotation;
        }
        //Load Inventory
        CollectBehaviour colBeh = player.GetComponent<CollectBehaviour>();
        //Copy Items from save
        List<Item> itemListCopyFSave = new List<Item>();
        for (int c = 0; c < SaveManager.instance.playerData.itemList.Count; c++)
        {
            itemListCopyFSave.Add(SaveManager.instance.playerData.itemList[c].ReturnDeepCopy());
        }
        colBeh.GetInventory().ChangeInvItemList(itemListCopyFSave);
        //////////////////
        ///Load PlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesLoad = new List<PlayerNotes>();
        for (int n = 0; n < SaveManager.instance.playerData.playerNotes.Count; n++)
        {
            pNotesLoad.Add(SaveManager.instance.playerData.playerNotes[n].ReturnDeepCopyNotes());
        }
        pData.pNotes = pNotesLoad;
        ///////////////////
        ///CharacterIDs
        charIDs = SaveManager.instance.playerData.characterIDs;
        ///////////////////
        ///ManananggalObs
        aswangObsIDs = SaveManager.instance.playerData.aswangSObsIDs;
        //AswangObs
        manananggalObsIDs = SaveManager.instance.playerData.manananggalSObsIDs;
        //MangkukulamObs
        mangkukulamObsID = SaveManager.instance.playerData.mangkukulamSObsIDs;
        //MonsterDiscover
        monsterIDDiscovered = SaveManager.instance.playerData.monsterDiscoverIDs;
        Debug.Log("PlayerLoaded");
    }

    public void SavePlayer()
    {
        if (sceneHasPlayer)
        {
            //
            SaveManager.instance.playerData.loadAllData = true;
            SaveManager.instance.hasData = true;
            //Save Position
            Debug.Log("Save Player Position = " + player.gameObject.transform.position);
            SaveManager.instance.playerData.lastSavePostion = player.gameObject.transform.position;
            SaveManager.instance.playerData.lastSaveRotation = player.gameObject.transform.eulerAngles;

            CollectBehaviour colBeh = player.GetComponent<CollectBehaviour>();
            List<Item> itemListCopy = new List<Item>();
            for (int c = 0; c < colBeh.GetInventory().GetInventoryList().Count; c++)
            {
                itemListCopy.Add(colBeh.GetInventory().GetInventoryList()[c].ReturnDeepCopy());
            }
            SaveManager.instance.playerData.itemList = itemListCopy;
        }
        else
        {
            List<Item> itemListCopy = new List<Item>();
            for (int c = 0; c < inventory.GetInventoryList().Count; c++)
            {
                itemListCopy.Add(inventory.GetInventoryList()[c].ReturnDeepCopy());
            }
            SaveManager.instance.playerData.itemList = itemListCopy;
        }    
        //Getting Inventory.
        //Save Inventory
        
        ////////////////////
        ///SavePlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesSave = new List<PlayerNotes>();
        for (int n = 0; n < pData.pNotes.Count; n++)
        {
            pNotesSave.Add(pData.pNotes[n].ReturnDeepCopyNotes());
        }
        SaveManager.instance.playerData.playerNotes = pNotesSave;
        ///////////////////
        ///CharacterIDs
        SaveManager.instance.playerData.characterIDs = charIDs;
        ///////////////////
        ///PlayerCharacter Infos
        SaveManager.instance.playerData.characterIDs = charIDs;
        ///////////////////
        ///ManananggalObs
        List<int> copyManang = new List<int>(manananggalObsIDs);
        SaveManager.instance.playerData.manananggalSObsIDs = copyManang;
        //AswangObs
        List<int> copyAswang = new List<int>(aswangObsIDs);
        SaveManager.instance.playerData.aswangSObsIDs = copyAswang;
        //Mankukulam
        List<int> copyMangkukulam = new List<int>(mangkukulamObsID);
        SaveManager.instance.playerData.mangkukulamSObsIDs = copyMangkukulam;
        //monster
        List<int> copyMonster = new List<int>(monsterIDDiscovered);
        SaveManager.instance.playerData.monsterDiscoverIDs = copyMonster;

        Debug.Log("PlayerSaved");
    }

    #region ManananggalObservation
    public void AddManananggalObservationID(int id)
    {
        if (manananggalObsIDs.Count > 0)
        {
            for (int m = 0; m < manananggalObsIDs.Count; m++)
            {
                if (id == manananggalObsIDs[m])
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.ObservationGain);
        manananggalObsIDs.Add(id);
    }
    #endregion

    #region AswangObservation
    public void AddAswangObservationID(int id)
    {
        if (aswangObsIDs.Count > 0)
        {
            for (int m = 0; m < aswangObsIDs.Count; m++)
            {
                if (id == aswangObsIDs[m])
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.ObservationGain);
        aswangObsIDs.Add(id);
    }

    #endregion

    #region MangkukulamObservation
    public void AddMangkukulamObservationID(int id)
    {
        if (mangkukulamObsID.Count > 0)
        {
            for (int m = 0; m < mangkukulamObsID.Count; m++)
            {
                if (id == mangkukulamObsID[m])
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.ObservationGain);
        mangkukulamObsID.Add(id);
    }
    #endregion

    #region MonsterDiscover
    public void AddMonsterDiscoverID(int id)
    {
        if (monsterIDDiscovered.Count > 0)
        {
            for (int c = 0; c < monsterIDDiscovered.Count; c++)
            {
                if (id == monsterIDDiscovered[c])
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.MonsterDiscovered);
        monsterIDDiscovered.Add(id);
    }
    #endregion
    #region CharacterProfiles
    public void AddCharacterProfile(int id)
    {
        if(charIDs.Count>0)
        {
            for(int c = 0;c<charIDs.Count;c++)
            {
                if (id == charIDs[c])
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.LoreUpdated);
        charIDs.Add(id);
    }
    #endregion
    public void PlayScribbleSound()
    {
        journalSoundSource.clip = scribble;
        journalSoundSource.Play();
    }
}

[System.Serializable]
public class MonsterKnowledge
{
    public string monsterName;
    public bool isDiscovered = false;
    public List<MonsterObservation> monsterObservations;
}
