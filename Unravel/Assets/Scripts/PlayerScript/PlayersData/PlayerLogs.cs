﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerLogs
{
    public string logButTitle;
    public string logTitle;
    [TextArea(5, 20)]
    public string logContent;
    public PlayerLogs ReturnDeepCopyNotes()
    {
        PlayerLogs n = new PlayerLogs();
        n.logButTitle = this.logButTitle;
        n.logTitle = this.logTitle;
        n.logContent = this.logContent;
        return n;
    }


}
