﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterInformation")]
public class CharacterInfo : ScriptableObject
{
    public int charID;
    public Sprite charImage;
    public string charName;
    [TextArea(10,20)]
    public string charDescription;
}
