﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerNotes
{
    public string noteButTitle;
    public string noteTitle;
    [TextArea(5, 20)]
    public string noteContent;

    public PlayerNotes ReturnDeepCopyNotes()
    {
        PlayerNotes n = new PlayerNotes();  
        n.noteButTitle = this.noteButTitle;
        n.noteTitle = this.noteTitle;
        n.noteContent = this.noteContent;
        return n;
    }
}
