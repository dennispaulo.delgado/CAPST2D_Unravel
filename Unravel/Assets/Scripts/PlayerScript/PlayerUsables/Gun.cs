﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon, ILethal
{
    public static EventHandler OnGunShot;

    [SerializeField] private int _damage = 10;
    [SerializeField] private int _capacity = 10;
    [SerializeField] private int _ammo = 20;
    [SerializeField] private int _maxCapacity = 10;
    [SerializeField] private float _range = 50f;
    [SerializeField] private float _fireRate = 1f;
    [SerializeField] bool hasShot = false;
    [SerializeField] private Transform _firePoint;
    [Tooltip("CrosshairTarget Object")]
    [SerializeField] private Transform projectileDestination;
    [SerializeField] private GameObject _projectilePrefab;
    [SerializeField] private ThirdPersonAimControl playerAim;

    [Header("Ammo")]
    public List<AmmoType> ammoTypes;
    public AmmoType currentAmmoType;
    private int ammoIndex;
    public BlowpipeHUD bHUD;
    [Header("CollectBehaviour")]
    public CollectBehaviour collectBehaviourReference;
    private Inventory inventory;
    [Header("Cheats")]
    private bool hasInfiniteAmmo = false;

    Ray ray;
    RaycastHit hitInfo;

    private void Start()
    {
        playerAim = FindObjectOfType<ThirdPersonAimControl>();
        inventory = collectBehaviourReference.GetInventory();
        inventory.OnItemListChanged += GotAnItem;
        StartCoroutine("DelayedStart");
    }

    //for testing
    private void Update()
    {
        if (playerAim.GetIsAiming())
        {           
            this.transform.LookAt(projectileDestination);
        }
       
        

        if(Input.GetKeyUp(KeyCode.Alpha1))
        {
            ChangeAmmoTypeIndex(1);
        }
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            ChangeAmmoTypeIndex(-1);
        }

        if (hasShot)
        {
            ray.origin = _firePoint.position;
            ray.direction = _firePoint.forward;

            if(Physics.Raycast(ray, out hitInfo))
            {
                Debug.DrawLine(ray.origin, hitInfo.point, Color.red, 1.0f);
            }

            collectBehaviourReference.gameObject.GetComponent<ThirdPersonAimControl>().GetAnimator().SetBool("HasShot", true);
            fireRate -= Time.deltaTime;
            if (fireRate <= 0)
            {
                collectBehaviourReference.gameObject.GetComponent<ThirdPersonAimControl>().GetAnimator().SetBool("HasShot", false);
                hasShot = false;
                fireRate = 1.5f;
            }
        }
    }

    IEnumerator DelayedStart()
    {
        yield return new WaitForSecondsRealtime(.3f);
        ChangeAmmoType();
    }

    public void GivePlayerInfiniteAmmoOrNot()
    {
        if(hasInfiniteAmmo)
        {
            hasInfiniteAmmo = false;
        }
        else
        {
            hasInfiniteAmmo = true;
        }
    }
    #region ILethal Property
    public int damage
    {
        get
        {
            return _damage;
        }
        set
        {
            _damage = value;
        }
    }
    public int capacity
    {
        get
        {
            return _capacity;
        }
        set
        {
            _capacity = value;
        }
    }
    public int ammo
    {
        get
        {
            return _ammo;
        }
        set
        {
            _ammo = value;
        }
    }
    public float range
    {
        get
        {
            return _range;
        }
        set
        {
            _range = value;
        }
    }
    public float fireRate
    {
        get
        {
            return _fireRate;
        }
        set
        {
            _fireRate = value;
        }
    }
    #endregion

    public override void Shoot()
    {
        if (!hasShot)
        {
            //Debug.Log("Shoot Pistol");
            if (capacity <= 0)
            {
                if (ammo > 0)
                {
                    Reload();
                }

                return;
            }
            capacity--;
            if (CheckIfHasAmmo(ammoTypes[ammoIndex])||hasInfiniteAmmo)
            {
                hasShot = true;
                var projectile = Instantiate(currentAmmoType.ammo, _firePoint.position, Quaternion.identity);
                OnGunShot?.Invoke(this, EventArgs.Empty);
            }
            //var projectile = Instantiate(_projectilePrefab, _firePoint.position, Quaternion.identity);
        }
        

    }

    

    public override void Reload()
    {
        int ammoRequired; 
        if(capacity < 10)
        {
            ammoRequired = _maxCapacity - capacity;

            if(ammo < ammoRequired)
            {
                ammoRequired = ammo;
                capacity += ammoRequired;
                ammo -= ammoRequired;
            }
            else
            {
                capacity += ammoRequired;
                ammo -= ammoRequired;
            }
        }
    }

    public void ChangeAmmoTypeIndex(int index)
    {
        if (ammoTypes.Count <= 0)
        {
            return;
        }
        else if (ammoTypes.Count == 1)
        {
            ammoIndex = 0;
        }
        else if (ammoTypes.Count > 1)
        {
            if (ammoIndex + index < 0)
            {
                ammoIndex = ammoTypes.Count - 1;
            }
            else if (ammoIndex + index > ammoTypes.Count - 1)
            {
                ammoIndex = 0;
            }
            else
            {
                ammoIndex += index;
            }
        }
        ChangeAmmoType();
    }

    private void GotAnItem(object sender, System.EventArgs e)
    {
        //Debug.Log("Check Inventory");
        CheckAmmoCount();
    }

    public bool CheckIfHasAmmo(AmmoType amTy)
    {
        for(int i = 0; i< inventory.GetInventoryList().Count;i++)
        {
            if (inventory.GetInventoryList()[i].interactable.itemType == amTy.ammoItemType)
            {
                if (inventory.GetInventoryList()[i].amount > 0)
                {
                    inventory.RemoveItem(inventory.GetInventoryList()[i]);
                    return true;
                }
            }
        }
        return false;
    }

    private void ChangeAmmoType()
    {
        currentAmmoType = ammoTypes[ammoIndex];
        bHUD.ChangeAmmoSprite(ammoTypes[ammoIndex].ammoSprite);
        CheckAmmoCount();
    }

    private void CheckAmmoCount()
    {


        if (inventory.GetInventoryList().Count <= 0)
        {
            bHUD.ChangeAmmoCount(0);
        }
        else
        {
            for (int i = 0; i < inventory.GetInventoryList().Count; i++)
            {
                if (inventory.GetInventoryList()[i].interactable.itemType == ammoTypes[ammoIndex].ammoItemType)
                {
                    if (inventory.GetInventoryList()[i].amount > 0)
                    {
                        bHUD.ChangeAmmoCount(inventory.GetInventoryList()[i].amount);
                        //Debug.Log("Ammo : " + ammoTypes[ammoIndex].ammoItemType + " ||| Count : "+ inventory.GetInventoryList()[i].amount);
                        return;
                    }
                }
            }
            bHUD.ChangeAmmoCount(0);
        }
    }


    public Transform GetFirePoint()
    {
        return _firePoint;
    }

    public float GetFireRate()
    {
        return fireRate;
    }

    public void SetFirePoint(Transform location)
    {
        _firePoint = location;
    }

    public GameObject GetProjectile()
    {
        return _projectilePrefab;
    }

    public Transform GetProjectileDirection()
    {
        return projectileDestination;
    }

}
