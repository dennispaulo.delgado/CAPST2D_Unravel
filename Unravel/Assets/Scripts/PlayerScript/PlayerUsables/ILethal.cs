﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILethal
{
    int damage { get; set; }
    int capacity { get; set; }
    int ammo { get; set; }
    float range { get; set; }
    float fireRate { get; set; }
}
