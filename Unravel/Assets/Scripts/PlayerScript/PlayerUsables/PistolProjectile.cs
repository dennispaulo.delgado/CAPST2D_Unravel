﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolProjectile : MonoBehaviour
{
    public class OnEnemyHitArgs : EventArgs
    {
        public Enemy enemy;
        public int damage;
    }

    public class OnBossEnemyHitArgs : EventArgs
    {
        public Enemy enemy;
        public int damage;
    }

    public static EventHandler<OnEnemyHitArgs> OnEnemyHit;
    public static EventHandler<OnBossEnemyHitArgs> OnBossEnemyHit;

    private float projectileSpeed = 3000f;
    private Transform player;
    [SerializeField] private Gun myGun;

    private Transform firePoint;
   


    private void Awake()
    {
        //player.Find("FollowTarget").transform.Find("WeaponSlot").transform.Find("Gun").GetComponent<Gun>();
        player = FindObjectOfType<ThirdPersonAimControl>().transform;
        myGun = player.transform.Find("WeaponSlot").transform.Find("WeaponHolder").transform.Find("WeaponPivot").GetComponentInChildren<Gun>();

        firePoint = myGun.transform.Find("firePoint");
    }


    // Start is called before the first frame update
    void Start()
    {
        Vector3 direction = myGun.GetProjectileDirection().position - firePoint.transform.position;

        transform.GetComponent<Rigidbody>().velocity = (firePoint.transform.forward + direction.normalized)  * (projectileSpeed * Time.deltaTime);

    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(myGun.GetFirePoint().position, transform.position) > myGun.range)
        {
            DestroyBullet();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Player");
            return;
        }
        else if(collision.gameObject.tag == "Enemy")
        {
            Debug.Log("Enemy");
            if (collision.gameObject.GetComponent<Enemy>().GetEnemyTypeSO().enemyTypes == EnemyType.EnemyTypes.LesserFlameWisps)
            {
                OnEnemyHit?.Invoke(this, new OnEnemyHitArgs { enemy = collision.gameObject.GetComponent<Enemy>(), damage = 10 });
            }
            else
            {
                OnBossEnemyHit?.Invoke(this, new OnBossEnemyHitArgs { enemy = collision.gameObject.GetComponent<Enemy>(), damage = 10 });
            }
            
            Destroy(gameObject);
        }
        else if(collision.gameObject.GetComponent<HitPuzzle>())
        {
            Debug.Log("HitPuzzle");
            collision.gameObject.GetComponent<HitPuzzle>().PuzzleHit();
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("miss");
            Destroy(gameObject);
        }



    }

    public void DestroyBullet()
    {
        Debug.Log("Destroyed");
        Destroy(gameObject);
    }
}
