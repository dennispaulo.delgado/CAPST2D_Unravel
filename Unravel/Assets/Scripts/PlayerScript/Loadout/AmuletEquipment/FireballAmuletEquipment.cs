﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Loadout/FireballAmulet")]
public class FireballAmuletEquipment : EquipableLoadout
{
    private GameObject origin;
    private Amulet whatAmulet;
    public int scrollID;
    public override void GetReference(GameObject refer)
    {
        origin = refer;
        PlayerLoadout pl = refer.GetComponent<PlayerLoadout>();
        //whatAmulet = pl.fireballAmulet;
    }

    public override void OnEquipped()
    {
    }

    public override void OnExecute()
    {
        if (origin.transform.parent.GetComponent<ThirdPersonAimControl>().ReturnIsAiming())
        {
            if (CanUseAmulet()||origin.GetComponent<PlayerLoadout>().isTesting)
            {
                whatAmulet.GetPlayerAbilities(origin);
            }
        }
    }

    public bool CanUseAmulet()
    {
        Inventory inv = origin.GetComponent<PlayerLoadout>().ReturnInventory();
        for(int i = 0; i< inv.GetInventoryList().Count;i++)
        {
            if(whatAmulet.whatAmulet == inv.GetInventoryList()[i].interactable.itemType)
            {
                inv.RemoveItem(inv.GetInventoryList()[i]);
                return true;
            }
        }
        return false;
    }

    public override void OnUnEquipped()
    {
        if (origin.transform.parent.GetComponent<ThirdPersonAimControl>().ReturnIsAiming())
        {
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().isAmuletActive = false;
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().ToggleAim();
        }
    }

    public override void OnUse()
    {
        origin.transform.parent.GetComponent<ThirdPersonAimControl>().isAmuletActive = true;
        origin.transform.parent.GetComponent<ThirdPersonAimControl>().ToggleAim();
    }

    public override void OnReloadEquipment()
    {
    }
}
