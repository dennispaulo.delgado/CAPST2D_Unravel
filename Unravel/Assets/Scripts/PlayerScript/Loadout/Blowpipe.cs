﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Loadout/Blowpipe")]
public class Blowpipe : EquipableLoadout
{
    private GameObject origin;

    public override void GetReference(GameObject refer)
    {
        origin = refer;
        PlayerLoadout pl = refer.GetComponent<PlayerLoadout>();
    }

    public override void OnEquipped() // 
    {
        origin.transform.parent.GetComponent<ThirdPersonAimControl>().isWeaponActive = true;
        origin.transform.parent.GetComponent<ThirdPersonAimControl>().GetWeapon().gameObject.SetActive(true);

    }

    public override void OnExecute() //Left Click
    {
        if (origin.transform.parent.GetComponent<ThirdPersonAimControl>().ReturnIsAiming())
        {
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().GetWeapon().Shoot();
        }

    }

    public override void OnUnEquipped() 
    {
        if (origin.transform.parent.GetComponent<ThirdPersonAimControl>().ReturnIsAiming())
        {
            //origin.transform.parent.GetComponent<ThirdPersonAimControl>().SetIsAiming(false);
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().isWeaponActive = false;
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().GetWeapon().gameObject.SetActive(false);
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().ToggleAim();
        }
        else
        {
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().isWeaponActive = false;
            origin.transform.parent.GetComponent<ThirdPersonAimControl>().GetWeapon().gameObject.SetActive(false);
            //origin.transform.parent.GetComponent<ThirdPersonAimControl>().ToggleAim();

        }

    }

    public override void OnUse() // Right Click
    {
        origin.transform.parent.GetComponent<ThirdPersonAimControl>().ToggleAim();
    }

    public override void OnReloadEquipment()
    {
        origin.transform.parent.GetComponent<ThirdPersonAimControl>().GetWeapon().Reload();
    }
}
