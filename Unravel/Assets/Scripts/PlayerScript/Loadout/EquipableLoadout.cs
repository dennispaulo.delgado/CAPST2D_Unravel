﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EquipableLoadout : ScriptableObject
{
    public string loadOutName;

    public abstract void GetReference(GameObject refer);
    public abstract void OnUse();
    public abstract void OnEquipped();
    public abstract void OnUnEquipped();
    public abstract void OnReloadEquipment();
    public abstract void OnExecute();
}
