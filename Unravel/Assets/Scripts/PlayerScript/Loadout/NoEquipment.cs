﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Loadout/NoLoadOut")]
public class NoEquipment : EquipableLoadout
{
    public override void GetReference(GameObject refer)
    {
    }

    public override void OnEquipped()
    {
    }

    public override void OnExecute()
    {
    }

    public override void OnReloadEquipment()
    {
    }

    public override void OnUnEquipped()
    {
    }

    public override void OnUse()
    {
    }

}
