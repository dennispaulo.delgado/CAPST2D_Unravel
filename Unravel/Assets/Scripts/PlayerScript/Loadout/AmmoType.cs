﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AmmoType")]
public class AmmoType : ScriptableObject
{
    public string ammoName;
    public GameObject ammo;
    public Item.ItemType ammoItemType;
    public Sprite ammoSprite;
}
