﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    public static EventHandler OnGreatHealth;
    public static EventHandler OnGoodHealth;
    public static EventHandler OnFatalHealth;
    public static EventHandler OnDamage;

    

    public float curHealth;
    public float maxHealth;
    public float healthPercentage;

    [SerializeField] GameObject burnEffect;
    [SerializeField] AudioClip[] painClip;
    [SerializeField] AudioSource audioSource;
    [Header("Cheat")]
    private bool isInvincible = false;

    //-Manananggal Grip Damage variable--//
    [SerializeField] private bool onGrip = false;
    private bool gripActive = false;
    private int gripTick = 0;

    private bool onBite = false;
    private bool biteActive = false;
    private int biteTick = 0;

    //-Magnkukulam Fire Damage variable--//
    private bool onFire = false;
    private bool isActive = false;
    private int fireTick = 0;


    private IEnumerator burnCoroutine;
    private IEnumerator gripCoroutine;
    private IEnumerator biteCoroutine;


    public GameObject GetBurnEffect()
    {
        return burnEffect;
    }

   

    private void Awake()
    {
        if(burnEffect != null)
        {
            burnEffect.SetActive(false);
        }
        burnCoroutine = BurnDamage(5f);
        gripCoroutine = GripDamage(30f);
        biteCoroutine = BiteDamage(40f);

        


    }
    // Start is called before the first frame update
    void Start()
    {
        JournalUIScript.OnToggleJournal += JournalUIScript_OnToggleJournal;
        curHealth = maxHealth;
        GetHealthPercentage();
        //CheckHealthStatus();
        //Invoke("CheckHealthStatus", 2f); //HealthStatusUI runs right after Invoke(ResetStatus)
        HealthStatusUI.OnDelayStatusFinish += HealthStatusUI_OnDelayStatusFinish;
        Attack.OnMangkukulamAttack += Attack_OnMangkukulamAttack;
        SpecialSkill.OnManananggalSpecial += SpecialSkill_OnManananggalSpecial;
        SpecialSkill.OnAswangSpecial += SpecialSkill_OnAswangSpecial;
        ButtonMashControl.OnButtonMashFinished += ButtonMashControl_OnButtonMashFinished;
        GameEvents.instance.clearListeners += ClearListeners;
    }

    public void ClearListeners()
    {
        JournalUIScript.OnToggleJournal -= JournalUIScript_OnToggleJournal;
        HealthStatusUI.OnDelayStatusFinish -= HealthStatusUI_OnDelayStatusFinish;
        Attack.OnMangkukulamAttack -= Attack_OnMangkukulamAttack;
        SpecialSkill.OnManananggalSpecial -= SpecialSkill_OnManananggalSpecial;
        SpecialSkill.OnAswangSpecial -= SpecialSkill_OnAswangSpecial;
        ButtonMashControl.OnButtonMashFinished -= ButtonMashControl_OnButtonMashFinished;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    private void HealthStatusUI_OnDelayStatusFinish(object sender, EventArgs e)
    {
        Debug.Log("StatusCheck");
        CheckHealthStatus();
    }

    void ButtonMashControl_OnButtonMashFinished(object sender, EventArgs e)
    {
        StopCoroutine(gripCoroutine);
        onGrip = false;
        gripActive = false;
        gripTick = 0;
    }


    void SpecialSkill_OnAswangSpecial(object sender, EventArgs e)
    {
        onBite = true;
    }


    void SpecialSkill_OnManananggalSpecial(object sender, System.EventArgs e)
    {
        onGrip = true;
    }


    void Attack_OnMangkukulamAttack(object sender, System.EventArgs e)
    {
        onFire = true;

        //StartCoroutine(coroutine);
    }


    // Update is called once per frame
    void Update()
    {
        #region Manananggal grip damage to player;
        if (onGrip)
        {

            if (!gripActive)
            {


                gripActive = true;
                StartCoroutine(gripCoroutine);
            }
        }

        if(gripTick == 5)
        {
            StopCoroutine(gripCoroutine);
            onGrip = false;
            gripActive = false;
            gripTick = 0;
        }

        #endregion


        #region Mangkukulam burning damage to player;
        if (onFire)
        {
            if (!isActive)
            {
                burnEffect.SetActive(true);
                isActive = true;
                StartCoroutine(burnCoroutine);
            }

        }
       

        if (fireTick == 4)
        {

            StopCoroutine(burnCoroutine);
            burnEffect.SetActive(false);
            onFire = false;
            isActive = false;
            fireTick = 0;
        }
        #endregion

        #region Aswang biting player
        if (onBite)
        {
            if (!biteActive)
            {
                biteActive = true;
                StartCoroutine(biteCoroutine);
            }
        }

        if(biteTick == 4)
        {
            StopCoroutine(biteCoroutine);
            onBite = false;
            biteActive = false;
            biteTick = 0;
        }
        #endregion


        /*
        if (Input.GetKeyUp(KeyCode.Delete))
        {
            Death();
        }
        
        if (Input.GetKeyUp(KeyCode.T))
        {
            DamageUnit(10f);
        }

        if (Input.GetKeyUp(KeyCode.Y))
        {
            HealUnit(10f);
        }*/
    }  

    private IEnumerator BurnDamage(float damage)
    {
        
        while (onFire)
        {
            Debug.Log("Burning Player");
            DamageUnit(damage);

            yield return new WaitForSeconds(2);
            Debug.Log("Fire Tick");
            Debug.Log(fireTick);

            fireTick++;
        }
       
    }

    private IEnumerator GripDamage(float damage)
    {
        while (onGrip)
        {
            Debug.Log("Gripping Player");
            DamageUnit(damage);

            yield return new WaitForSeconds(2);

            Debug.Log("Grip Tick");

            gripTick++;
        }
    }

    private IEnumerator BiteDamage(float damage)
    {
        while (onBite)
        {
            Debug.Log("Biting Player");
            DamageUnit(damage);

            yield return new WaitForSeconds(1);

            Debug.Log("Bite Tick");

            biteTick++;
        }
    }

    public void MakePlayerInvicibleOrNot()
    {
        if(isInvincible)
        {
            isInvincible = false;
        }
        else
        {
            isInvincible = true;
        }
    }

    public void DamageUnit(float damage)
    {
        AudioClip clip = GetRandomClipPain();
        audioSource.clip = clip;
        audioSource.PlayOneShot(clip);
        curHealth -= damage;
        CheckHealthStatus();
        OnDamage?.Invoke(this, EventArgs.Empty);
        if (curHealth<0&&!isInvincible)
        {
            onFire = false;
            curHealth = 0;
            Death();
        }
    }

    public void HealUnit(float heal)
    {
        curHealth += heal;
        CheckHealthStatus();
        if(curHealth>maxHealth)
        {
            curHealth = maxHealth;
        }
    }

    public void Death()
    {
        if(this.gameObject.tag == "Player")
        {          
            PlayersData.instance.SavePlayerInventory();
            PlayersData.instance.SavePlayerNotesAndObservations();
            GameManager.instance.OnPlayerDeath(this.gameObject);
        }
    }

    public bool GetGrip()
    {
        return onGrip;
    }

    public bool GetBite()
    {
        return onBite;
    }

    private AudioClip GetRandomClipPain()
    {
        return painClip[UnityEngine.Random.Range(0, painClip.Length)];
    }

    public float GetHealthPercentage()
    {
       
        return healthPercentage = curHealth / maxHealth * 100f;
    }

    public void CheckHealthStatus()
    {
       
        if(GetHealthPercentage() > 60f)
        {
            OnGreatHealth?.Invoke(this, EventArgs.Empty);
        }
        else if(GetHealthPercentage() < 59f && GetHealthPercentage() > 30f)
        {
            OnGoodHealth?.Invoke(this, EventArgs.Empty);
        }
        else if(GetHealthPercentage() < 29f)
        {
            OnFatalHealth?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            Debug.Log("Invalid Health check code");
        }
    }

    public void ClearBurning()
    {
        StopAllCoroutines();
        if(burnEffect != null)
        {
            burnEffect.SetActive(false);
        }

        onGrip = false;
        gripActive = false;
        gripTick = 0;
        onFire = false;
        isActive = false;
        fireTick = 0;
        onBite = false;
        biteActive = false;
        biteTick = 0;
    }





    void JournalUIScript_OnToggleJournal(object sender, EventArgs e)
    {
        CheckHealthStatus();
    }

    






}
