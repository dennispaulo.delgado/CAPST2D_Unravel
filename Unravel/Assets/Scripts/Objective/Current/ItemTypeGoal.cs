﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeGoal : Goal
{
    Item.ItemType type;
    public ItemTypeGoal(string des, Item.ItemType tpe, GoalType gt)
    {
        //objective = obj;
        description = des;
        type = tpe;
        goalType = gt;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvent.instance.onCheckItemType += CheckItemType;
        //Evaluate();
    }
    public void ReCallEvent()
    {
        ObjectiveEvent.instance.onCheckItemType += CheckItemType;
    }

    public void CheckItemType(Item.ItemType ty)
    {
        if (type == ty)
        {
            isCompleted = true;
            Debug.Log("Correct Quest ID");
        }
        ObjectiveEvent.instance.CheckObjectives();
    }
}
