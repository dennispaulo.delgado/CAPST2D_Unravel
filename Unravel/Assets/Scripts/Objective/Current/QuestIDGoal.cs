﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestIDGoal : Goal
{
    public int questID;
    public QuestIDGoal(string des, int id, GoalType gt)
    {
        //objective = obj;
        description = des;
        questID = id;
        goalType = gt;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvent.instance.onQuestArrived += CheckQuestID;
        //Evaluate();
    }

    public void ReCallEvent()
    {
        ObjectiveEvent.instance.onQuestArrived += CheckQuestID;
    }

    public void CheckQuestID(int id)
    {
        if (id == questID)
        {
            isCompleted = true;
        }
        ObjectiveEvent.instance.CheckObjectives();
    }
}
