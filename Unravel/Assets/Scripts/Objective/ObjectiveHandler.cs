﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public enum ObjectiveType { Kill, Gather, PointAB }
public class ObjectiveHandler : MonoBehaviour
{
    public static ObjectiveHandler instance;

    public List<Objective> curObjectives;
    public List<Objective> comObjectives;

    [Header("Testing Variables")]
    public ObjectiveSlot slot;
    public ObjectiveSlot slot2;

    public AudioClip scribble;
    public AudioClip objectiveComplete;
    public AudioClip addObjective;
    //public Objective curObj;
    private void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        //ObjectiveEvent.Instance.evalObjHandler += CheckHandler;
        GameEvents.instance.onSave += SaveObjectives;
        ObjectiveEvent.instance.addObjective += AddObjective;
        CheckIfExistingObjectives();
    }

    private void Update()
    {
        /*
        if (Input.GetKeyUp(KeyCode.PageUp))
        {
            curObjectives.Add(new Objective(slot));
        }
        if (Input.GetKeyUp(KeyCode.PageDown))
        {
            curObjectives.Add(new Objective(slot2));
        }*/
    }

    public void SaveObjectives()
    {
        List<Objective> copyObj = new List<Objective>(curObjectives);
        SaveManager.instance.playerData.playerObjective = copyObj;
    }

    public void CheckIfExistingObjectives()
    {
        if (SaveManager.instance.playerData != null)
        {
            if (SaveManager.instance.playerData.playerObjective.Count > 0)
            {
                curObjectives = SaveManager.instance.playerData.playerObjective;
                ReCallEventCaller();
            }
        }
    }
   
    private void ReCallEventCaller()
    {
        foreach(Objective o in curObjectives)
        {
            o.ReAddEvent();
        }
    }

    public void AddObjective(ObjectiveSlot slot)
    {
        if(!CheckIfPlayerGotObjective(slot))
        {
            curObjectives.Add(new Objective(slot));
            this.GetComponent<AudioSource>().clip = addObjective;
            this.GetComponent<AudioSource>().Play();
        }
    }    

    private bool CheckIfPlayerGotObjective(ObjectiveSlot slot)
    {
        for(int o = 0;o<curObjectives.Count;o++)
        {
            if(slot.objID == curObjectives[o].objID)
            {
                return true;
            }
        }

        for (int o = 0; o < comObjectives.Count; o++)
        {
            if (slot.objID == comObjectives[o].objID)
            {
                return true;
            }
        }
        return false;
    }

    public void ObjectiveCompleted(Objective obj)
    {
        this.GetComponent<AudioSource>().clip = objectiveComplete;
        this.GetComponent<AudioSource>().Play();
        for (int o = 0;o<curObjectives.Count;o++)
        {
            if(obj == curObjectives[o])
            {
                comObjectives.Add(curObjectives[o]);
                curObjectives.RemoveAt(o);
            }
        }
    }

    public void CheckHandler()
    {
        
    }
}
