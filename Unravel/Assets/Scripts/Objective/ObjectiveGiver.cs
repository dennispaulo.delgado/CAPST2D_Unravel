﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveGiver : MonoBehaviour
{
    public ObjectiveSlot slot;

    private bool isActive = false;

    public void GetObjective()
    {
        if (!isActive)
        {
            ObjectiveEvent.instance.AddObjectiveSlot(slot);
            isActive = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            GetObjective();
        }
    }

    public void OnCompleteObjective(int id)
    {
        ObjectiveEvent.instance.OnQuestArrivedS(id);
    }

}
