﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveChecker : MonoBehaviour
{
    public int questID;

    public void CheckForObjective()
    {
        ObjectiveEvent.instance.OnQuestArrivedS(questID);
    }

    public void ObjectiveComplete(int id)
    {
        ObjectiveEvent.instance.OnQuestArrivedS(id);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            CheckForObjective();
        }
    }
}
