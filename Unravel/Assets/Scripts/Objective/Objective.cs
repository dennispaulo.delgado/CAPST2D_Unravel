﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Objective 
{
    public List<Goal> goals = new List<Goal>();
    public string ObjectiveName;
    public string ObjectiveDescription;
    public int objID;
    public ObjectiveSlot objSlot;

    public bool isCompleted;

    public Objective(ObjectiveSlot slot)
    {
        ObjectiveEvent.instance.evalObjective += CheckObjective;
        objID = slot.objID;
        ObjectiveName = slot.objTitle;
        ObjectiveDescription = slot.objDescription;
        objSlot = slot;
        for (int i = 0; i < slot.goals.Count; i++)
        {
                    //ObjectiveEvent.Instance.UpdateSimpleObjUI(slot.objTitle);
                    //ObjectiveEvent.Instance.ConstructObjPageUI(this);
            switch(slot.goals[i].type)
            {
                case GoalType.quest:
                    goals.Add(new QuestIDGoal(slot.objDescription, slot.goals[i].questID, slot.goals[i].type));
                    break;
                case GoalType.item:
                    goals.Add(new ItemTypeGoal(slot.objDescription, slot.goals[i].requiredItem, slot.goals[i].type));
                    break;
            }
        }
        ObjectiveEvent.instance.AddedObjective();
        ObjectiveDisplay.instance.AddObjectiveToObjectiveDisplay(this);
    }

    public void ReAddEvent()
    {
        ObjectiveEvent.instance.evalObjective += CheckObjective;
        for(int y = 0;y< goals.Count;y++)
        {
            goals[y].Init();
        }
        
    }

    public void CheckObjective()
    {
        isCompleted = goals.All(g => g.isCompleted);
        if(isCompleted)
        {
            ObjectiveEvent.instance.evalObjective -= CheckObjective;
            isCompleted = true;
            ObjectiveHandler.instance.ObjectiveCompleted(this);
            ObjectiveDisplay.instance.ResetObjectiveDisplay();
            ObjectiveEvent.instance.CheckObjHandler();
            if(objSlot.chainedObjectiveSlot!=null)
            {
                Debug.Log("addOBj");
                ObjectiveEvent.instance.AddObjectiveSlot(objSlot.chainedObjectiveSlot);
            }
        }
    }

}
