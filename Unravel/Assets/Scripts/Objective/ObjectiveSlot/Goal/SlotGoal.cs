﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ObjectiveSlot/Goal")]
public class SlotGoal : ScriptableObject
{
    public ObjectiveType objType;
    [Header("For Kill or Gather")]
    public int requiredAmount;
    [Header("For Kill")]
    public int enemyID;
    [Header("For Gather")]
    public int itemID;
    [Header("For PointAB")]
    public int pointID;
    public string LocationName;
}
