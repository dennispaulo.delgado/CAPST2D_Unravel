﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ObjectiveSlot/Slot")]
public class ObjectiveSlot : ScriptableObject
{
    [Header("For All ObjectiveTypes")]
    public int objID;
    public string objTitle;
    [TextArea(5,20)]
    public string objDescription;
    //public List<SlotGoal> goals;
    public List<SlotQGoal> goals;
    public ObjectiveSlot chainedObjectiveSlot;
}

