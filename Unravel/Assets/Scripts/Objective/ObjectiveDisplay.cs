﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveDisplay : MonoBehaviour
{
    public static ObjectiveDisplay instance;

    public float delay;
    public GameObject objectiveFeed;
    public Transform feedContainer;
    public List<GameObject> feeds;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }
    void Start()
    {
        StartCoroutine("DelayedStart");
    }

    private void Update()
    {
    }
    public void AddObjectiveToObjectiveDisplay(Objective obj)
    {
        GameObject od = Instantiate(objectiveFeed, feedContainer);
        feeds.Add(od);
        //od.GetComponent<Text>().text = obj.ObjectiveName;
        od.GetComponent<ObjectiveFeed>().SetObjectiveFeedReferences(this, obj);
        //od.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, ((feeds.Count - 1) * objectiveFeed.GetComponent<RectTransform>().sizeDelta.y) * -1, 0);
        //feedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(feedContainer.GetComponent<RectTransform>().sizeDelta.x,
        //   feeds.Count * objectiveFeed.GetComponent<RectTransform>().sizeDelta.y);
        RefreshObjectiveDisplay();
    }

    public void ResetObjectiveDisplay()
    {
        foreach(GameObject g in feeds)
        {
            Destroy(g);
        }
        feeds.Clear();
        for(int o = 0;o<ObjectiveHandler.instance.curObjectives.Count;o++)
        {
            GameObject od = Instantiate(objectiveFeed, feedContainer);
            feeds.Add(od);
            od.GetComponent<ObjectiveFeed>().SetObjectiveFeedReferences(this, ObjectiveHandler.instance.curObjectives[o]);
            RefreshObjectiveDisplay();
        }
    }

    public void RemoveObjectiveToObjectiveDisplay(Objective obj)
    {
        for(int o = 0;o<feeds.Count;o++)
        {
            if(obj == feeds[o].GetComponent<ObjectiveFeed>().ReturnObjective())
            {
                //Destroy(feeds[0]);
                //feeds.Remove(feeds[0]);
                //RefreshObjectiveDisplay();
                //return;
            }
        }
        RefreshObjectiveDisplay();
    }

    public void RefreshObjectiveDisplay()
    {
        float currentYPos = 0;
        for (int o = 0; o < feeds.Count; o++) 
        {
            //feedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(feedContainer.GetComponent<RectTransform>().sizeDelta.x,
               //feedContainer.GetComponent<RectTransform>().sizeDelta.y+feeds[o].GetComponent<RectTransform>().sizeDelta.y);
            feedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(feedContainer.GetComponent<RectTransform>().sizeDelta.x,
               currentYPos);
            feeds[o].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, currentYPos * -1);
            currentYPos += feeds[o].GetComponent<RectTransform>().sizeDelta.y;
            //Debug.Log(o+" : " + feeds[o].GetComponent<RectTransform>().anchoredPosition);
            //Debug.Log("Current Y Pos : " + currentYPos);
        }
    }
    IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(delay);
        ResetObjectiveDisplay();
    }
}
