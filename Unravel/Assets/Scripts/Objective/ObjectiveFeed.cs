﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveFeed : MonoBehaviour
{
    public Objective objfeed;
    public GameObject goalTemp;
    public List<GameObject> goals;
    [Header("RectTransform")]
    public GameObject title;
    [Header("Fade")]
    public List<Text> texA;
    public float mainAlpha;
    public float fadeDuration;

    private void Start()
    {
        texA.Add(title.GetComponent<Text>());
    }

    public void BuildObjectiveFeed()
    {
        title.gameObject.GetComponent<Text>().text = objfeed.ObjectiveName;
        AddObjectiveGoals();
        this.GetComponent<RectTransform>().sizeDelta = new Vector2(this.gameObject.GetComponent<RectTransform>().sizeDelta.x, ReturnVerticalSize());
    }

    private void AddObjectiveGoals()
    {
        float vertSize = title.GetComponent<Text>().preferredHeight;
        for (int g = 0; g < objfeed.objSlot.goals.Count; g++)
        {
            GameObject od = Instantiate(goalTemp, this.transform);
            goals.Add(od);
            od.GetComponent<Text>().text = objfeed.objSlot.goals[g].goalRequirement;
            texA.Add(od.GetComponent<Text>());
            od.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, vertSize * -1);
            vertSize += od.GetComponent<Text>().preferredHeight;
        }
    }
    private void StartFade()
    {
        foreach (Text i in texA)
        {
            Color col = new Color(i.color.r, i.color.g, i.color.b, mainAlpha);
            i.color = col;
        }
        StartCoroutine("FadeIn");
    }

    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        mainAlpha += Time.deltaTime / 1;
        foreach (Text i in texA)
        {
            Color col = new Color(i.color.r, i.color.g, i.color.b, mainAlpha);
            i.color = col;
        }
        if (fadeDuration <= 0)
        {
            mainAlpha = 1;
        }
        else
        {
            fadeDuration -= Time.deltaTime;
            StartCoroutine("FadeIn");
        }
    }

    public float ReturnVerticalSize()
    {
        float vertSize = title.GetComponent<Text>().preferredHeight;
        for (int v = 0; v < goals.Count; v++)
        {
            vertSize += goals[v].GetComponent<Text>().preferredHeight;
        }
        return vertSize;
    }

    public Objective ReturnObjective()
    {
        return objfeed;
    }

    public void SetObjectiveFeedReferences(ObjectiveDisplay oDisplay, Objective obj)
    {
        objfeed = obj;
        //objContainer = objCon;
        BuildObjectiveFeed();
    }

}
