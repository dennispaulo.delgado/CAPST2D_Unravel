﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackMist : MonoBehaviour
{
    private ParticleSystem blackMist;
    void Start()
    {
        blackMist = this.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClearBlackMist()
    {
        ParticleSystem.MainModule main = blackMist.main;
        main.loop = false;

        this.GetComponent<BoxCollider>().enabled = false;
    }

}
