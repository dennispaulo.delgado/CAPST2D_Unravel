﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ChangeCamera : MonoBehaviour
{
    public static ChangeCamera instance;

    public Transform player;
    public CinemachineFreeLook freelook;
    public float duration;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeCameraTo(Transform target)
    {
        freelook.m_Follow = target;
        freelook.m_LookAt = target;
        StartCoroutine("ChangeCameraToOriginal");
    }

    IEnumerator ChangeCameraToOriginal()
    {
        yield return new WaitForSecondsRealtime(duration);
        freelook.m_Follow = player;
        freelook.m_LookAt = player;
    }
}
