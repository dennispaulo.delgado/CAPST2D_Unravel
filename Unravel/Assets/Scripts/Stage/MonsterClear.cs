﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterClear : MonoBehaviour
{
    public GameObject clearScreen;
    private bool isActive = false;

    public void OnClearMonster()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 1;
        FadeScreen.instance.StartFadeOutRoutine(2);
        StartCoroutine("OpenScreen", 2);
    }

    void Update()
    {
        if(isActive)
        {
            if(Input.GetKeyUp(KeyCode.Space))
            {
                GoBackToMapSequence();
            }
        }
    }

    IEnumerator OpenScreen(float dur)
    {
        yield return new WaitForSecondsRealtime(dur);
        FadeScreen.instance.ClearFadeScreen();
        isActive = true;
        clearScreen.SetActive(true);
        Time.timeScale = 0;
    }

    public void GoBackToMapSequence()
    {
        SceneManagerScript.instance.ChangeSceneBaseOnString("DaySequence");
    }

}
