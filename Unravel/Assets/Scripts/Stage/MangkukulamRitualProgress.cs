﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MangkukulamRitualProgress : MonoBehaviour
{
    public UnityEvent onRitualFinish;
    public GameObject visual;
    [Header("Progress Bar")]
    public Image progressBar;
    [Header("Required")]
    public MangkukulamRitual ritual;
    [Header("Help")]
    public GameObject helpItems;
    public Transform helpItemsPos;
    private float progress;
    private bool isActive = false;
    void Start()
    {
        CombatEvents.instance.onMangkukulamCastingRitual += AddRitualProgress;
        isActive = false;
        visual.SetActive(false);
    }

    void Update()
    {
        if(isActive)
        {
            progressBar.fillAmount = progress / 100f;
            if(progress>100)
            {
                progress = 100;
                RitualComplete();
            }
        }
    }

    public void MakeRitualInProgress()
    {
        visual.SetActive(true);
        isActive = true;
    }

    public void AddRitualProgress(float amountPerSecond)
    {
        if (isActive)
        {
            progress += amountPerSecond * Time.deltaTime;
        }
    
    }

    private void RitualComplete()
    {
        isActive = false;
        progress = 0;
        onRitualFinish.Invoke();
        GameManager.instance.OnPlayerDeath(PlayersData.instance.player);
        StartCoroutine("DelayedReset");
    }

    private void ResetRitual()
    {
        progressBar.fillAmount = progress / 100f;
        visual.SetActive(false);
        ritual.ResetRitual();
    }

    IEnumerator DelayedReset()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        ResetRitual();
        SendHelpItems();
    }    

    private void SendHelpItems()
    {
        Instantiate(helpItems, helpItemsPos.position, transform.rotation);
    }

}
