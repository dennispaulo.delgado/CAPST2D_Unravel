﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GateScene : MonoBehaviour
{
    public string sceneName;
    void Start()
    {
        
    }

    void Update()
    {
    }

    public void DelayedChangeScene(float dur)
    {
        StartCoroutine("DelayChangeScene", dur);
    }

    IEnumerator DelayChangeScene(float dur)
    {
        yield return new WaitForSecondsRealtime(dur);
        SceneManagerScript.instance.ChangeSceneBaseOnString(sceneName);
    }

    public void ChangeScene()
    {
       
        SceneManagerScript.instance.ChangeSceneBaseOnString(sceneName);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            SceneManagerScript.instance.ChangeSceneBaseOnString(sceneName);
        }
    }
}
