﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transport : MonoBehaviour
{
    public string sceneName;

    public int questIDComplete;

    public void InteractTranport()
    {

        ChoicesButtons.instance.gameObject.SetActive(true);
        ChoicesButtons.instance.yesButton.onClick.AddListener(ChangeScene);
        ChoicesButtons.instance.noButton.onClick.AddListener(ChangeMind);
         
        Time.timeScale = 0.1f;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void ChangeScene()
    {
        ObjectiveEvent.instance.OnQuestArrivedS(questIDComplete);
        SceneManagerScript.instance.ChangeSceneBaseOnString(sceneName);
    }
    public void ChangeSceneBaseWithString(string sceneNameF)
    {
        ObjectiveEvent.instance.OnQuestArrivedS(questIDComplete);
        SceneManagerScript.instance.ChangeSceneBaseOnString(sceneNameF);
    }
    public void ChangeMind()
    {
        ChoicesButtons.instance.NoChoice();
    }
}
