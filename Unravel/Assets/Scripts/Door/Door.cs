﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public GameObject door;
    private Vector3 originalRot;
    public bool isOpen = false;
    public bool canInteract = false;
    public float turnSmoothTime;
    float turnSmoothVelocity;
    public float maxSpeed;
    public float duration;
    private float defDur;
    void Start()
    {
        defDur = duration;
        originalRot = new Vector3(0, door.transform.rotation.eulerAngles.y, 0);
    }

    void Update()
    {
        if (isOpen&&duration>0)
        {
            float angle = Mathf.SmoothDampAngle(door.transform.eulerAngles.y, door.transform.eulerAngles.y-90, ref turnSmoothVelocity, turnSmoothTime);
            //door.transform.rotation = Quaternion.Euler(0f, angle, 0f);
            door.transform.eulerAngles = new Vector3(0, angle, 0);
            if(door.transform.localEulerAngles.y<270)
            {
                door.transform.localEulerAngles = new Vector3(0, 270, 0);
            }
            /*
            Vector3 rot = new Vector3(0, -90, 0);
            Vector3 rot2 = new Vector3(0, door.transform.localEulerAngles.y, 0);
            door.transform.localEulerAngles = Vector3.Slerp(rot2, rot, Time.deltaTime * turnSmoothTime);
            */
            /*
            Vector3 rot = new Vector3(-90 + this.transform.eulerAngles.y, 0, 0);
                Quaternion rot2 = Quaternion.LookRotation(rot);
                door.transform.rotation = Quaternion.Slerp(door.transform.rotation, rot2, Time.deltaTime * turnSmoothTime);*/
            duration -= Time.deltaTime;
        }
        else
        {
            
            float angle = Mathf.SmoothDampAngle(door.transform.eulerAngles.y, originalRot.y, ref turnSmoothVelocity, turnSmoothTime);
            door.transform.eulerAngles = new Vector3(0, angle, 0);
            isOpen = false;

            /*
            Vector3 rot = new Vector3(0, 0, 0);
            Vector3 rot2 = new Vector3(0, door.transform.localEulerAngles.y, 0);
            door.transform.localEulerAngles = Vector3.Slerp(rot2, rot, Time.deltaTime * turnSmoothTime);*/
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            //InteractDoor();
        }
    }

    public void InteractDoor()
    {
        if(canInteract)
        isOpen = true;
        duration = defDur;
    }
    public float Clamp0360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }
        return result;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            canInteract = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            canInteract = false;
        }
    }
}
