﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gate : MonoBehaviour
{
    public UnityEvent onGateOpen;
    public int keyIDNeeded;

    public float turnSmoothTime;
    float turnSmoothVelocity;

    public ObjectiveSlot slot;

    public bool isOpen = false;
    private bool questActive = false;
    public int questID;
    [Header("Gate Animator")]
    public List<Animator> animator;
    void Start()
    {
        //origPos = transform.position;
        if(isOpen)
        {
            foreach(Animator a in animator)
            {
                a.SetBool("isOpen", isOpen);
            }
            this.GetComponent<BoxCollider>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isOpen)
        {
            foreach (Animator a in animator)
            {
                a.SetBool("isOpen", isOpen);
            }
            this.GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            foreach (Animator a in animator)
            {
                a.SetBool("isOpen", isOpen);
            }
        }
    }

    public void GateCheat()
    {
        isOpen = true;
        onGateOpen.Invoke();
    }

    public void CheckIfYouHaveKey(Inventory inv)
    {
        Debug.Log("CheckIfHaveKeys");
        ObjectiveEvent.instance.OnQuestArrivedS(questID);
        Debug.Log("Inv : " +inv.GetInventoryList().Count);
        for (int i = 0; i < inv.GetInventoryList().Count; i++)
        {
            if (inv.GetInventoryList()[i].interactable.itemType == Item.ItemType.Key)
            {
                if (inv.GetInventoryList()[i].interactable.keyID == keyIDNeeded)
                {
                    onGateOpen.Invoke();
                    isOpen = true;
                    return;
                }
            }
        }
        ObjectiveHandler.instance.AddObjective(slot);
        if (!questActive)
        {
            questActive = true;
        }
    }
}
