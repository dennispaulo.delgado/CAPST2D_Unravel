﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuendeInteraction : MonoBehaviour
{
    public List<DuendeDeBuff> dDebuff;
    public GameObject debuffGO;
    [Header("Cooldown")]
    public bool hasCooldown = true;
    public float triggerCooldown;
    private float defTriggerCooldown;
    private bool isTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        defTriggerCooldown = triggerCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        if(isTriggered&&hasCooldown)
        {
            if( triggerCooldown<=0)
            {
                isTriggered = false;
                triggerCooldown = defTriggerCooldown;
            }
            else
            {
                triggerCooldown -= Time.deltaTime;
            }
        }
    }

    private void PlayerInteractWithDuende(GameObject player)
    {
        GameObject db = player.transform.Find("Debuffs").gameObject;
        GameObject duendeDebuff = Instantiate(debuffGO, db.transform);
        duendeDebuff.GetComponent<EnemyDebuff>().ReferenceDebuff(dDebuff[0]);
        isTriggered = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player"&&!isTriggered)
        {
            PlayerInteractWithDuende(other.gameObject);
        }
    }
}
