﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KapreInteraction : MonoBehaviour
{
    public ParticleSystem kapreFog;
    public GameObject mapBlock;
    public float breakDistance;
    public float killThreshold;
    private float defKillThreshold;
    private bool isActive = false;
    private Transform player;

    private void Start()
    {
        defKillThreshold = killThreshold;
    }

    private void Update()
    {
        if(isActive)
        {
            if(player!=null)
            {
                CheckPlayerDistance();
            }
            if (killThreshold>0)
            {
                killThreshold -= Time.deltaTime;
            }
            else
            {
                player.GetComponent<HealthScript>().DamageUnit(1000);
                ResetKapre();
            }
        }
    }

    private void CheckPlayerDistance()
    {
        float distance = Vector3.Distance(this.transform.position, player.position);
        if (distance > breakDistance)
        {
            ResetKapre();
        }
    }

    private void ResetKapre()
    {
        mapBlock.SetActive(false);
        player = null;
        ParticleSystem.MainModule main = kapreFog.main;
        main.loop = false;
        isActive = false;
        killThreshold = defKillThreshold;
        kapreFog.Stop();
    }

    private void PlayerInteractWithKapre(Transform other)
    {
        mapBlock.SetActive(true);
        player = other.transform;
        kapreFog.Play();
        ParticleSystem.MainModule main = kapreFog.main;
        main.loop = true;
        isActive = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            PlayerInteractWithKapre(other.transform);
        }
    }
}
