﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDebuff : MonoBehaviour
{
    private DuendeDeBuff debuff;
    void Start()
    {
        debuff.StartDebuff(this);
    }

    void Update()
    {
        debuff.Debuff();
    }
    
    public void ReferenceDebuff(DuendeDeBuff dBuf)
    {
        debuff = dBuf;
    }

    public void PassDuration(float duration)
    {
        DestroyThisIn(duration);
        StartCoroutine(DestroyThisIn(duration));
    }

    IEnumerator DestroyThisIn(float duration)
    {
        yield return new WaitForSeconds(duration);

        debuff.EndDebuff();
    }
}
