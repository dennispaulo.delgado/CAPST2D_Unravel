﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="DuendeDebuff/BattleSoundSuprise")]
public class DuendeDebuffBattleSound : DuendeDeBuff
{
    public override void Debuff()
    {
    }

    public override void EndDebuff()
    {
        Destroy(debuffObject.gameObject);
    }

    public override void StartDebuff(EnemyDebuff obj)
    {
        debuffObject = obj;
        debuffObject.GetComponent<AudioSource>().clip = audio;
        debuffObject.GetComponent<AudioSource>().Play();
        debuffObject.PassDuration(duration);
    }
}
