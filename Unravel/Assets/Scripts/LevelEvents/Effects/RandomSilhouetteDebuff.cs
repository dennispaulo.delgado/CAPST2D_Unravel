﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "DuendeDebuff/RandomSilhouette")]
public class RandomSilhouetteDebuff : DuendeDeBuff
{
    public override void Debuff()
    {
    }

    public override void EndDebuff()
    {
        Destroy(debuffObject.gameObject);
    }

    public override void StartDebuff(EnemyDebuff obj)
    {
        debuffObject = obj;
        //1 is starting left 2 is starting right
        int rng = Random.Range(1,3);
        Debug.Log("Rnd : "+rng);
        int dir =0;
        Vector3 offset = new Vector3();
        switch(rng)
        {
            case 1:
                dir = 1;
                offset = new Vector3(5, 0, 5);
                break;
            case 2:
                dir = -1;
                offset = new Vector3(-5, 0, 5);
                /*
                GameObject sil2 = Instantiate(silhouette, obj.transform.position + (obj.transform.forward + silhouettePos), Quaternion.identity); 
                Vector3 targetVector2 = obj.transform.position - sil2.transform.position;

                float newAngle2 = Mathf.Atan2(targetVector2.z, targetVector2.x) * Mathf.Rad2Deg;

                sil2.transform.rotation = Quaternion.Euler(0, -1 * newAngle2, 0);
                sil2.GetComponent<SilhouetteBehaviour>().SetReferences(10, dir, duration);*/
                break;
        }
        GameObject sil = Instantiate(silhouette, (obj.transform.position + offset) + (obj.transform.forward *2), Quaternion.identity);
        Vector3 targetVector = obj.transform.position - sil.transform.position;

        float newAngle = Mathf.Atan2(targetVector.z, targetVector.x) * Mathf.Rad2Deg;

        sil.transform.rotation = Quaternion.Euler(0, -1 * newAngle, 0);
        sil.GetComponent<SilhouetteBehaviour>().SetReferences(10, dir, duration);
        debuffObject.PassDuration(duration);
    }
}
