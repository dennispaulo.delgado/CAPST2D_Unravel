﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilhouetteBehaviour : MonoBehaviour
{
    private float speed;
    private int direction;
    public Transform graphic;
    private float duration;
    private Vector3 moveDirection;
    void Start()
    {
        
    }

    void Update()
    {
        if (duration > 0)
        {
            Vector3 targetVector = Camera.main.transform.position - this.transform.position;

            float newAngle = Mathf.Atan2(targetVector.z, targetVector.x) * Mathf.Rad2Deg;

            graphic.transform.rotation = Quaternion.Euler(0, -1 * newAngle, 0);

            //transform.Translate(moveDirection * speed * Time.deltaTime);
            duration -= Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }
    public void SetReferences(float spd, int drctn,float drtn)
    {
        speed = spd;
        direction = drctn;
        duration = drtn;
        switch(drctn)
        {
            case 1:
                //this.transform.position = new Vector3(this.transform.position.x - 5, this.transform.position.y, this.transform.position.z + 5);
                break;
            case -1:
                //this.transform.position = new Vector3(this.transform.position.x + 5, this.transform.position.y, this.transform.position.z + 5);
                break;
        }
        moveDirection = this.transform.right * -drctn;
    }
}
