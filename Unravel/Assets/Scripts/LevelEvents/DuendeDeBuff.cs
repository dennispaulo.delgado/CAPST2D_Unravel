﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DuendeDeBuff : ScriptableObject
{
    public AudioClip audio;
    public float duration;
    public GameObject silhouette;
    protected bool isFinish = false;
    protected EnemyDebuff debuffObject;

    public abstract void StartDebuff(EnemyDebuff obj);
    public abstract void Debuff();
    public abstract void EndDebuff();
}
