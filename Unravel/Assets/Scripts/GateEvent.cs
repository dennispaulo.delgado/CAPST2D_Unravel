﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateEvent : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Die.OnDie += Die_OnDie; //(object sender, System.EventArgs e) =>
       

        GameEvents.instance.clearListeners += ClearListeners;


    }

    private void Die_OnDie(object sender, EventArgs e)
    {
        Destroy(gameObject);
    }

    public void ClearListeners()
    {
        Die.OnDie -= Die_OnDie;

        GameEvents.instance.clearListeners -= ClearListeners;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
