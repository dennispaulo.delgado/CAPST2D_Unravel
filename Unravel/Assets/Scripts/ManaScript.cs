﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaScript : MonoBehaviour
{
    public float currentMana;
    public float maxMana;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void AddMana(float add)
    {
        if(currentMana+add>maxMana)
        {
            currentMana = maxMana;
        }
        else
        {
            currentMana += add;
        }
    }

    public bool EnoughToUseAbility(Ability ablty)
    {
        if(currentMana<ablty.manaCost)
        {
            return false;
        }
        else
        {
            UseAbility(ablty.manaCost);
            return true;
        }
    }

    public void UseAbility(float cost)
    {
        currentMana -= cost;
    }

}
