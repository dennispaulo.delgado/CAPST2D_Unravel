﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpInteractBehavior : MonoBehaviour
{
    private IPickUp pickup;
    private float interactRange = 1f;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.DrawRay(transform.position, transform.forward, Color.red, 5f);
            //Collider[] colliderArray = Physics.OverlapSphere(transform.position, interactRadius);
            RaycastHit hit;
            if(Physics.Raycast(new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z) ,transform.forward, out hit, interactRange))
            {
                Interactable interactable = hit.collider.GetComponent<Interactable>();
                if (interactable)
                {
                    GameObject pickUpObject = interactable.gameObject;
                    pickup = pickUpObject.GetComponent<IPickUp>();
                }

                if(pickup != null)
                {
                    //There is an item in range;
                    pickup.PickUpObject();
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            if (pickup != null)
            {
                pickup.DropObject();
            }
           
        }


        if (Input.GetKeyDown(KeyCode.F))
        {
            if(pickup != null)
            {
                pickup.ThrowObject();
            }

        }
    }




}
