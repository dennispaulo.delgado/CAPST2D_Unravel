﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class SpecialSkill : State
{
    static public EventHandler OnManananggalSpecial;
    static public EventHandler OnAswangSpecial;
    static public EventHandler OnManananggalSpecialFinished;
    static public EventHandler OnMangkukulamSpecialFinished;

    int frameCount = 0;

    float summonChannelingDuration = 2f;


    public SpecialSkill(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.SPECIALSKILL;
    }

    public override void Enter()
    {
        Debug.Log("Enter Special Skill");
        anim.SetTrigger("IsSpecial");
        SpecialMove();
        base.Enter();
    }


    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();
        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:

                if (!player.GetComponent<HealthScript>().GetGrip())
                {
                    anim.SetBool("IsMidAnim", false);
                    OnManananggalSpecialFinished?.Invoke(this, EventArgs.Empty);
                    player.GetComponent<ThirdPersonMovement>().canMove = true;

                    if (npc.GetComponent<Enemy>().GetIsStunned())
                    {
                        nextState = new Stun(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }
                    else
                    {
                        nextState = new Chase(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }

                }
                else
                {
                    anim.SetBool("IsMidAnim", true);
                }

                frameCount++;
                break;

            case EnemyType.EnemyTypes.Mangkukulam:
                {
                    summonChannelingDuration -= Time.deltaTime;
                    
                    if(summonChannelingDuration <= 0)
                    {
                        Debug.Log(summonChannelingDuration);
                        npc.GetComponent<Enemy>().SetSummonComplete(true);
                    }

                    if (npc.GetComponent<Enemy>().GetSummonComplete())
                    {
                        anim.SetBool("IsMidAnimSpecial", false);
                        nextState = new Return(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }
                    else
                    {
                        anim.SetBool("IsMidAnimSpecial", true);
                    }
                    break;
                }

            case EnemyType.EnemyTypes.MangkukulamIllusion:
                {
                    summonChannelingDuration -= Time.deltaTime;

                    if (summonChannelingDuration <= 0)
                    {
                        Debug.Log(summonChannelingDuration);
                        npc.GetComponent<Enemy>().SetSummonComplete(true);
                    }

                    if (npc.GetComponent<Enemy>().GetSummonComplete())
                    {
                        anim.SetBool("IsMidAnimSpecial", false);
                        nextState = new Chase(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }
                    else
                    {
                        anim.SetBool("IsMidAnimSpecial", true);
                    }
                    break;
                }


            case EnemyType.EnemyTypes.Aswang:
                if (!player.GetComponent<HealthScript>().GetBite())
                {
                    player.GetComponent<ThirdPersonMovement>().canMove = true;
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                break;

            case EnemyType.EnemyTypes.Aswang2:
                if (!player.GetComponent<HealthScript>().GetBite())
                {
                    player.GetComponent<ThirdPersonMovement>().canMove = true;
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                break;
        }

       

    }

    public override void Exit()
    {
        agent.isStopped = false;
        anim.ResetTrigger("IsSpecial");
        npc.GetComponent<Enemy>().SetSummonComplete(false);
        base.Exit();
    }


    private void SpecialMove()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                Debug.Log("Grip");
                player.GetComponent<ThirdPersonMovement>().canMove = false;
                npc.GetComponent<Enemy>().SetSpecialCD(true);
                OnManananggalSpecial?.Invoke(this, EventArgs.Empty);
                break;


            case EnemyType.EnemyTypes.Mangkukulam:
                Debug.Log("Summon Lesser Wisp");
                for(int i = 0; i < 2; i++)
                {
                    Vector3 enemySpawnPosition = npc.transform.position + GetRandomDir() * 20f;
                    Enemy enemy = npc.GetComponent<Enemy>().CreateMinions(enemySpawnPosition, npc.GetComponent<MangkukulamSummons>());
                    enemy.SetIsSummoned(true);
                    OnMangkukulamSpecialFinished?.Invoke(this, EventArgs.Empty);


                }
              
                break;

            case EnemyType.EnemyTypes.MangkukulamIllusion:
                Debug.Log("Summon Lesser Wisp");
                for (int i = 0; i < 4; i++)
                {
                    Vector3 enemySpawnPosition = npc.transform.position + GetRandomDir() * 20f;
                    Enemy enemy = npc.GetComponent<Enemy>().CreateMinions(enemySpawnPosition, npc.GetComponent<MangkukulamSummons>());
                    enemy.SetIsSummoned(true);
                    OnMangkukulamSpecialFinished?.Invoke(this, EventArgs.Empty);


                }

                break;

            case EnemyType.EnemyTypes.Aswang:
                Debug.Log("Bite");
                agent.isStopped = true;
                player.GetComponent<ThirdPersonMovement>().canMove = false;
                npc.GetComponent<Enemy>().SetSpecialCD(true);
                OnAswangSpecial?.Invoke(this, EventArgs.Empty);
                break;

            case EnemyType.EnemyTypes.Aswang2:
                Debug.Log("Bite");
                agent.isStopped = true;
                player.GetComponent<ThirdPersonMovement>().canMove = false;
                npc.GetComponent<Enemy>().SetSpecialCD(true);
                OnAswangSpecial?.Invoke(this, EventArgs.Empty);
                break;
        }
    }


    private static Vector3 GetRandomDir()
    {
        return new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f)).normalized;
    }


}
