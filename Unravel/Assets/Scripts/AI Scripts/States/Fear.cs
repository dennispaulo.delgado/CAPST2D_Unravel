﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public class Fear : State
{

    public Fear(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.FEAR;
        agent.speed = 5f;
    }

    public override void Enter()
    {
        Debug.Log("isFeared");
        RandomLocation();

        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Return(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind() == false)
                {
                    Debug.Log("Not Blind anymore returning to idle");
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;

                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                if(npc.GetComponent<Enemy>().GetBlind() == false)
                {
                    Debug.Log("Mangkukulam not Blind anymore returning to idle");
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                break;
                #endregion


            case EnemyType.EnemyTypes.Aswang:
                #region Aswang
                if (npc.GetComponent<Enemy>().GetBlind() == false)
                {
                    Debug.Log("Aswang not Blind anymore returning to idle");
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                break;
            #endregion


            case EnemyType.EnemyTypes.Aswang2:
                #region Aswang
                if (npc.GetComponent<Enemy>().GetBlind() == false)
                {
                    Debug.Log("Aswang not Blind anymore returning to idle");
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                break;
                #endregion

        }





    }



    public override void Exit()
    {

        //Reset anim
        base.Exit();
    }


    private void RandomLocation()
    {
        Debug.Log("RandomLocation");
        float randX = UnityEngine.Random.Range(-5, 5);
        float randZ = UnityEngine.Random.Range(-5, 5);
        Vector3 newLocation = new Vector3(randX, npc.transform.position.y, randZ);


        Vector3 moveDir = (player.transform.position - npc.transform.position);
        agent.SetDestination(npc.transform.position - moveDir);
    }



}
