﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RitualCasting : State
{
    public static EventHandler OnRitualCasting;

    public RitualCasting(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.CASTING;
        agent.speed = 0;
    }

    public override void Enter()
    {
        Debug.Log("Monster is Ritual Casting");
        //Do Anim
        anim.SetTrigger("IsCasting");
        OnRitualCasting?.Invoke(this, EventArgs.Empty);
        npc.GetComponent<Enemy>().GetRitualVFX().gameObject.SetActive(true);

        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();
        switch (type.enemyTypes)
        {
            
            case EnemyType.EnemyTypes.Mangkukulam:
                CombatEvents.instance.MangkukulamCastingRitualProgress(2f);
                anim.SetBool("IsMidAnimCasting", true);
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    CombatEvents.instance.OnMangkukulamStun();
                    anim.SetTrigger("IsCastingEnd");
                    //npc.GetComponent<Enemy>().SetProvokeCounter(npc.GetComponent<Enemy>().GetProvokeCounter() + 1); // increase provoke counter
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                
                break;
        }
    }

    public override void Exit()
    {
        //Reset anim
        npc.GetComponent<Enemy>().GetRitualVFX().gameObject.SetActive(false);
        anim.ResetTrigger("IsCasting");
        anim.SetBool("IsMidAnimCasting", false);
        anim.ResetTrigger("IsCastingEnd");
        base.Exit();
    }
}
