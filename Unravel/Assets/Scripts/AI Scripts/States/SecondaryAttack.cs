﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SecondaryAttack : State
{

    public SecondaryAttack(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.SECONDARYATTACK;
    }

    public override void Enter()
    {
        Debug.Log("Enter Secondary Attack");

        if(npc.GetComponent<Enemy>().GetEnemyTypeSO().enemyTypes == EnemyType.EnemyTypes.Mangkukulam)
        {
            MangkukulamFireball.OnCastFireBall += MangkukulamFireball_OnCastFireBall;
            
        }

        if (npc.GetComponent<Enemy>().GetEnemyTypeSO().enemyTypes == EnemyType.EnemyTypes.MangkukulamIllusion)
        {
            MangkukulamFireball.OnCastFireBall += MangkukulamFireball_OnCastFireBall;

        }

        if (npc.GetComponent<Enemy>().GetEnemyTypeSO().enemyTypes == EnemyType.EnemyTypes.MangkukulamIllusionFinal)
        {
            MangkukulamFireball.OnCastFireBall += MangkukulamFireball_OnCastFireBall;

        }

        base.Enter();
    }

    private void MangkukulamFireball_OnCastFireBall(object sender, EventArgs e)
    {
        FinishFireBall();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Die(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                SecondaryBasicAttack();

                if (CanAttackPlayer())
                {
                    npc.GetComponent<Enemy>().SetCoolDown(true);
                    Debug.Log(npc.GetComponent<Enemy>().GetCoolDown());
                    agent.speed = 3.5f;
                    agent.angularSpeed = 120f;
                    nextState = new Attack(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                agent.speed = 0f;
                
                SecondaryBasicAttack();
                if (npc.GetComponent<Enemy>().GetCoolDown())
                {
                    //agent.speed = 10f;
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                break;
            #endregion

            case EnemyType.EnemyTypes.MangkukulamIllusion:
                #region Mangkukulam Illusion
                agent.speed = 0f;
                SecondaryBasicAttack();
                if (npc.GetComponent<Enemy>().GetCoolDown())
                {
                    //agent.speed = 10f;
                    nextState = new Idle(npc, agent, anim, player, audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.MangkukulamIllusionFinal:
                #region Mangkukulam Illusion
                agent.speed = 0f;
                SecondaryBasicAttack();
                if (npc.gameObject.GetComponent<Enemy>().GetHealth() <= 0)
                {
                    nextState = new Die(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetCoolDown())
                {
                    //agent.speed = 10f;
                    nextState = new Idle(npc, agent, anim, player, audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang:
                #region Aswang

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                SecondaryBasicAttack();
                if (npc.GetComponent<Enemy>().GetCoolDown())
                {
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang2:
                #region Aswang
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                SecondaryBasicAttack();
                if (npc.GetComponent<Enemy>().GetCoolDown())
                {
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                break;
                #endregion
        }



    }

    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                anim.ResetTrigger("IsChasing1");
                break;

            case EnemyType.EnemyTypes.Mangkukulam:

                //MangkukulamFireball.OnCastFireBall -= MangkukulamFireball_OnCastFireBall;
                anim.ResetTrigger("IsFireBlast");
                break;

            case EnemyType.EnemyTypes.MangkukulamIllusion:

                //MangkukulamFireball.OnCastFireBall -= MangkukulamFireball_OnCastFireBall;
                anim.ResetTrigger("IsFireBlast");
                break;

            case EnemyType.EnemyTypes.MangkukulamIllusionFinal:

                //MangkukulamFireball.OnCastFireBall -= MangkukulamFireball_OnCastFireBall;
                anim.ResetTrigger("IsFireBlast");
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsRunning");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsRunning");
                break;
        }


        base.Exit();
    }
    

    private void SecondaryBasicAttack()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                //npc.GetComponent<Enemy>().SetCoolDown(true);
                agent.SetDestination(player.position);
                agent.speed = 10f;
                agent.angularSpeed = 200f;
                if (agent.remainingDistance < 3)
                {
                    npc.GetComponent<Enemy>().SetCoolDown(true);
                }

                Debug.Log("Manananggal Lunges");
                anim.SetTrigger("IsChasing1");
                break;

            case EnemyType.EnemyTypes.Mangkukulam:

                anim.SetTrigger("IsFireBlast");
                //npc.GetComponent<Enemy>().SetCoolDown(true);

                break;

            case EnemyType.EnemyTypes.MangkukulamIllusion:

                anim.SetTrigger("IsFireBlast");
                //npc.GetComponent<Enemy>().SetCoolDown(true);

                break;


            case EnemyType.EnemyTypes.MangkukulamIllusionFinal:

                anim.SetTrigger("IsFireBlast");
                //npc.GetComponent<Enemy>().SetCoolDown(true);

                break;

            case EnemyType.EnemyTypes.Aswang:
                agent.SetDestination(player.position);
                agent.speed = 15f;
                agent.angularSpeed = 200f;
                agent.stoppingDistance = 2;
                if(agent.remainingDistance < 3)
                {
                    npc.GetComponent<Enemy>().SetCoolDown(true);
                }
                anim.SetTrigger("IsRunning");

                Debug.Log("Aswang Lunges");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                agent.SetDestination(player.position);
                agent.speed = 15f;
                agent.angularSpeed = 200f;
                agent.stoppingDistance = 2;
                if (agent.remainingDistance < 3)
                {
                    npc.GetComponent<Enemy>().SetCoolDown(true);
                }
                anim.SetTrigger("IsRunning");

                Debug.Log("Aswang Lunges");
                break;
        }
    }

    public void FinishFireBall()
    {
        var rangeAttribute = npc.GetComponent<EnemyRangeAttribute>();

        rangeAttribute.CreateFireBall();
        npc.GetComponent<Enemy>().SetCoolDown(true);
        

    }

}
