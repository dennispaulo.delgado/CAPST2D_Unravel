﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : State
{
    public static EventHandler OnReachStartingLocation;

    int currentIndex = -1;
    float patrolDuration = 30f;
    float tempPartrolDuration;


    public Patrol(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.PATROL;
        agent.speed = 2;
        agent.isStopped = false;
    }

    public override void Enter()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();
        var waypoint = npc.GetComponent<Enemy>().GetWaypoints().GetWaypointList();
        float lastDist = Mathf.Infinity;

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                //float lastDist = Mathf.Infinity;
                audioSource.clip = npc.GetComponent<ManananggalSounds>().idleSound;
                audioSource.loop = true;
                audioSource.Play();
                agent.speed = 7;
                

                for (int i = 0; i < waypoint.Count; i++)
                {
                    Transform thisWP = waypoint[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex = i - 1;
                        lastDist = distance;
                    }
                }

                Debug.Log("isWalking");
                anim.SetTrigger("IsIdle");
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                agent.speed = 8;
                patrolDuration = 15f;
                for (int i = 0; i < waypoint.Count; i++)
                {
                    Transform thisWP = waypoint[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex = i - 1;
                        lastDist = distance;
                    }
                }
                anim.SetTrigger("IsPatrolling");
                break;


            #endregion

            case EnemyType.EnemyTypes.MangkukulamIllusion:
                #region Mangkukulam
                agent.speed = 8;
                patrolDuration = 15f;
                for (int i = 0; i < waypoint.Count; i++)
                {
                    Transform thisWP = waypoint[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex = i - 1;
                        lastDist = distance;
                    }
                }
                anim.SetTrigger("IsPatrolling");
                break;


            #endregion

            case EnemyType.EnemyTypes.LesserFlameWisps:
                #region Lesser Flame Wisp
                float randX = UnityEngine.Random.Range(-5, 5);
                float randZ = UnityEngine.Random.Range(-5, 5);

                Vector3 randLocation = new Vector3(randX, npc.transform.position.y, randZ);

                agent.SetDestination(randLocation);

                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang:
                #region Aswang
                tempPartrolDuration = patrolDuration;
                for (int i = 0; i < waypoint.Count; i++)
                {
                    Transform thisWP = waypoint[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex = i - 1;
                        lastDist = distance;
                    }
                }

                anim.SetTrigger("IsWalking");
                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang2:
                #region Aswang
                tempPartrolDuration = patrolDuration;
                for (int i = 0; i < waypoint.Count; i++)
                {
                    Transform thisWP = waypoint[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex = i - 1;
                        lastDist = distance;
                    }
                }

                anim.SetTrigger("IsWalking");
                break;
                #endregion
        }

        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();
        var waypoint = npc.GetComponent<Enemy>().GetWaypoints().GetWaypointList();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Die(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetAggro())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetAlerted())
                {
                    nextState = new Investigate(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if(player.GetComponent<HealthScript>().curHealth <= 100)
                {
                    AswangObsEvent.instance.OnAddManananggalObs(1);
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (agent.remainingDistance < 1)
                {
                    if (currentIndex >= waypoint.Count - 1)
                    {
                        currentIndex = 0;
                    }
                    else
                    {
                        currentIndex++;
                    }
                    agent.SetDestination(waypoint[currentIndex].transform.position);
                }




                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                
                patrolDuration -= Time.deltaTime;
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    CombatEvents.instance.OnMangkukulamStun();
                    nextState = new Stun(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (patrolDuration <= 0)
                {
                    nextState = new Return(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (agent.remainingDistance < 1)
                {
                    if (currentIndex >= waypoint.Count - 1)
                    {
                        currentIndex = 0;
                    }
                    else
                    {
                        currentIndex++;
                    }
                    agent.SetDestination(waypoint[currentIndex].transform.position);
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim,player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.MangkukulamIllusion:
                #region Mangkukulam

                patrolDuration -= Time.deltaTime;
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    //npc.GetComponent<Enemy>().DecreaseIllusionLife();
                    CombatEvents.instance.OnMangkukulamStun();
                    nextState = new Stun(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetAlerted())
                {
                    nextState = new Chase(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetAggro())
                {
                    nextState = new Chase(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (agent.remainingDistance < 1)
                {
                    if (currentIndex >= waypoint.Count - 1)
                    {
                        currentIndex = 0;
                    }
                    else
                    {
                        currentIndex++;
                    }
                    agent.SetDestination(waypoint[currentIndex].transform.position);
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player, audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.LesserFlameWisps:
                #region Lesser Flame Wisp
                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (agent.remainingDistance < 1)
                {
                
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;

                }

                break;
                #endregion


            case EnemyType.EnemyTypes.Aswang:
                #region Aswang
                patrolDuration -= Time.deltaTime;
                if(patrolDuration <= 0)
                {
                    nextState = new Return(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim,player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetAlerted())
                {
                    nextState = new Investigate(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (agent.remainingDistance < 1)
                {
                    if (currentIndex >= waypoint.Count - 1)
                    {
                        currentIndex = 0;
                    }
                    else
                    {
                        currentIndex++;
                    }
                    agent.SetDestination(waypoint[currentIndex].transform.position);
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang2:
                #region Aswang
                patrolDuration -= Time.deltaTime;

                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (patrolDuration <= 0)
                {
                    nextState = new Return(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetAlerted())
                {
                    nextState = new Investigate(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (agent.remainingDistance < 1)
                {
                    if (currentIndex >= waypoint.Count - 1)
                    {
                        currentIndex = 0;
                    }
                    else
                    {
                        currentIndex++;
                    }
                    agent.SetDestination(waypoint[currentIndex].transform.position);
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
                #endregion
        }

    }

    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                //reset anim
                anim.ResetTrigger("IsIdle");
                break;

            case EnemyType.EnemyTypes.Mangkukulam:
                //reset anim
                anim.ResetTrigger("IsPatrolling");
                break;

            case EnemyType.EnemyTypes.MangkukulamIllusion:
                //reset anim
                anim.ResetTrigger("IsPatrolling");
                break;


            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsWalking");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsWalking");
                break;
        }


        base.Exit();
    }
}
