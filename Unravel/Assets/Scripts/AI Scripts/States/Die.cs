﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Die : State
{
    static public EventHandler OnDie;

    public Die(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.DIE;
    }

    public override void Enter()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                {
                    Debug.Log("isDead");
                    audioSource.clip = npc.GetComponent<ManananggalSounds>().deadSound;
                    audioSource.loop = false;
                    audioSource.Play();

                    npc.gameObject.SetActive(false);
                    OnDie?.Invoke(this, EventArgs.Empty);
                    break;
                }


            case EnemyType.EnemyTypes.MangkukulamIllusion:
                {
                    //Add Effects

                    Debug.Log("Illusion is Destroyed");
                    npc.gameObject.SetActive(false);
                    OnDie?.Invoke(this, EventArgs.Empty);
                    break;
                }

            case EnemyType.EnemyTypes.LesserFlameWisps:
                {
                    npc.GetComponent<WispDeathEffect>().SpawnDeathEffect();
                    npc.gameObject.SetActive(false);
                    OnDie?.Invoke(this, EventArgs.Empty);
                    break;
                }
        }
       
        base.Enter();
    }


    public override void Update()
    {
       
    }


    public override void Exit()
    {
        base.Exit();
    }
}
