﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flee : State
{
    public Flee(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.FLEE;
        agent.speed = 10f;
    }

    public override void Enter()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();


        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                //Set Manananggal Anim//
                anim.SetTrigger("IsChasing1");
                break;
              
        }
        base.Enter();
    }


    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                
                    nextState = new Return(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                
                break;
        }
    }


    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                //Reset anim//
                anim.ResetTrigger("IsChasing1");
                break;
        }
        base.Exit();
    }


}
