﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Return : State
{
    static public EventHandler OnEndChase;

    GameObject returnLocation;

    public Return(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.RETURN;
    }

    public override void Enter()
    {
        Debug.Log("IsReturning");
        agent.isStopped = false;
        agent.speed = 10;


        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                returnLocation = GameObject.FindGameObjectWithTag("Return");
                OnEndChase?.Invoke(this, EventArgs.Empty);
                //Added Smoke Particle
                //Disable Manananggal Object
                anim.SetTrigger("IsChasing1");
                agent.SetDestination(returnLocation.transform.position);

                break;

            case EnemyType.EnemyTypes.Mangkukulam:
                //set anim;
                returnLocation = npc.GetComponent<Enemy>().GetReturnLocation().gameObject;
                agent.SetDestination(returnLocation.transform.position);
                break;


            case EnemyType.EnemyTypes.Aswang:
                returnLocation = GameObject.FindGameObjectWithTag("Return");
                var prefab = npc.transform.Find("Aswang");
                if (!prefab.gameObject.activeInHierarchy)
                {
                    prefab.gameObject.SetActive(true);
                }
                anim.SetTrigger("IsWalking");
                agent.SetDestination(returnLocation.transform.position);
                break;


            case EnemyType.EnemyTypes.Aswang2:
                returnLocation = GameObject.FindGameObjectWithTag("Return2");
                prefab = npc.transform.Find("Aswang");
                if (!prefab.gameObject.activeInHierarchy)
                {
                    prefab.gameObject.SetActive(true);
                }
                anim.SetTrigger("IsWalking");
                agent.SetDestination(returnLocation.transform.position);
                break;
        }

        base.Enter();
    }


    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.transform.position.x == returnLocation.transform.position.x && npc.transform.position.z == returnLocation.transform.position.z)
                {
                    if (!npc.GetComponent<Enemy>().GetIsTutorialMode())
                    {
                        nextState = new Die(npc, agent, anim, player, audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                    else
                    {
                        npc.transform.gameObject.SetActive(false);
                    }
                }
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player, audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                var distance = Vector3.Distance(npc.transform.position, returnLocation.transform.position);

                if (distance < 10f)
                {
                    nextState = new RitualCasting(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang:
                #region Aswang
                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                if (npc.transform.position.x == returnLocation.transform.position.x && npc.transform.position.z == returnLocation.transform.position.z)
                {
                    nextState = new Feast(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion


            case EnemyType.EnemyTypes.Aswang2:
                #region Aswang
                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }


                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                if (npc.transform.position.x == returnLocation.transform.position.x && npc.transform.position.z == returnLocation.transform.position.z)
                {
                    nextState = new Feast(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
                #endregion


        }
    }


    public override void Exit()
    {

        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                anim.ResetTrigger("IsChasing1");
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsWalking");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsWalking");
                break;
        }
        //Reset anim
        base.Exit();
    }
}
