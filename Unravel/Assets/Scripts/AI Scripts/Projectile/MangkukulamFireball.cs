﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MangkukulamFireball : MonoBehaviour
{
    public static EventHandler OnCastFireBall;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CastFireBall()
    {
        OnCastFireBall?.Invoke(this, EventArgs.Empty);
    }
}
