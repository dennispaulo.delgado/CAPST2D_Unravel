﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeAttribute : MonoBehaviour
{
    

    [SerializeField] private float damage = 20f;
    [SerializeField] private float range = 50f;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform firePoint;


    private void Awake()
    {
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateFireBall()
    {
        var projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
    }

    

    public Transform GetFirePoint()
    {
        return firePoint;
    }

    

    public float GetRange()
    {
        return range;
    }

    public float GetDamage()
    {
        return damage;
    }
}
