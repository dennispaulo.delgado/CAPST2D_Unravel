﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpseWaypoints : MonoBehaviour
{
    [SerializeField] private List<Transform> waypointList;


    public List<Transform> GetWaypointList()
    {
        return waypointList;
    }
}
