﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GateKeyThreshold : MonoBehaviour
{
    public static EventHandler OnGateKeyDrop;
    public UnityEvent OnDropKey;
    

    private int stunsRequired = 3;
    private int stunCount;
    [SerializeField] ItemWorldSpawner GateKey;

    // Start is called before the first frame update
    void Start()
    {
        Stun.OnStun += Stun_OnStun;
    }

    private void Stun_OnStun(object sender, EventArgs e)
    {
        stunCount++;
        Debug.Log(stunCount);
    }

    // Update is called once per frame
    void Update()
    {

        if(stunCount >= stunsRequired)
        {
            if (GateKey != null)
            {
                GameObject pos = Instantiate(GateKey.gameObject, this.transform.position, transform.rotation);
                GateKey.shouldSpawn = true;
                OnDropKey.Invoke();
                ChangeCamera.instance.ChangeCameraTo(pos.transform); 
                OnGateKeyDrop?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                Debug.Log("GateKey ItemWorldSpawner is missing");
            }
            
        }
    }

    public int GetStunRequired()
    {
        return stunCount;
    }
}
