﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
    [SerializeField] List<Transform> waypoints;

    public List<Transform> GetWaypointList()
    {
        return waypoints;
    }

    
}
