﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WispDeathEffect : MonoBehaviour
{
    [SerializeField] private GameObject deathEffect;


    public void SpawnDeathEffect()
    {
        Instantiate(deathEffect, this.transform.position, Quaternion.identity);
    }
}
