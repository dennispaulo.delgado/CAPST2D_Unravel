﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static event EventHandler OnGateKeyThreshold;
    

    [SerializeField] private EnemyTypeSO EnemyTypeSO;
    [SerializeField] private SkinnedMeshRenderer skinnedMeshRenderer;
    [SerializeField] private Waypoints waypoints;
    [Header("-----Only for Aswang-----")] [SerializeField] private CorpseWaypoints corpseWaypoints;
    [Header("-------------------------")]
    [SerializeField] private float health;
    [SerializeField] public bool isTutorialMode;
    [SerializeField] private ParticleSystem stunEffect;
    


    private bool isDead = false;
    private bool isBlind = false;
    private bool itemCollected = false;
    private float blindDuration = 3f;

    //Skill1 Setting//
    [SerializeField] private float skillCD = 20f;
    private float tempCD;
    private bool onCoolDown = false;
    private int usagePercentage = 10;

    //Special Skill Setting//
    [SerializeField] private float specialCD = 30f;
    private float specialTempCD;
    private bool onSpecialCoolDown = false;
    private int specialUsagePercentage = 100;

    //threshold--//
    [SerializeField] private int stunThreshold;
    private bool isStunned = false;
    private float stunDuration = 7f;

    //Aggro Setting//
    private bool isAggro = false;
    private float aggroDuration = 10f;
    private float tempAggroDuration;

    //Alert Setting//
    private bool isAlerted = false;
    [SerializeField] private Vector3 locationAlerted;
    private float alertDuration = 20f;
    private float tempAlertDuration;

    private bool isEnrage = false;
    [SerializeField] private bool hitFeedback = false;
    [SerializeField] private ParticleSystem hitFeedbackVFX;
    float feedbackDuration = 0.5f;

    [Header("Mangkukulam Variable")]
    [SerializeField] private int provokeCounter = 0;
    [SerializeField] private bool summonComplete;
    [SerializeField] private Transform returnLocation;
    [SerializeField] private int illusionLife = 4;
    [SerializeField] private ParticleSystem ritualVFX;
    private bool isSummoned;
    bool hasDecrease = false;


    public Enemy CreateEnemy(Vector3 position)
    {
        Transform pfEnemy = EnemyTypeSO.prefab.transform;
        Transform enemyTransform = Instantiate(pfEnemy, position, Quaternion.identity);

        Enemy enemy = enemyTransform.GetComponent<Enemy>();

        return enemy;
    }

    public Enemy CreateMinions(Vector3 position, MangkukulamSummons prefab)
    {
        MangkukulamSummons pfEnemy = prefab;
        Transform enemyTransform = Instantiate(pfEnemy.GetSummonMinion().transform, position, Quaternion.identity);

        Enemy enemy = enemyTransform.GetComponent<Enemy>();

        return enemy;
    }

    private void Awake()
    {
        health = EnemyTypeSO.health;
        if(EnemyTypeSO.enemyTypes == EnemyType.EnemyTypes.LesserFlameWisps)
        {
            health = UnityEngine.Random.Range(EnemyTypeSO.health, EnemyTypeSO.health + 10);
        }

        stunThreshold = EnemyTypeSO.maxStunThreshold;

    }


    // Start is called before the first frame update
    public virtual void Start()
    {
        tempCD = skillCD;
        specialTempCD = specialCD;
        tempAggroDuration = aggroDuration;

        BlessAbilityBehaviour.OnBlindUnit += BlessAbilityBehaviour_OnBlindUnit;
        Gun.OnGunShot += Gun_OnGunShot;

        Die.OnDie += Die_OnDie; //(object sender, EventArgs e) => { isDead = true;  Debug.Log(isDead); };
        PistolProjectile.OnEnemyHit += PistolProjectile_OnEnemyHit;     
        if(EnemyTypeSO.enemyTypes != EnemyType.EnemyTypes.LesserFlameWisps)
        {
            PistolProjectile.OnBossEnemyHit += PistolProjectile_OnBossEnemyHit;
           
        }
        ButtonMashControl.OnButtonMashFinished += ButtonMashControl_OnButtonMashFinished;
        Fireball.OnFireballHit += Fireball_OnFireballHit;
        Freeze.OnFreezeHit += Freeze_OnFreezeHit;
        LightEffect.OnLightHit += LightEffect_OnLightHit;
        Fireball.OnFireballShot += Fireball_OnFireballShot;
        Freeze.OnFreezeShot += Freeze_OnFreezeShot;
        CorpseObjective.OnObjectiveComplete += CorpseObjective_OnObjectiveComplete;
        SpecialSkill.OnMangkukulamSpecialFinished += SpecialSkill_OnMangkukulamSpecialFinished;
        GameEvents.instance.clearListeners += ClearListeners;

    }

    private void PistolProjectile_OnBossEnemyHit(object sender, PistolProjectile.OnBossEnemyHitArgs e)
    {
        e.enemy.stunThreshold -= 10;
        if (!isAggro) e.enemy.isAggro = true;
        if (!hitFeedback) e.enemy.hitFeedback = true;
        Instantiate(hitFeedbackVFX.gameObject, this.transform.position, Quaternion.identity);
       
            
        
    }

    private void PistolProjectile_OnEnemyHit(object sender, PistolProjectile.OnEnemyHitArgs e)
    {
        if (this == e.enemy)
        {
            if (e.enemy.EnemyTypeSO.enemyTypes == EnemyType.EnemyTypes.LesserFlameWisps)
            {
                e.enemy.health -= 10;
                if (!isAggro) e.enemy.isAggro = true;
                if (!hitFeedback) e.enemy.hitFeedback = true;
                Instantiate(hitFeedbackVFX.gameObject, this.transform.position, Quaternion.identity);
            }

        }
    }

    private void Die_OnDie(object sender, EventArgs e)
    {
        isDead = true;
        Debug.Log(isDead);
    }

    private void SpecialSkill_OnMangkukulamSpecialFinished(object sender, EventArgs e)
    {
        Debug.Log("Mangkukulam Special Finished");
        SetSummonComplete(true);
        //Invoke("SummonChanneling", 2f);
    }

    void CorpseObjective_OnObjectiveComplete(object sender, EventArgs e)
    {
        isEnrage = true;
    }


    void Fireball_OnFireballShot(object sender, EventArgs e)
    {
        isAlerted = true;
    }

    void Freeze_OnFreezeShot(object sender, EventArgs e)
    {
        isAlerted = true;
    }

    void Fireball_OnFireballHit(object sender, EventArgs e)
    {
        //Debug.Log("istunned by fireall");
        isStunned = true;
    }

    void Freeze_OnFreezeHit(object sender, EventArgs e)
    {
        isStunned = true;
    }

    void LightEffect_OnLightHit(object sender, EventArgs e)
    {
        isStunned = true;
    }

    void ButtonMashControl_OnButtonMashFinished(object sender, EventArgs e)
    {
        isStunned = true;
    }


    void Gun_OnGunShot(object sender, EventArgs e)
    {
        float dist = Vector3.Distance(this.GetComponent<EnemyAI>().player.position, this.transform.position);
        Debug.Log(dist);

        if(dist < 100)
        {
            isAlerted = true;
            locationAlerted = GetComponent<EnemyAI>().player.position;
            Debug.Log("isAlerted");
        }
        Debug.Log("Not close enough to be alerted");


    }


    void BlessAbilityBehaviour_OnBlindUnit(object sender, EventArgs e)
    {
        isBlind = true;
        Debug.Log(isBlind);
    }

   


    // Update is called once per frame
    void Update()
    {
        if(stunThreshold <= 0)
        {
            SetIsStunned(true);
           
           

            //Debug.Log("Enemy is Stunned");
        }

        if (isStunned)
        {
            //Debug.Log("Stun Duration: " + stunDuration);
            PlayStunEffect();
            stunDuration -= Time.deltaTime;
            if (stunDuration <= 0f)
            {
                SetIsStunned(false);
                EndStunEffect();           
                //Debug.Log("SetIsStunned: " + isStunned);
                stunDuration = 7f;
                hasDecrease = false;
                stunThreshold = EnemyTypeSO.maxStunThreshold;

            }
        }

        if (isAggro)
        {
            //Debug.Log("Aggro-ed the Enemy");
            aggroDuration -= Time.deltaTime;
            if(aggroDuration <= 0f)
            {
                SetAggro(false);
                aggroDuration = tempAggroDuration;
            }
        }

        if (isAlerted)
        {
            //Debug.Log("Alerted by something");
            alertDuration -= Time.deltaTime;
            if(alertDuration <= 0f)
            {
                SetAlerted(false);
                alertDuration = tempAlertDuration;
            }
        }

        if(hitFeedback == true)
        {
            skinnedMeshRenderer.material = EnemyTypeSO.damageMaterial;
            
            feedbackDuration -= Time.deltaTime;

            if(feedbackDuration <= 0f)
            {
                skinnedMeshRenderer.material = EnemyTypeSO.normalMaterial;
                hitFeedback = false;
                feedbackDuration = 0.5f;
                Debug.Log("Feedback finish");
            }


        }


        if (GetCoolDown())
        {
            SkillCoolDown();
        }

        if (GetSpecialCD())
        {
            SpecialSkillCoolDown();
        }

        if (isBlind)
        {
            Debug.Log(blindDuration);
            blindDuration -= Time.deltaTime;
            if (blindDuration <= 0f)
            {
                SetBlind(false);
                Debug.Log("SetBlind: " + isBlind);
                blindDuration = 3f;
            }

        }

        //if (isBlind)
        //{
        //    return;
        //}

        //Vector3 moveDir = (targetTransform.position - transform.position).normalized;

        //float moveSpeed = 6f;
        //rigidbody.velocity = moveDir * moveSpeed;
    }

    private void SkillCoolDown()
    {

        skillCD -= Time.deltaTime;
        if (skillCD <= 0)
        {
            SetCoolDown(false);
            skillCD = tempCD;

        }
        
    }

    private void SpecialSkillCoolDown()
    {
        specialCD -= Time.deltaTime;
        if(specialCD <= 0)
        {
            SetSpecialCD(false);
            specialCD = specialTempCD;
        }
    }

    public float GetHealth()
    {
        return health;
    }

    public virtual void Attack()
    {
        Debug.Log("I am Attacking");
    }

    public Waypoints GetWaypoints()
    {
        return waypoints;
    }

    public CorpseWaypoints GetCorpseWaypoints()
    {
        return corpseWaypoints;
    }

    public ParticleSystem GetRitualVFX()
    {
        return ritualVFX;
    }

  


    private void RandomLocation()
    {

        float randX = UnityEngine.Random.Range(-5, 5);
        float randZ = UnityEngine.Random.Range(-5, 5);
        Vector3 newLocation = new Vector3(randX, transform.position.y, randZ);


        Vector3 moveDir = (newLocation - transform.position).normalized;

    }

    private void PlayStunEffect()
    {
        if(stunEffect != null)
        {
            stunEffect.gameObject.SetActive(true);
        }
    }

    private void EndStunEffect()
    {
        if(stunEffect != null)
        {
            stunEffect.gameObject.SetActive(false);
        }
    }

    #region Properties
    public void SetBlind(bool blind)
    {
        isBlind = blind;
    }

    public bool GetBlind()
    {
        return isBlind;
    }

    public EnemyTypeSO GetEnemyTypeSO()
    {
        return EnemyTypeSO;
    }

    public bool GetCollectAllItem()
    {
        return itemCollected;
    }

    public void SetCollectAllItem(bool value)
    {
        itemCollected = value;
    }

    public void SetCoolDown(bool cd)
    {
        onCoolDown = cd;
    }

    public bool GetCoolDown()
    {
        return onCoolDown;
    }

    public int GetUsagePercentage()
    {
        return usagePercentage;
    }

    public bool GetIsStunned()
    {
        return isStunned;
    }

    public void SetIsStunned(bool value)
    {
        isStunned = value;
    }

    public bool GetAggro()
    {
        return isAggro;
    }

    public void SetAggro(bool value)
    {
        isAggro = value;
    }

    public int GetProvokeCounter()
    {
        return provokeCounter;
    }

    public void SetProvokeCounter(int count)
    {
        provokeCounter = count;
    }

    public bool GetAlerted()
    {
        return isAlerted;
    }

    public void SetAlerted(bool value)
    {
        isAlerted = value;
    }

    public Vector3 GetLocationAlerted()
    {
        return locationAlerted;
    }

    public bool GetSpecialCD()
    {
        return onSpecialCoolDown;
    }

    public void SetSpecialCD(bool value)
    {
        onSpecialCoolDown = value;
    }

    public int GetSpecialUsagePercentage()
    {
        return specialUsagePercentage;
    }

    public bool GetEnrage()
    {
        return isEnrage;
    }

    public bool GetIsTutorialMode()
    {
        return isTutorialMode;
    }

    public bool GetSummonComplete()
    {
        return summonComplete;
    }
    public void SetSummonComplete(bool value)
    {
        summonComplete = value;
    }

    public Transform GetReturnLocation()
    {
        return returnLocation;
    }

    public int GetIllusionLife()
    {
        return illusionLife;
    }

    public bool GetIsSummoned()
    {
        return isSummoned;
    }

    public void SetIsSummoned(bool value)
    {
        isSummoned = value;
    }

    public void DecreaseIllusionLife()
    {
        if (!hasDecrease)
        {
            illusionLife -= 1;
            hasDecrease = true;
        }
       
    }



    #endregion

    public void SummonChanneling()
    {
        summonComplete = true;
    }

    public void ClearListeners()
    {
        BlessAbilityBehaviour.OnBlindUnit -= BlessAbilityBehaviour_OnBlindUnit;
        Gun.OnGunShot -= Gun_OnGunShot;

        Die.OnDie -= Die_OnDie;
        PistolProjectile.OnEnemyHit -= PistolProjectile_OnEnemyHit; //(object sender, PistolProjectile.OnEnemyHitArgs e) => {

        if (EnemyTypeSO.enemyTypes != EnemyType.EnemyTypes.LesserFlameWisps)
        {
            PistolProjectile.OnBossEnemyHit -= PistolProjectile.OnBossEnemyHit;
        }
        ButtonMashControl.OnButtonMashFinished += ButtonMashControl_OnButtonMashFinished;
        Fireball.OnFireballHit -= Fireball_OnFireballHit;
        Freeze.OnFreezeHit -= Freeze_OnFreezeHit;
        LightEffect.OnLightHit -= LightEffect_OnLightHit;
        Fireball.OnFireballShot -= Fireball_OnFireballShot;
        Freeze.OnFreezeShot -= Freeze_OnFreezeShot;
        CorpseObjective.OnObjectiveComplete -= CorpseObjective_OnObjectiveComplete;
        SpecialSkill.OnMangkukulamSpecialFinished -= SpecialSkill_OnMangkukulamSpecialFinished;
        GameEvents.instance.clearListeners -= ClearListeners;
    }


}

[Serializable]
public class EnemyType
{
    public enum EnemyTypes
    {
        Manananggal,
        Tiyanak,
        Mangkukulam,
        MangkukulamIllusion,
        MangkukulamIllusionFinal,
        LesserFlameWisps,
        Aswang,
        Aswang2
    };
}
