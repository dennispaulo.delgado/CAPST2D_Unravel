﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ManananggalLowerBody : MonoBehaviour
{
    public UnityEvent OnDestroyLowerBody;
    public UnityEvent OnFailed;

    public GameObject manananggalUpperBody;

    public List<Item> requiredItem;
    public bool isDead = false;
    public void InteractLowerBody(Inventory pInv)
    {
        int counter = 0;
        for(int r = 0; r<requiredItem.Count;r++)
        {
            for(int i = 0; i < pInv.GetInventoryList().Count; i++)
            {
                if(requiredItem[r].interactable.itemType == pInv.GetInventoryList()[i].interactable.itemType)
                {
                    counter++;
                    break;
                }
                else if(requiredItem[r].interactable.itemType != pInv.GetInventoryList()[i].interactable.itemType && i == pInv.GetInventoryList().Count -1)
                {
                    NotEnoughSupply();
                }
            }
        }
        if(counter == requiredItem.Count)
        {
            RemoveItem(pInv);
            SaveManager.instance.stageData.playerStopTheManananggal = true;
            isDead = true;
        }
        else
        {
            NotEnoughSupply();
        }
    }

    public void ManananggalLowerHalfCheat()
    {
        SaveManager.instance.stageData.playerStopTheManananggal = true;
        isDead = true;
        OnDestroyLowerBody.Invoke();
    }

    private void RemoveItem(Inventory pInv)
    {
        for (int r = 0; r < requiredItem.Count; r++)
        {
            for (int i = 0; i < pInv.GetInventoryList().Count; i++)
            {
                if (requiredItem[r].interactable.itemType == pInv.GetInventoryList()[i].interactable.itemType)
                {
                    pInv.RemoveItem(pInv.GetInventoryList()[i]);
                }
            }
        }
        OnDestroyLowerBody.Invoke();
    }

    private void NotEnoughSupply()
    {
        Debug.Log("No Supply");
        OnFailed.Invoke();
    }

}
