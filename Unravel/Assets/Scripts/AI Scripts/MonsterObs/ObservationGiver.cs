﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObservationGiver : MonoBehaviour
{
    public enum ObservationType
    {
        Manananggal,
        Aswang,
        Mangkukulam
    }

    public ObservationType type;
    public int obsID;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetMonsterObservation()
    {
        switch(type)
        {
            case ObservationType.Manananggal:
                AswangObsEvent.instance.OnAddManananggalObs(obsID);
                break;
            case ObservationType.Aswang:
                AswangObsEvent.instance.OnAddAswangObs(obsID);
                break;
            case ObservationType.Mangkukulam:
                AswangObsEvent.instance.OnAddMangkukulamObs(obsID);
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            GetMonsterObservation();
        }
    }
}
