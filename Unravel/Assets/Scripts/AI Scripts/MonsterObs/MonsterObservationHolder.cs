﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="MonsterObservationHolder")]
public class MonsterObservationHolder : ScriptableObject
{
    public List<MonsterObservation> monsterObservation;
}
