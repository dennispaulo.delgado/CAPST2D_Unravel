﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MangkukulamSummons : MonoBehaviour
{
    [SerializeField] private Enemy summonMinion;

    public Enemy GetSummonMinion()
    {
        return summonMinion;
    }
}
