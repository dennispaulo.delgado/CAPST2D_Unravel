﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class RegularAIAction
{
    public DialogueSet dialSet;
    public List<Transform> destinations;
    public bool isFinished = false;
    public bool waitForPlayer = false;
    public UnityEvent OnFinishDialogue;
    public UnityEvent ArriveDestination;
    //public bool isAvailable = false;
}
