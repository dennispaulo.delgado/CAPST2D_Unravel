﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public sealed class CorpseLocation
{
    private static CorpseLocation instance;

    private List<GameObject> checkpoints = new List<GameObject>();

    public List<GameObject> Checkpoints { get { return checkpoints; } }



    public static CorpseLocation Singleton
    {
        get
        {
            if (instance == null)
            {
                instance = new CorpseLocation();
                //instance.returnLocation = GameObject.FindGameObjectWithTag("Return");
                instance.Checkpoints.AddRange(GameObject.FindGameObjectsWithTag("Corpse"));
                instance.checkpoints = instance.checkpoints.OrderBy(waypoint => waypoint.name).ToList();
            }
            return instance;
        }
    }

   
}

public sealed class CorpseLocation2
{
    private static CorpseLocation2 instance2;
    private List<GameObject> checkpoints2 = new List<GameObject>();
    public List<GameObject> Checkpoints2 { get { return checkpoints2; } }

    public static CorpseLocation2 Singleton2
    {
        get
        {
            if (instance2 == null)
            {
                instance2 = new CorpseLocation2();
                //instance.returnLocation = GameObject.FindGameObjectWithTag("Return");
                instance2.Checkpoints2.AddRange(GameObject.FindGameObjectsWithTag("Corpse2"));
                instance2.checkpoints2 = instance2.checkpoints2.OrderBy(waypoint => waypoint.name).ToList();
            }
            return instance2;
        }
    }
}
