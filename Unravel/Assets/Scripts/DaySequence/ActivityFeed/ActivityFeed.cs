﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum JournalFeedType
{
    LoreUpdated,HiddenItem,NewItem,ObservationGain,MonsterDiscovered,MapUpdated
}

public class ActivityFeed : MonoBehaviour
{
    public static ActivityFeed instance;

    public GameObject feedTemplate; //reference the feed prefab
    public Transform feedContainer; //used to reference the container
    public List<GameObject> feeds;

    public float feedDuration; // duration of each feed

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }
    public void AddFeedToActivityFeed(string feed)
    {
        GameObject fe = Instantiate(feedTemplate, feedContainer);
        fe.GetComponent<Feed>().SetReferences(this, feedDuration);
        feeds.Add(fe);
        //fe.GetComponent<Text>().text = feed;
        RefreshActivityFeed();
    }
    public void AddFeedToActivityFeed(Item item)
    {
        GameObject fe = Instantiate(feedTemplate, feedContainer);
        fe.GetComponent<Feed>().SetReferences(this, feedDuration, item.interactable.sprite2D);
        feeds.Add(fe);
        RefreshActivityFeed();
    }
    public void AddFeedToActivityFeed(JournalFeedType type)
    {
        GameObject fe = Instantiate(feedTemplate, feedContainer);
        fe.GetComponent<Feed>().SetReferences(this, feedDuration, type);
        feeds.Add(fe);
        RefreshActivityFeed();
    }

    public void RemoveFeedToActivityFeed(GameObject feed)
    {
        Destroy(feed.gameObject);
        feeds.Remove(feed);
        RefreshActivityFeed();
    }
    public void RefreshActivityFeed()
    {
        float currentYPos = 0;
        for (int o = 0; o < feeds.Count; o++)
        {
            feedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(feedContainer.GetComponent<RectTransform>().sizeDelta.x,
               currentYPos);
            feeds[o].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, currentYPos * -1);
            currentYPos += feeds[o].GetComponent<RectTransform>().sizeDelta.y;
        }
    }
}
