﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Feed : MonoBehaviour
{
    public Image feedIcon;
    private float duration;
    private ActivityFeed acFeed;
    [Header("ItemUpdates")]
    public GameObject itemUpdates;
    public GameObject title;
    public GameObject text;
    [Header("JournalUpdates")]
    public GameObject journalUpdates;
    public GameObject loreUpdate;
    public GameObject mapUpdated;
    public GameObject hiddenItem;
    public GameObject newItem;
    public GameObject observation;
    public GameObject monsterDiscover;
    [Header("Extra")]
    public List<Image> main;
    public float fadeDuration;
    private float defDuration;
    public float mainAlpha;

    private void Start()
    {
        defDuration = fadeDuration;
    }
    void Update()
    {
        if (duration<=0)
        {
            acFeed.RemoveFeedToActivityFeed(this.gameObject);
        }
        else
        {
            duration -= Time.deltaTime;
        }
        if(duration<1)
        {
            foreach (Image i in main)
            {
                i.CrossFadeAlpha(0, .5f, false);
            }
        }
    }

    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        mainAlpha += Time.deltaTime / defDuration;
        foreach(Image i in main)
        {
            Color col = new Color(i.color.r, i.color.g, i.color.b, mainAlpha);
            i.color = col;
        }
        if (fadeDuration <= 0)
        {
            mainAlpha = 1;
            fadeDuration = defDuration;
        }
        else
        {
            fadeDuration -= Time.deltaTime;
            StartCoroutine("FadeIn");
        }
    }

    public void SetReferences(ActivityFeed activityF, float dur)
    {
        acFeed = activityF;
        duration = dur;
    }
    public void SetReferences(ActivityFeed activityF, float dur, Sprite itemIcon)
    {
        acFeed = activityF;
        duration = dur;
        feedIcon.sprite = itemIcon;
        main.Add(title.GetComponent<Image>());
        main.Add(text.GetComponent<Image>());
        StartFade();
        itemUpdates.SetActive(true);
        journalUpdates.SetActive(false);
    }
    public void SetReferences(ActivityFeed activityF, float dur, JournalFeedType typ)
    {
        acFeed = activityF;
        duration = dur;
        itemUpdates.SetActive(false);
        journalUpdates.SetActive(true);
        switch(typ)
        {
            case JournalFeedType.HiddenItem:
                JournalTypeHiddenItem();
                break;
            case JournalFeedType.LoreUpdated:
                JournalTypeLoreUpdated();
                break;
            case JournalFeedType.MonsterDiscovered:
                JournalTypeMonsterDiscovered();
                break;
            case JournalFeedType.NewItem:
                JournalTypeNewItem();
                break;
            case JournalFeedType.ObservationGain:
                JournalTypeObservation();
                break;
            case JournalFeedType.MapUpdated:
                JournalTypeMapUpdated();
                break;
        }
        StartFade();
    }

    private void StartFade()
    {
        foreach (Image i in main)
        {
            Color col = new Color(i.color.r, i.color.g, i.color.b, mainAlpha);
            i.color = col;
        }
        StartCoroutine("FadeIn");
    }

    private void JournalTypeObservation()
    {
        loreUpdate.SetActive(false);
        newItem.SetActive(false);
        hiddenItem.SetActive(false);
        observation.SetActive(true);
        monsterDiscover.SetActive(false);
        mapUpdated.SetActive(false);
        this.GetComponent<RectTransform>().sizeDelta =
            new Vector2(observation.GetComponent<RectTransform>().sizeDelta.x, observation.GetComponent<RectTransform>().sizeDelta.y);
        main.Add(observation.GetComponent<Image>());
    }
    private void JournalTypeMapUpdated()
    {
        loreUpdate.SetActive(false);
        newItem.SetActive(false);
        hiddenItem.SetActive(false);
        observation.SetActive(false);
        monsterDiscover.SetActive(false);
        mapUpdated.SetActive(true);
        this.GetComponent<RectTransform>().sizeDelta =
            new Vector2(observation.GetComponent<RectTransform>().sizeDelta.x, observation.GetComponent<RectTransform>().sizeDelta.y);
        main.Add(observation.GetComponent<Image>());
    }

    private void JournalTypeNewItem()
    {
        loreUpdate.SetActive(false);
        newItem.SetActive(true);
        this.GetComponent<RectTransform>().sizeDelta =
            new Vector2(newItem.GetComponent<RectTransform>().sizeDelta.x, newItem.GetComponent<RectTransform>().sizeDelta.y);
        main.Add(newItem.GetComponent<Image>());
        hiddenItem.SetActive(false);
        observation.SetActive(false);
        monsterDiscover.SetActive(false);
        mapUpdated.SetActive(false);
    }
    private void JournalTypeMonsterDiscovered()
    {
        loreUpdate.SetActive(false);
        newItem.SetActive(false);
        hiddenItem.SetActive(false);
        observation.SetActive(false);
        monsterDiscover.SetActive(true);
        mapUpdated.SetActive(false);
        this.GetComponent<RectTransform>().sizeDelta =
            new Vector2(monsterDiscover.GetComponent<RectTransform>().sizeDelta.x, monsterDiscover.GetComponent<RectTransform>().sizeDelta.y);
        main.Add(monsterDiscover.GetComponent<Image>());
    }
    private void JournalTypeLoreUpdated()
    {
        loreUpdate.SetActive(true);
        this.GetComponent<RectTransform>().sizeDelta =
            new Vector2(loreUpdate.GetComponent<RectTransform>().sizeDelta.x, loreUpdate.GetComponent<RectTransform>().sizeDelta.y);
        main.Add(loreUpdate.GetComponent<Image>());
        newItem.SetActive(false);
        hiddenItem.SetActive(false);
        observation.SetActive(false);
        monsterDiscover.SetActive(false);
        mapUpdated.SetActive(false);
    }
    private void JournalTypeHiddenItem()
    {
        loreUpdate.SetActive(false);
        newItem.SetActive(false);
        hiddenItem.SetActive(true);
        mapUpdated.SetActive(false);
        this.GetComponent<RectTransform>().sizeDelta =
            new Vector2(hiddenItem.GetComponent<RectTransform>().sizeDelta.x, hiddenItem.GetComponent<RectTransform>().sizeDelta.y);
        main.Add(hiddenItem.GetComponent<Image>());
        observation.SetActive(false);
        monsterDiscover.SetActive(false);
    }

}
