﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlaceScriptable : ScriptableObject
{
    public string placeName;

    public List<Item> possibleItems;
    public List<PlayerNotes> notes;


    public abstract void ContructActionButtons(DaySequence dS);
    /*
    {
        for (int i = 0; i < possibleItems.Count; i++)
        {
            int howMany = Random.Range(1, 5);
            possibleItems[i].amount = howMany;
            //SaveManager.instance.playerData.itemList.Add(possibleItems[i]);
            string fe = "You got " + howMany + " " + possibleItems[i].interactable.nameString;
            dS.acFeed.AddFeedToActivityFeed(fe);
        }
        
    }*/
}
