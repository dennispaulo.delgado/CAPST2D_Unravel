﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayButton : MonoBehaviour
{
    public Sprite selected;
    public Sprite unSelected;

    public void SelectThisButton()
    {
        this.gameObject.GetComponent<Image>().sprite = selected;
    }
    public void UnSelectThisButton()
    {
        this.gameObject.GetComponent<Image>().sprite = unSelected;
    }
}
