﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using TMPro;

public class DayUiInventory : MonoBehaviour
{
    private Inventory inventory;
    public Transform itemSlotContainer;
    public Transform itemSlotTemplate;
    private Transform btnTransform;
    private CollectBehaviour player;

    private bool isOpen = false;



    private void Awake()
    {
        //itemSlotContainer = transform.Find("itemSlotContainer");
        //itemSlotTemplate = itemSlotContainer.Find("itemSlotTemplate");
        //Show();

    }




    public void SetPlayer(CollectBehaviour player)
    {
        btnTransform = itemSlotTemplate;
        this.player = player;
    }


    public void SetInventory(Inventory inventory)
    {
        this.inventory = inventory;
        inventory.OnItemListChanged += Inventory_OnItemListChanged;


        //RefreshInventoryItems();
    }


    void Inventory_OnItemListChanged(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }


    public void RefreshInventoryItems()
    {
        foreach (Transform child in itemSlotContainer)
        {
            //Debug.Log("Checking Template");
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }
        float sX = 109.1f;
        float sY = -17.4f;
        float x = 0f;
        float y = 0f;
        float itemSlotCellSize = 130.2f;

        foreach (Item item in inventory.GetInventoryList())
        {
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            itemSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () =>
            {
                //Use Item
                inventory.UseItem(item);
            };

            itemSlotRectTransform.GetComponent<Button_UI>().MouseRightClickFunc = () =>
            {
                //Drop item
                Item duplicateItem = new Item { interactable = item.interactable, amount = item.amount };
                inventory.RemoveItem(item);
                ItemWorld.DropItem(player.transform.position, duplicateItem);
            };

            itemSlotRectTransform.anchoredPosition = new Vector2((x * itemSlotCellSize) + sX, (-y * itemSlotCellSize) + sY);
            Image image = itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite = item.GetSprite();

            TextMeshProUGUI uiText = itemSlotRectTransform.Find("text").GetComponent<TextMeshProUGUI>();
            if (item.amount > 1)
            {
                uiText.SetText(item.amount.ToString());

            }
            else
            {
                uiText.SetText("");
            }



            x++;
            if (x > 2)
            {
                x = 0;
                y++;
            }
        }
    }

    public bool GetIsOpen()
    {
        return isOpen;
    }

    public void SetIsOpen(bool value)
    {
        isOpen = value;
    }


    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
