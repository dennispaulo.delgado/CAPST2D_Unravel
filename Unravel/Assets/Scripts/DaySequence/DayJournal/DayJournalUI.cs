﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayJournalUI : MonoBehaviour
{
    public GameObject aswangCanvas;
    public GameObject notesCanvas;
    public GameObject objectiveCanvas;
    public GameObject helpCanvas;

    public GameObject journalCanvas;

    [Header("JournalTabs")]
    public JournalTabs aswangTab;
    public JournalTabs notesTab;
    public JournalTabs objectiveTab;
    public JournalTabs helpTab;

    public bool isOpen = false;
    void Start()
    {

        journalCanvas.SetActive(false);
    }

    public void OpenCanvasTab(int index)
    {
        switch (index)
        {
            case 0:
                OpenAswangCanvas();
                CloseNotesCanvas();
                CloseObjectiveCanvas();
                CloseHelpCanvas();
                break;
            case 1:
                OpenNotesCanvas();
                CloseAswangCanvas();
                CloseObjectiveCanvas();
                CloseHelpCanvas();
                break;
            case 2:
                OpenObjectiveCanvas();
                CloseAswangCanvas();
                CloseNotesCanvas();
                CloseHelpCanvas();
                break;
            case 3:
                CloseObjectiveCanvas();
                CloseAswangCanvas();
                CloseNotesCanvas();
                OpenHelpCanvas();
                break;
        }
    }

    public void ViewOrPutAwayJournal()
    {
        if (isOpen)
        {
            CloseAswangCanvas();
            journalCanvas.SetActive(false);
            isOpen = false;
        }
        else
        {
            journalCanvas.SetActive(true);
            isOpen = true;
            OpenJournal();
        }
    }

    public void OpenJournal()
    {
        OpenAswangCanvas();
        CloseNotesCanvas();
        CloseObjectiveCanvas();
        CloseHelpCanvas();
    }
    public void OpenAswangCanvas()
    {
        aswangCanvas.SetActive(true);
        //aswangTab.JournalTabOn();
        aswangCanvas.GetComponent<DayAswangCanvas>().ShowOrCloseAswangJournal();
    }
    public void CloseAswangCanvas()
    {
        aswangCanvas.SetActive(false);
        //aswangTab.JournalTabOff();
        aswangCanvas.GetComponent<DayAswangCanvas>().ResetNotesCanvas();
    }
    public void OpenNotesCanvas()
    {
        notesCanvas.SetActive(true);
       // notesTab.JournalTabOn();
        notesCanvas.GetComponent<DayNotesJ>().ShowOrCloseNoteJournal();
    }
    public void CloseNotesCanvas()
    {
        notesCanvas.SetActive(false);
        //notesTab.JournalTabOff();
        notesCanvas.GetComponent<DayNotesJ>().ResetNotesCanvas();
    }
    public void OpenObjectiveCanvas()
    {
        objectiveCanvas.SetActive(true);
        //objectiveTab.JournalTabOn();
        objectiveCanvas.GetComponent<ObjectiveJCanvas>().ShowOrCloseObjectiveJournal();
    }
    public void CloseObjectiveCanvas()
    {
        objectiveCanvas.SetActive(false);
        //objectiveTab.JournalTabOff();
        objectiveCanvas.GetComponent<ObjectiveJCanvas>().ResetObjectiveCanvas();
    }

    public void OpenHelpCanvas()
    {
        //helpTab.JournalTabOn();
        helpCanvas.SetActive(true);
    }

    public void CloseHelpCanvas()
    {
        //helpTab.JournalTabOff();
        helpCanvas.SetActive(false);
    }
}
