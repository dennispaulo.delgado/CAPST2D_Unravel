﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DaySequence : MonoBehaviour
{
    public GameObject actionPanel;//test


    private PlaceScriptable currentPlaceSelected;

    public ActivityFeed acFeed;

    public int actionsRemaining;

    [Header("Panels")]
    public GameObject baryoPanel;
    public GameObject aswangPanel;
    public GameObject manananggalPanel;
    public GameObject travelHousePanel;
    [Header("TownButtons")]
    public GameObject aswangLevel;
    public GameObject manananggalLevel;

    [Header("ButtonAct")]
    public DayButton baryo;
    public DayButton cemetery;
    public DayButton abandoned;
    [Header("UnityEvent")]
    public UnityEvent OnMapTutorial;
    public UnityEvent OnBothMonsterDefeated;
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 1;
        actionsRemaining = 3; 
        CheckPlayerDiscoveredLocations();
        if(SaveManager.instance.stageData.playerStopTheManananggal&& SaveManager.instance.stageData.playerStopAswang&& !SaveManager.instance.daySequenceData.talkedAboutMangkukulam)
        {
            Debug.Log("Invoke Mangkukulam");
            StartCoroutine("DelayedInvoke");
        }
        if(!SaveManager.instance.daySequenceData.hasTalkedAboutMapTutorial)
        {
            StartCoroutine("MapTutorial");
        }
        ChangeActionPanel("Baryo");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //place.TakeAction1(this);
        }
    }


    public void SelectPlace(PlaceScriptable place)
    {
        currentPlaceSelected = place;
    }
    public void CheckPlayerDiscoveredLocations()
    {
        /*
        if (SaveManager.instance.daySequenceData.talkedToGarcia)
        {
            aswangLevel.SetActive(true);
        }
        else
        {
            aswangLevel.SetActive(false);
        }*/
        if (SaveManager.instance.daySequenceData.manananggalMissionBriefing)
        {
            manananggalLevel.SetActive(true);
        }
        else
        {
            manananggalLevel.SetActive(false);
        }
        if (SaveManager.instance.daySequenceData.aswangMissionBriefing)
        {
            aswangLevel.SetActive(true);
        }
        else
        {
            aswangLevel.SetActive(false);
        }
    }

    IEnumerator DelayedInvoke()
    {
        yield return new WaitForSeconds(2.5f);
        SaveManager.instance.daySequenceData.talkedAboutMangkukulam = true;
        OnBothMonsterDefeated.Invoke();
    }
    IEnumerator MapTutorial()
    {
        yield return new WaitForSeconds(2.5f);
        SaveManager.instance.daySequenceData.hasTalkedAboutMapTutorial = true;
        OnMapTutorial.Invoke();
    }

    public void ChangeActionPanel(string n)
    {
        switch(n)
        {
            case "Baryo":
                OpenBaryo();
                CloseAswang();
                CloseManananggal();
                break;
            case "Aswang":
                CloseBaryo();
                OpenAswang();
                CloseManananggal();
                break;
            case "Manananggal":
                CloseBaryo();
                CloseAswang();
                OpenManananggal();
                break;
            case "Travel":
                baryoPanel.SetActive(false);
                aswangPanel.SetActive(false);
                manananggalPanel.SetActive(false);
                break;
        }
    }
    public void OpenBaryo()
    {
        baryoPanel.SetActive(true);
        baryoPanel.GetComponent<BaryoPlace>().OpenBaryoPlaceActions();
        baryo.SelectThisButton();
    }
    public void CloseBaryo()
    {
        baryoPanel.GetComponent<BaryoPlace>().OpenBaryoPlaceActions();
        baryoPanel.SetActive(false);
        baryo.UnSelectThisButton();
    }
    public void OpenAswang()
    {
        aswangPanel.SetActive(true);
        aswangPanel.GetComponent<AswangPlace>().OpenAswangPlaceActions();
        cemetery.SelectThisButton();
    }
    public void CloseAswang()
    {
        aswangPanel.GetComponent<AswangPlace>().CloseAswangPlaceActions();
        aswangPanel.SetActive(false);
        cemetery.UnSelectThisButton();
    }
    public void OpenManananggal()
    {
        manananggalPanel.SetActive(true);
        manananggalPanel.GetComponent<ManananggalPlace>().OpenManangPlaceActions();
        abandoned.SelectThisButton();
    }
    public void CloseManananggal()
    {
        manananggalPanel.GetComponent<ManananggalPlace>().CloseManangPlaceActions();
        manananggalPanel.SetActive(false);
        abandoned.UnSelectThisButton();
    }

    public void CursorChecker()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

}



[System.Serializable]
public class DaySequenceAction
{
    public UnityEvent OnStartDialogue;
    public DialogueSet dlSet;
    public ObjectiveSlot objSlot;
    public ObjectiveSlot altObjSlot;
    public List<PlayerNotes> notesToGive;
}
