﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaryoPlace : MonoBehaviour
{
    public DaySequenceAction firstTalkToChief;
    public DaySequenceAction cemeteryTest;


    public DaySequenceAction missionBriefingManananggal;
    public DaySequenceAction missionBriefingAswang;


    [Header("Others")]
    public DaySequence daySeq;

    [Header("Actions")]
    public GameObject container;
    public GameObject level;
    public GameObject manananggalBrief;
    public GameObject aswangBrief;
    [Header("CheckMarks")]
    public GameObject briefManang;
    public GameObject briefAswang;
    public GameObject lvlCheck;
    void Start()
    {
    }

    
    void Update()
    {
    }
    private void CheckAvailableButtons()
    {
        if (SaveManager.instance.daySequenceData.manananggalMissionBriefing)
        {
            briefManang.SetActive(true);
        }
        else
        {
            briefManang.SetActive(false);
        }
        if (SaveManager.instance.stageData.playerStopMangkukulam)
        {
            lvlCheck.SetActive(true);
        }
        else
        {
            lvlCheck.SetActive(false);
        }
        if (SaveManager.instance.daySequenceData.aswangMissionBriefing)
        {
            briefAswang.SetActive(true);
        }
        else
        {
            briefAswang.SetActive(false);
        }
        if(SaveManager.instance.stageData.playerStopTheManananggal)
        {
            aswangBrief.SetActive(true);
        }
        else
        {
            aswangBrief.SetActive(false);
        }
        if (SaveManager.instance.stageData.hasTheCompleteAmulet)
        {
            level.SetActive(true);
        }
        else
        {
            level.SetActive(false);
        }
    }

    public void OpenBaryoPlaceActions()
    {
        container.SetActive(true);
        CheckAvailableButtons();
    }

    public void CloseBaryoPlaceActions()
    {
        container.SetActive(false);
    }

    public void MissionBriefingMananananggal()
    {
        if (!SaveManager.instance.daySequenceData.manananggalMissionBriefing)
        {
            SaveManager.instance.daySequenceData.manananggalMissionBriefing = true;
            ActivateDialogue(missionBriefingManananggal.dlSet, missionBriefingManananggal.OnStartDialogue);
            daySeq.CheckPlayerDiscoveredLocations();
            CheckAvailableButtons();
            if (!CheckIfHasAlreadyObjective(missionBriefingManananggal.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(missionBriefingManananggal.objSlot);
            }
        }
        ActivateDialogue(missionBriefingManananggal.dlSet, missionBriefingAswang.OnStartDialogue);
    }
    public void MissionBriefingAswang()
    {
        if (!SaveManager.instance.daySequenceData.aswangMissionBriefing)
        {
            SaveManager.instance.daySequenceData.aswangMissionBriefing = true;
            ActivateDialogue(missionBriefingAswang.dlSet, missionBriefingAswang.OnStartDialogue);
            daySeq.CheckPlayerDiscoveredLocations();
            CheckAvailableButtons();
            if (!CheckIfHasAlreadyObjective(missionBriefingAswang.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(missionBriefingAswang.objSlot);
            }
        }
        ActivateDialogue(missionBriefingAswang.dlSet, missionBriefingAswang.OnStartDialogue);
    }

    public void FirstTalkToTheChief()
    {
        if (!SaveManager.instance.daySequenceData.firstTalkToTheChief)
        {
            SaveManager.instance.daySequenceData.firstTalkToTheChief = true;
            ActivateDialogue(firstTalkToChief.dlSet, firstTalkToChief.OnStartDialogue);
            daySeq.CheckPlayerDiscoveredLocations();
        }
    }
    public void CemeteryTest()
    {
        if (!SaveManager.instance.daySequenceData.playerFinishManananggalLvl)
        {
            SaveManager.instance.daySequenceData.playerFinishManananggalLvl = true;
            ActivateDialogue(cemeteryTest.dlSet, cemeteryTest.OnStartDialogue);
            daySeq.CheckPlayerDiscoveredLocations();
        }
    }
    public void TalkToGarciaFam()
    {
        if(!SaveManager.instance.daySequenceData.talkedToGarcia)
        {
            //ActivateDialogue(GarFamDial);
            SaveManager.instance.daySequenceData.talkedToGarcia = true;
            //ObjectiveHandler.instance.curObjectives.Add();
            //aswangLevel.SetActive(true);
            //ObjectiveHandler.instance.curObjectives.Add(new Objective(talkToGarcia));
        }
        //ActivityFeed.instance.AddFeedToActivityFeed("Talked To Garcia Family");
    }
    public void TalkToMendozaFam()
    {
        if (!SaveManager.instance.daySequenceData.talkedToMendoza)
        {
            //ActivateDialogue(MenFamDial);
            SaveManager.instance.daySequenceData.talkedToMendoza = true;
            //manananggalLevel.SetActive(true);
            //ObjectiveHandler.instance.curObjectives.Add(new Objective(talkToMendoza));
        }
        //ActivityFeed.instance.AddFeedToActivityFeed("Talked To Mendoza Family");
    }
    public void CheckTravHouseGuide()
    {
        if (!SaveManager.instance.daySequenceData.checkedTravelHouse)
        {
            SaveManager.instance.daySequenceData.checkedTravelHouse = true;
        }
        //ActivityFeed.instance.AddFeedToActivityFeed("Checked Travel House Guide");
    }
    private bool CheckIfHasAlreadyObjective(ObjectiveSlot cSlot)
    {
        for (int i = 0; i < ObjectiveHandler.instance.curObjectives.Count; i++)
        {
            if (cSlot.objID == ObjectiveHandler.instance.curObjectives[i].objID)
            {
                return true;
            }
        }
        for (int i = 0; i < ObjectiveHandler.instance.comObjectives.Count; i++)
        {
            if (cSlot.objID == ObjectiveHandler.instance.comObjectives[i].objID)
            {
                return true;
            }
        }
        return false;
    }
    public void ActivateDialogue(DialogueSet dialSetTL, UnityEvent dialEvent)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSetTL);
        dialEvent.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }

}
