﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ManananggalPlace : MonoBehaviour
{
    public Item mix;
    [Header("DSActions")]
    public DaySequenceAction briefingAction;
    public DaySequenceAction talkToMendozaAction;
    public DaySequenceAction checkTravelGuideAction;
    public DaySequenceAction goToLevelAction;

    [Header("Actions")]
    public GameObject container;
    public GameObject briefing;
    public GameObject talkToMendoza;
    public GameObject checkTravelGuide;
    public GameObject goToLevel;
    [Header("Check")]
    public GameObject mendozaCheck;
    public GameObject travelCheck;
    public GameObject lvlCheck;
    void Start()
    {
        CheckAvailableButtons();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OpenManangPlaceActions()
    {
        container.SetActive(true);
        CheckAvailableButtons();

    }

    public void CloseManangPlaceActions()
    {
        container.SetActive(false);
    }

    private void CheckAvailableButtons()
    {
        if (SaveManager.instance.daySequenceData.talkedToMendoza)
        {
            checkTravelGuide.SetActive(true);
            mendozaCheck.SetActive(true);
        }
        else
        {
            mendozaCheck.SetActive(false);
            checkTravelGuide.SetActive(false);
        }
        
        if (SaveManager.instance.daySequenceData.checkedTravelHouse)
        {
            travelCheck.SetActive(true);
        }
        else
        {
            travelCheck.SetActive(false);
        }

        if (SaveManager.instance.stageData.playerStopTheManananggal)
        {
            goToLevel.SetActive(false);
        }
        else
        {
            lvlCheck.SetActive(false);
            goToLevel.SetActive(true);
        }


    }

    public void CheckTravelGuideHouse()
    {
        if(SaveManager.instance.stageData.playerHasTravelGuideKey)
        {
            Debug.Log("Has Key");
            ActivateDialogue(checkTravelGuideAction.dlSet, checkTravelGuideAction.OnStartDialogue);
            ObjectiveEvent.instance.OnQuestArrivedS(2224);
            ObjectiveEvent.instance.OnQuestArrivedS(2223);
            PlayersData.instance.inventory.AddItem(mix);
            SaveManager.instance.daySequenceData.checkedTravelHouse = true;
            CheckAvailableButtons();
            if (!CheckIfHasAlreadyObjective(checkTravelGuideAction.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(checkTravelGuideAction.objSlot);
                for (int n = 0; n <  checkTravelGuideAction.notesToGive.Count;n++)
                {
                    PlayersData.instance.AddPlayerNotes(checkTravelGuideAction.notesToGive[n]);
                }
            }
        }
        else
        {
            Debug.Log("Has no Key");
            ObjectiveEvent.instance.OnQuestArrivedS(2223);

            if (!CheckIfHasAlreadyObjective(checkTravelGuideAction.altObjSlot))
            {
                ObjectiveHandler.instance.AddObjective(checkTravelGuideAction.altObjSlot);
            }
        }
    }
    public void MissionBriefing()
    {
        if (!SaveManager.instance.daySequenceData.manananggalMissionBriefing)
        {
            SaveManager.instance.daySequenceData.manananggalMissionBriefing = true;
            ActivateDialogue(briefingAction.dlSet, briefingAction.OnStartDialogue);
            CheckAvailableButtons();
            if (!CheckIfHasAlreadyObjective(briefingAction.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(briefingAction.objSlot);
            }
        }
        ActivateDialogue(briefingAction.dlSet, briefingAction.OnStartDialogue);
    }

    public void TalkToMendoza()
    {
        if (!SaveManager.instance.daySequenceData.talkedToMendoza)
        {
            ObjectiveEvent.instance.OnQuestArrivedS(2224);
            SaveManager.instance.daySequenceData.talkedToMendoza = true;
            ActivateDialogue(talkToMendozaAction.dlSet, talkToMendozaAction.OnStartDialogue);
            CheckAvailableButtons();
            if (!CheckIfHasAlreadyObjective(talkToMendozaAction.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(talkToMendozaAction.objSlot);
            }
        }
        ActivateDialogue(talkToMendozaAction.dlSet, talkToMendozaAction.OnStartDialogue);
    }

    private bool CheckIfHasAlreadyObjective(ObjectiveSlot cSlot)
    {
        for(int i = 0;i<ObjectiveHandler.instance.curObjectives.Count;i++)
        {
            if(cSlot.objID == ObjectiveHandler.instance.curObjectives[i].objID)
            {
                return true;
            }
        }
        for (int i = 0; i < ObjectiveHandler.instance.comObjectives.Count; i++)
        {
            if (cSlot.objID == ObjectiveHandler.instance.comObjectives[i].objID)
            {
                return true;
            }
        }
        return false;
    }
   
    public void ActivateDialogue(DialogueSet dialSetTL, UnityEvent dialEvent)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSetTL);
        dialEvent.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }
    /// <summary>
    /// Junked
    /// </summary> 
    
    public void ExploreTheAbandonedBuildings()
    {
        //playDay.ReturnInventory().AddItem(ash);
        //ActivityFeed.instance.AddFeedToActivityFeed("Explored the Buildings and Found an Ash");
    }
public void InvestigateBloodTrails()
    {
        
    }
    public void InvestigateToolShed()
    {
        /*
        if(!SaveManager.instance.stageData.hasLadder)
        {
            SaveManager.instance.stageData.hasLadder = true;
        }*/
        //ActivityFeed.instance.AddFeedToActivityFeed("Acquired a Ladder");
    }

    public void GoToManananggalLevel()
    {
        this.gameObject.GetComponent<GateScene>().ChangeScene();
    }
   
}
