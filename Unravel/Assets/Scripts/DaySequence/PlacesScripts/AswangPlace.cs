﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AswangPlace : MonoBehaviour
{
    public Item tears;
    [Header("Actions")]
    public DaySequenceAction briefingAction;
    public DaySequenceAction talkToGarciaAction;
    public DaySequenceAction checkJuanHouseAction;
    public DaySequenceAction goToLevelAction;
    public DaySequenceAction talkToTheDuwende;

    [Header("Others")]
    public GameObject container;
    public GameObject brief;
    public GameObject garcia;
    public GameObject juan;
    public GameObject level;
    public GameObject duwende;
    [Header("Check")]
    public GameObject garciaCheck;
    public GameObject juanCheck;
    public GameObject lvlCheck;
    public GameObject duwendeCheck;

    void Start()
    {
        CheckAvailableButtons();
    }

    void Update()
    {
        
    }

    public void OpenAswangPlaceActions()
    {
        container.SetActive(true);
        CheckAvailableButtons();
    }

    public void CloseAswangPlaceActions()
    {
        container.SetActive(false);
    }
    private void CheckAvailableButtons()
    {
        
        if (SaveManager.instance.daySequenceData.talkedToGarcia)
        {
            garciaCheck.SetActive(true);
            juan.SetActive(true);
        }
        else
        {
            garciaCheck.SetActive(false);
            juan.SetActive(false);
        }

        if (SaveManager.instance.daySequenceData.checkedJuansHouse)
        {
            juanCheck.SetActive(true);
        }
        else
        {
            juanCheck.SetActive(false);
        }

        if (SaveManager.instance.stageData.playerStopAswang)
        {
            level.SetActive(false);
        }
        else
        {
            level.SetActive(true);
            lvlCheck.SetActive(false);
        }

        if (SaveManager.instance.daySequenceData.hasTalkedToDuwende)
        {
            duwendeCheck.SetActive(true);
        }
        else
        {
            duwendeCheck.SetActive(false);
        }
        if (SaveManager.instance.daySequenceData.talkedAboutMangkukulam)
        {
            duwende.SetActive(true);
        }
        else
        {
            duwende.SetActive(false);
        }
    }
    public void MissionBriefing()
    {
        if (!SaveManager.instance.daySequenceData.aswangMissionBriefing)
        {
            SaveManager.instance.daySequenceData.aswangMissionBriefing = true;
            ActivateDialogue(briefingAction.dlSet, briefingAction.OnStartDialogue);
            CheckAvailableButtons(); 
            if (!CheckIfHasAlreadyObjective(briefingAction.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(briefingAction.objSlot);
            }
        }
        ActivateDialogue(briefingAction.dlSet, briefingAction.OnStartDialogue);
    }
    public void MeetTheDuwendes()
    {
        if (!SaveManager.instance.daySequenceData.hasTalkedToDuwende)
        {
            SaveManager.instance.daySequenceData.hasTalkedToDuwende = true;
            SaveManager.instance.stageData.hasTheCompleteAmulet = true;
            ActivateDialogue(talkToTheDuwende.dlSet, talkToTheDuwende.OnStartDialogue);
            CheckAvailableButtons();
            CheckAvailableButtons();
            ObjectiveEvent.instance.OnQuestArrivedS(4442);
            if (!CheckIfHasAlreadyObjective(talkToTheDuwende.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(talkToTheDuwende.objSlot);
            }
        }
        ActivateDialogue(talkToTheDuwende.dlSet, talkToTheDuwende.OnStartDialogue);
    } 

    public void GoToJuansHouse()
    {
        if (SaveManager.instance.stageData.playerHasJuanHouseKey)
        {
            PlayersData.instance.inventory.AddItem(tears);
            SaveManager.instance.stageData.hasTheIncompleteAmulet = true;
            ActivateDialogue(checkJuanHouseAction.dlSet, checkJuanHouseAction.OnStartDialogue);
            ObjectiveEvent.instance.OnQuestArrivedS(3331);
            ObjectiveEvent.instance.OnQuestArrivedS(3333);
            ObjectiveEvent.instance.OnQuestArrivedS(3335);
            ObjectiveEvent.instance.OnQuestArrivedS(3337);
            SaveManager.instance.daySequenceData.checkedJuansHouse = true;
            CheckAvailableButtons();
            if (!CheckIfHasAlreadyObjective(checkJuanHouseAction.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(checkJuanHouseAction.objSlot); 
                for (int n = 0; n < checkJuanHouseAction.notesToGive.Count; n++)
                {
                    PlayersData.instance.AddPlayerNotes(checkJuanHouseAction.notesToGive[n]);
                }
            }
            CheckAvailableButtons();
        }
        else
        {
            ObjectiveEvent.instance.OnQuestArrivedS(3337);
            if (!CheckIfHasAlreadyObjective(checkJuanHouseAction.altObjSlot))
            {
                ObjectiveHandler.instance.AddObjective(checkJuanHouseAction.altObjSlot);
            }
        }
        //ActivityFeed.instance.AddFeedToActivityFeed("Scouted The Area and Found an Observation");
    }
    public void TalkToGarciaFamily()
    {
        if(!SaveManager.instance.daySequenceData.talkedToGarcia)
        {
            ObjectiveEvent.instance.OnQuestArrivedS(3331);
            SaveManager.instance.daySequenceData.talkedToGarcia = true;
            ActivateDialogue(talkToGarciaAction.dlSet, talkToGarciaAction.OnStartDialogue);
            CheckAvailableButtons();
            if(!CheckIfHasAlreadyObjective(talkToGarciaAction.objSlot))
            {
                ObjectiveHandler.instance.AddObjective(talkToGarciaAction.objSlot);
            }
        }
        ActivateDialogue(talkToGarciaAction.dlSet, talkToGarciaAction.OnStartDialogue);
        //ActivityFeed.instance.AddFeedToActivityFeed("Scouted The Area and Found an Observation");
    }
    
    private bool CheckIfHasAlreadyObjective(ObjectiveSlot cSlot)
    {
        for (int i = 0; i < ObjectiveHandler.instance.curObjectives.Count; i++)
        {
            if (cSlot.objID == ObjectiveHandler.instance.curObjectives[i].objID)
            {
                return true;
            }
        }
        for (int i = 0; i < ObjectiveHandler.instance.comObjectives.Count; i++)
        {
            if (cSlot.objID == ObjectiveHandler.instance.comObjectives[i].objID)
            {
                return true;
            }
        }
        return false;
    }

    public void ActivateDialogue(DialogueSet dialSetTL, UnityEvent dialEvent)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSetTL);
        dialEvent.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }
    /// <summary>
    /// Junked
    /// </summary>
    public void SealTheGraves()
    {
        /*
        if (!SaveManager.instance.stageData.blessedTheGraves)
        {
            SaveManager.instance.stageData.blessedTheGraves = true;
        }*/
        //ActivityFeed.instance.AddFeedToActivityFeed("Blessed the Cemeteries Graves");
    }
    public void GoToAswangLevel()
    {
        this.gameObject.GetComponent<GateScene>().ChangeScene();
    }
}
