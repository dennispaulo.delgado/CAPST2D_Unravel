﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrototypeCutscene : MonoBehaviour
{
    public CinemachineFreeLook cinema;
    public Transform enemy;
    public float lookDuration;
    public Transform objective;
    public float objDuration;
    public Transform player;

    public bool isActive;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive)
        {
            if(lookDuration>0)
            {
                cinema.m_LookAt = this.enemy.transform;
                cinema.m_Follow = this.enemy.transform;
                lookDuration -= Time.deltaTime;
            }
            else
            {
                if(objDuration>0)
                {
                    cinema.m_LookAt = this.objective.transform;
                    cinema.m_Follow = this.objective.transform;
                    objDuration -= Time.deltaTime;
                    cinema.m_YAxis.Value = 0.5f;
                    cinema.m_XAxis.Value = -6.06f;
                    cinema.m_YAxis.m_MaxSpeed = 0f;
                    cinema.m_XAxis.m_MaxSpeed = 0f;
                }
                else
                {
                    cinema.m_LookAt = this.player.transform;
                    cinema.m_Follow = this.player.transform;
                    cinema.m_YAxis.m_MaxSpeed = 2f;
                    cinema.m_XAxis.m_MaxSpeed = 300f;
                    player.GetComponent<ThirdPersonMovement>().canMove = true;
                    Destroy(this.gameObject);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = true;
            player = other.transform;
            player.GetComponent<ThirdPersonMovement>().canMove = false;
        }
    }
}
