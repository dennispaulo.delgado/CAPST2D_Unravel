﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetScript : MonoBehaviour
{
    public Transform target;

    public bool ignoreYPos;
    public bool followTargetRotY= false;
    public float worldYPosition;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    private void LateUpdate()
    {
        if (ignoreYPos)
        {
            Vector3 desiredPosition = new Vector3(target.position.x, worldYPosition, target.position.z);
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
        }
        else
        {
            Vector3 desiredPosition = this.target.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
        }
        if(followTargetRotY)
        {
            this.transform.eulerAngles = new Vector3(90, target.eulerAngles.y, 0);
        }
    }
}
