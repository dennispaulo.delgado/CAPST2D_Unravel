﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenInfi : MonoBehaviour
{
    public GameObject bookCanvas;
    public bool isActive;
    void Start()
    {
        isActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyUp(KeyCode.J))
        {
            if(bookCanvas.active&& isActive)
            {
                bookCanvas.SetActive(false);
            }
            else if(!bookCanvas.active && isActive)
            {
                bookCanvas.SetActive(true);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            bookCanvas.SetActive(true);
            isActive = true;
        }
    }
}
