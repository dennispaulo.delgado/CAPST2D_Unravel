﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ManualEventScript : MonoBehaviour
{

    public UnityEvent OnPlayerEnter;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player"&&this.GetComponent<ItemWorld>() == null)
        {
            OnPlayerEnter.Invoke();
            Destroy(this.gameObject);
        }
    }
}
