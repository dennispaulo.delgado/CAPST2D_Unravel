﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstateTrigger : MonoBehaviour
{
    public bool triggered = false;

    public void PlayerDiscoverAswangWeakness()
    {
        AswangObsEvent.instance.OnAddAswangObs(4);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player"&&!triggered)
        {
            AswangObsEvent.instance.OnAddAswangObs(2);
            triggered = true;
        }
    }
}
