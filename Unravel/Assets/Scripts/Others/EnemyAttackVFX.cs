﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class EnemyAttackVFX : MonoBehaviour
{
    public GameObject enemAttackVFX1;
    public GameObject enemAttackVFX2;
    public GameObject enemAttackVFX3;

    public GameObject gm;

    private void Start()
    {
        Vector3 v = this.transform.position - gm.transform.position;
        
    }
    public void EnemyAttackVFX1(GameObject enemRef, float timer)
    {

    }
    public void EnemyAttackVFX1(Vector3 direction, float timer)
    {

    }
    public void EnemyAttackVFX1(float timer)
    {

    }
    public void EnemyAttackVFX1(GameObject enemRef,Transform target,float timer)
    {
        Vector3 v = enemRef.transform.position - target.position;
        GameObject vfxG = Instantiate(enemAttackVFX1, enemRef.transform.position, transform.rotation);
        vfxG.GetComponent<XSectoDestroy>().secondsToDestroy = timer;
        VisualEffect vfx = vfxG.GetComponent<VisualEffect>();
        vfx.SetVector3("Direction", -v);
    }
}
