﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NoteItem : MonoBehaviour
{
    public PlayerNotes noteToGive;

    public UnityEvent OnPlayerGet;
    public void AddNoteToPlayer(GameObject player)
    {
        this.GetComponent<AudioSource>().Play();
        this.GetComponent<MeshRenderer>().enabled = false;
        this.GetComponent<BoxCollider>().enabled = false;
        PlayersData.instance.AddPlayerNotes(noteToGive);
        OnPlayerGet.Invoke();
        if(this.GetComponent<ObjectDataScript>()!=null)
        {
            this.GetComponent<ObjectDataScript>().ObjectCollected();
        }
        Destroy(this.gameObject,2f);
    }

    public void AddNoteCanvasMode()
    {
        PlayersData.instance.AddPlayerNotes(noteToGive);
    }

    public void CallEventManangWeakness()
    {
        AswangObsEvent.instance.OnAddManananggalObs(3);
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.GetComponent<PlayersData>().AddPlayerNotes(noteToGive);
            Destroy(this.gameObject);
        }
    }*/
}
