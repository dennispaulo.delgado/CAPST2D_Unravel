﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalTutorial : MonoBehaviour
{
    private void OnDestroy()
    {
        Debug.Log("Ondestroy Called");
        GameObject jIns = GameObject.FindGameObjectWithTag("Instruction3");
        if (jIns != null)
        {
            jIns.GetComponent<InstructionScript>().ActivateInstruction();
        }
    }
}
