﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.Events;


public class DialogueSystem : MonoBehaviour
{

    public static DialogueSystem instance = null;

    public UnityEvent OnDialogueComplete;

    public DialogueSet activeDialogue;
    [SerializeField]
    private string activeDialogueText;

    public bool skipTextInstances = false;
    public float textSpeed = 0.2f;
    public bool isDialCompleted = false;

    public bool instanceTalking;

    public Text displayText;
    public Text characterTextName;
    public Image characterPortrait;
    public Image otherCharPortrait;

    public int index;
    public int dialogueTotal;

    [Header("Dial")]
    public GameObject all;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);
    }
    void Start()
    {
        //initial erase
        EraseDialogue();
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Debug.Log("active dialogue ID:" + index);
            StopAllCoroutines();
            if (instanceTalking)
            {
                PostAllDialogue();
            }
            else
            {
                //Debug.Log("cont Dial");
                ContCoverstation();
            }

            if (index > dialogueTotal&&!instanceTalking)
            {
                //Debug.Log("Setting False Dial");
                all.SetActive(false);
                this.gameObject.SetActive(false);
                OnDialogueComplete.Invoke();
            }
        }

        
    }

    public void PostAllDialogue()
    {
        displayText.text = activeDialogue.dialogueSet[index].dialogue;
        instanceTalking = false;
        index++;
    }

    public void ActivateDialogue(DialogueSet dialogueSetID)
    {
        all.SetActive(true);
        isDialCompleted = false;
        index = 0;
        activeDialogue = dialogueSetID;
        dialogueTotal = activeDialogue.TotalDialogue();
        EraseDialogue();
        PrintText();
    }


    private void PrintText()
    {
        if (activeDialogue.dialogueSet[index].charPortrait != null)
        {
            characterPortrait.sprite = activeDialogue.dialogueSet[index].charPortrait;
            characterPortrait.color = new Color(characterPortrait.color.r, characterPortrait.color.g, characterPortrait.color.b, activeDialogue.dialogueSet[index].charAlpha);
        }
        else
        {
            characterPortrait.color = new Color(characterPortrait.color.r, characterPortrait.color.g, characterPortrait.color.b, 0);
        }
        if (activeDialogue.dialogueSet[index].otherPortrait != null)
        {
            otherCharPortrait.sprite = activeDialogue.dialogueSet[index].otherPortrait;
            otherCharPortrait.color = new Color(otherCharPortrait.color.r, otherCharPortrait.color.g, otherCharPortrait.color.b, activeDialogue.dialogueSet[index].otherAlpha);
        }
        else
        {
            otherCharPortrait.color = new Color(otherCharPortrait.color.r, otherCharPortrait.color.g, otherCharPortrait.color.b, 0);
        }
        characterTextName.text = activeDialogue.dialogueSet[index].sourceName;
        this.activeDialogueText = activeDialogue.dialogueSet[index].dialogue;

        if (skipTextInstances == true)
        {
            displayText.text = this.activeDialogueText;
        }
        else
        {
            instanceTalking = true;
            StartCoroutine(TypeWriting());
        }
    }
    private void EraseDialogue()
    {
        displayText.text = "";
        characterTextName.text = "";
    }

    private void ContCoverstation()
    {
        EraseDialogue();

        if (index < dialogueTotal)
        {
            PrintText();
        }
        else
        {
            index++;
        }

    }

    private void FinishConversation()
    {
        displayText.text = activeDialogue.dialogueSet[index].dialogue;
    }
    IEnumerator TypeWriting()
    {
        int cCur = 0;
        foreach (char letter in this.activeDialogueText.ToCharArray())
        {
            displayText.text += letter;
            cCur++;
            yield return new WaitForSecondsRealtime(textSpeed);
            if (cCur>= this.activeDialogueText.ToCharArray().Length)
            {
                index++;
                
                instanceTalking = false;
            }
        }
        if (index >= dialogueTotal)
        {

            isDialCompleted = true;
        }
    }

    public bool OnDialogueCompleted()
    {

        return isDialCompleted;
    }
}
