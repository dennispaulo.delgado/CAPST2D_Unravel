﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueGiver : MonoBehaviour
{
    public DialogueSet dialSet;
    public UnityEvent OnStartDialogue;
    public UnityEvent OnFinishDialogue;

    public void SignalToActivate()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSet);
        OnStartDialogue.Invoke();
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
        GameManager.instance.gameIsPaused = true;
        Time.timeScale = 0f;
    }
    public void OnDialogueComplete()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
        GameManager.instance.gameIsPaused = false;
        Time.timeScale = 1f;
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SignalToActivate();
        }
    }

}
