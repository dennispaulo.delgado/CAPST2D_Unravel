﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTeleporter : MonoBehaviour
{
    public Transform teleportHere;
    private Quaternion originalRotation;
    void Start()
    {

        originalRotation = transform.rotation;
        
    }

    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DeathPit")
        {

            this.transform.position = teleportHere.position;
            transform.rotation = originalRotation;
            
        }
    }
}
