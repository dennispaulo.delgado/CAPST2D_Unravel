﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsForceObj : MonoBehaviour
{
    public bool isActive = false;
    public bool beingPointed = false;
    public bool canPlace;
    public bool collisionIsClear;
    public bool topIsClear;
    public bool forcePlace;

    private Collider[] col;
    private Collider[] topCol;
    public LayerMask collisionMaskLayer;
    public LayerMask topMaskLayer;

    private GameObject thisObject;
    private Vector3 objectSize;

    public Material originalPlaceShader;
    public Material cantPlaceShader;
    public Material highlightShader;
    void Start()
    {
        originalPlaceShader = this.GetComponent<MeshRenderer>().material;
        thisObject = this.gameObject;
        objectSize = new Vector3(this.GetComponent<BoxCollider>().size.x * this.transform.localScale.x,
               this.GetComponent<BoxCollider>().size.y * this.transform.localScale.y,
               this.GetComponent<BoxCollider>().size.z * this.transform.localScale.z);
    }

    
    void Update()
    {
        if(isActive)
        {
            if(collisionIsClear&&topIsClear)
            {
                canPlace = true;
            }
            else
            {
                canPlace = false;
            }
        }
    }

    private void FixedUpdate()
    {
        if(isActive)
        {
            collisionIsClear = CollisionObjectIsClear();
            if(collisionIsClear||forcePlace)
            {
                thisObject.GetComponent<MeshRenderer>().material = highlightShader;
            }
            else
            {
                thisObject.GetComponent<MeshRenderer>().material = cantPlaceShader;
            }
        }
        if(beingPointed&&!isActive)
        {
            topIsClear = TopOfObjectIsClear();
            if(topIsClear)
            {
                thisObject.GetComponent<MeshRenderer>().material = highlightShader;
            }
            else
            {
                thisObject.GetComponent<MeshRenderer>().material = cantPlaceShader;
            }
        }
    }
    public void BeginChain()
    {
        this.GetComponent<BoxCollider>().enabled = false;
        this.GetComponent<Rigidbody>().useGravity = false;
        this.GetComponent<Rigidbody>().freezeRotation = true;
    }
    public void EndChain()
    {
        this.GetComponent<BoxCollider>().enabled = true;
        this.GetComponent<Rigidbody>().useGravity = true;
        this.GetComponent<Rigidbody>().freezeRotation = false;
    }
    public void ChangeMatOriginal()
    {
        thisObject.GetComponent<MeshRenderer>().material = originalPlaceShader;
    }
    public bool CollisionObjectIsClear()
    {
        col = Physics.OverlapBox(this.transform.position + new Vector3(0,this.GetComponent<BoxCollider>().center.z * this.transform.localScale.z, 
            (this.GetComponent<BoxCollider>().center.y * this.transform.localScale.y) * -1)
            , objectSize / 2, this.transform.rotation, collisionMaskLayer);
        if (col.Length <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool TopOfObjectIsClear()
    {
        topCol = Physics.OverlapBox(this.transform.position + new Vector3(0, objectSize.z + (this.GetComponent<BoxCollider>().center.z * this.transform.localScale.z), (this.GetComponent<BoxCollider>().center.y * this.transform.localScale.y) * -1)
            , objectSize / 2, this.transform.rotation, topMaskLayer);
        if (topCol.Length <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
