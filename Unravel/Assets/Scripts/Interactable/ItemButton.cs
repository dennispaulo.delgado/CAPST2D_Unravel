﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemButton : MonoBehaviour
{
    private Item itm;
    private UI_Inventory uiInv;

    public void OnButtonEnter()
    {
        uiInv.itemDescription.GetComponent<TextMeshProUGUI>().SetText(itm.interactable.description);
    }
    public void OnButtonExit()
    {
        uiInv.itemDescription.GetComponent<TextMeshProUGUI>().SetText("");
    }
    public void SetReference(Item item, UI_Inventory ui)
    {
        itm = item;
        uiInv = ui;
    }
    public Item ReturnItem()
    {
        return itm;
    }
}
