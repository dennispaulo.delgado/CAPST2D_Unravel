﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour,IPickUp
{
    private Transform playerTransform;

    private void Awake()
    {
        playerTransform = FindObjectOfType<ThirdPersonMovement>().transform;
    }

    public virtual void DropObject()
    {
        transform.SetParent(null);
        transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
        transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        Debug.Log("Drop item");
    }

    public virtual void PickUpObject()
    {
        transform.SetParent(playerTransform);
        transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
        transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, playerTransform.position.z + 1.3f);
        Debug.Log("Pickup item");
    }

    public virtual void ThrowObject()
    {
        if(transform.parent != null)
        {
            transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
            transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            transform.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 1000f));
            Debug.Log("Throw");
        }



    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
