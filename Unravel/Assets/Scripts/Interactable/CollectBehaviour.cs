﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CollectBehaviour : MonoBehaviour
{
    public event EventHandler OnItemDetect;
    public static EventHandler OnToggleInventory;
    [SerializeField] private Animator anim;
    public LayerMask noteMask;
    [Header("Interactable")]
    public LayerMask interactableMasks;
    public float interactableLength;
    public GameObject nearestInteractable;
    [Space(10f)]

    [Header("ItemIndication")]
    public float itemIndicationLength;
    [Space(10f)]
    [SerializeField] private UI_Inventory uiInventory;

    public Inventory inventory;
    private float interactRange = 5f;
    private Transform itemPrompt;
    private ThirdPersonMovement playerMovement;

    private ItemIndicatorScript itemIndi;
    private bool nullChecked = false;
    private float nullTimer = .5f;



    private void Awake()
    {

        inventory = new Inventory(UseItem);
        //inventory = new Inventory();
        //uiInventory.Hide();
        itemPrompt = transform.Find("itemPrompt");
        uiInventory.SetPlayer(this);
        uiInventory.SetInventory(inventory);
        playerMovement = GetComponent<ThirdPersonMovement>();
        Referencer.Instance.colBeh = this;
        //ItemWorld.SpawnItemWorld(new Vector3(20, 0, 20), new ItemREDO { itemType = ItemREDO.ItemType.Ash, amount = 1 });
        //ItemWorld.SpawnItemWorld(new Vector3(-20, 0, 20), new ItemREDO { itemType = ItemREDO.ItemType.Salt, amount = 1 });
        //ItemWorld.SpawnItemWorld(new Vector3(0, 0, -20), new ItemREDO { itemType = ItemREDO.ItemType.Garlic, amount = 1 });
    }
    
    private void Start()
    {
        //interactableTypes = inventory.GetInventoryList();
       
    }

    public void ReplaceInventory(Inventory inv)
    {
        inventory = inv;
    }

    void Update()
    {
        CheckInteractables(); //Gets the nearest item
        CheckItemIndication(); //Puts Item indicators
    }

    private void CheckItemIndication()
    {
        Collider[] indicationColliders = Physics.OverlapSphere(this.transform.position, itemIndicationLength, interactableMasks);
        foreach(Collider i in indicationColliders)
        {
            if(nearestInteractable != i.gameObject)
            {
                InteractableVisualChange(i.gameObject);
            }
        }
        
    }

    private void CheckInteractables()
    {
        Collider[] interactableColliders = Physics.OverlapSphere(this.transform.position, interactableLength, interactableMasks);
        //Debug.Log(interactableColliders.Length);
        if(interactableColliders.Length == 1)
        {
            //Debug.Log("Length 1");
            nearestInteractable = interactableColliders[0].gameObject;
            NearestInteractableVisualChange(nearestInteractable);
        }
        else if(interactableColliders.Length>1)
        {
            //Debug.Log("Length Many");
            GameObject tempNear = interactableColliders[0].gameObject;
            for (int i = 1;i<interactableColliders.Length;i++)
            {
                //Debug.Log(interactableColliders[i].name + " "+CheckDistance(this.transform, interactableColliders[i].transform)+ " < " +CheckDistance(this.transform, tempNear.transform));
                if(CheckDistance(this.transform, interactableColliders[i].transform) < CheckDistance(this.transform, tempNear.transform))
                {
                    tempNear = interactableColliders[i].gameObject;
                }
            }
            nearestInteractable = tempNear.gameObject;
            NearestInteractableVisualChange(nearestInteractable);
        }
        else
        {
            //Debug.Log("Length No");
            //ItemPromtUI.Instance.Hide();
            nearestInteractable = null;
        }
    }
    private void InteractableVisualChange(GameObject item)
    {
        if(item.transform.parent.Find("ItemIndicator")!=null)
        {
            ItemIndicatorScript itemIndi = item.transform.parent.Find("ItemIndicator").GetComponent<ItemIndicatorScript>();
            itemIndi.ItemInRange(this.transform);
            
        }
        else
        {
            if (item.GetComponent<Door>() != null)
            {

            }
            else if (item.GetComponent<HouseScript>() != null)
            {
                //ItemPromtUI.Instance.Show("E : To Knock");
                InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
            }
            else if (item.GetComponent<NoteItem>() != null)
            {
                //ItemPromtUI.Instance.Show("E : To Knock");
                InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
            }
            else if (item.GetComponent<Corpse>() != null)
            {
                if (!item.GetComponent<Corpse>().isBlessed)
                {
                    InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
                }
                else
                {
                    
                }
            }
            else if (item.GetComponent<Gate>() != null)
            {
                if (!item.GetComponent<Gate>().isOpen)
                {
                    InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
                }
            }
            else if (item.GetComponent<Transport>() != null)
            {
                //ItemPromtUI.Instance.Show("E : To Knock");
                InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
            }
            else if (item.GetComponent<ManananggalLowerBody>() != null)
            {
                if (!item.GetComponent<ManananggalLowerBody>().isDead)
                {
                    InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
                }
            }
            else if (item.GetComponent<MangkukulamTotem>() != null)
            {
                if (!item.GetComponent<MangkukulamTotem>().ReturnIsDestroyed())
                {
                    InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
                }
            }
            else
            {
                if(item.transform.parent.GetComponent<Puzzle>()!=null)
                {
                    if (!item.transform.parent.GetComponent<Puzzle>().ReturnIsHolding())
                    {
                        InteractableManager.Instance.IndicatorInRange(this.transform, item.transform);
                    }
                }
            }
        }
    }

    private void NearestInteractableVisualChange(GameObject item)
    {
        if (item.transform.parent.Find("ItemIndicator") != null)
        {
            ItemIndicatorScript itemIndi = item.transform.parent.Find("ItemIndicator").GetComponent<ItemIndicatorScript>();
            itemIndi.ItemIsNearest(this.transform);
            
        }
        else
        {
            if (item.GetComponent<Door>() != null)
            {
                //InteractableManager.Instance.Show("E : To open Door");
            }
            else if (item.GetComponent<Gate>() != null)
            {
                if (!item.GetComponent<Gate>().isOpen)
                {
                    InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
                }
                else
                {
                }
            }
            else if (item.GetComponent<NoteItem>() != null)
            {
                //InteractableManager.Instance.Show("E : To grab Note");
                InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
            }
            else if (item.GetComponent<HouseScript>() != null)
            {
                //ItemPromtUI.Instance.Show("E : To Knock");
                InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
            }
            else if (item.GetComponent<Corpse>() != null && !item.GetComponent<Corpse>().isBlessed)
            {
                if (!item.GetComponent<Corpse>().isBlessed)
                {
                    InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
                }
                else
                {
                    
                }
            }
            else if (item.GetComponent<Transport>() != null)
            {
                //ItemPromtUI.Instance.Show("E : To Knock");
                InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
            }
            else if (item.GetComponent<ManananggalLowerBody>() != null)
            {
                if(!item.GetComponent<ManananggalLowerBody>().isDead)
                {
                    InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
                }
            }
            else if (item.GetComponent<MangkukulamTotem>() != null)
            {
                if (!item.GetComponent<MangkukulamTotem>().ReturnIsDestroyed())
                {
                    InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
                }
            }
            else
            {
                if (item.transform.parent.GetComponent<Puzzle>() != null)
                {
                    if (!item.transform.parent.GetComponent<Puzzle>().ReturnIsHolding())
                    {
                        InteractableManager.Instance.IndicatorIsNearest(this.transform, item.transform);
                    }
                }
            }
            //ItemPromtUI.Instance.Hide();
        }
    }

    private float CheckDistance(Transform object1, Transform object2)
    {
        return Vector3.Distance(object1.position, object2.position);
    }

    public void InteractItem()
    {
        RevampInteract();
    }

    public void RevampInteract()
    {
        if(nearestInteractable != null)
        {
            //Raycast from player to nearestinteractable
            //if(if no object between)
            //{all }
            if (nearestInteractable.GetComponent<ItemWorld>())
            {
                StartCoroutine(PickupAnimation());
                ItemWorld interactable = nearestInteractable.GetComponent<ItemWorld>();
                inventory.AddItem(interactable.GetItem());
                //For Checking if quest needed this item
                ObjectiveEvent.instance.OnCheckItemType(interactable.GetItem().interactable.itemType);
                var interactableType = interactable.GetComponent<InteractableTypeHolder>().interactableType;


                //itemPrompt.gameObject.SetActive(false);
                //ItemPromtUI.Instance.Hide();
                //interactable.DestroySelf();

                Destroy(interactable.gameObject.transform.parent.gameObject);
                //Destroy(interactable.gameObject);
            }
            else if (nearestInteractable.GetComponent<NoteItem>())
            {
                StartCoroutine(PickupAnimation());
                InteractableManager.Instance.RemoveIndicator(this.transform, nearestInteractable.transform);
                NoteItem np = nearestInteractable.GetComponent<NoteItem>();
                np.AddNoteToPlayer(this.gameObject);
            }

            else if (nearestInteractable.GetComponent<Gate>())
            {
                //Debug.Log("GateDetected");
                InteractableManager.Instance.RemoveIndicator(this.transform, nearestInteractable.transform);
                nearestInteractable.transform.GetComponent<Gate>().CheckIfYouHaveKey(GetInventory());
            }

            else if (nearestInteractable.GetComponent<SimpleItem>())
            {
                //inventory.AddItem(nearestInteractable.transform.GetComponent<SimpleItem>().itemToGet);
                InteractableManager.Instance.RemoveIndicator(this.transform, nearestInteractable.transform);
                nearestInteractable.transform.GetComponent<SimpleItem>().GotSimpleItem();
                inventory.AddItem(nearestInteractable.transform.GetComponent<SimpleItem>().ReturnItem());
            }

            else if (nearestInteractable.GetComponent<Ladder>())
            {
                nearestInteractable.transform.GetComponent<Ladder>().InteractLadder(this.gameObject.transform);
            }

            else if (nearestInteractable.GetComponent<Corpse>())
            {
                InteractableManager.Instance.RemoveIndicator(this.transform, nearestInteractable.transform);
                nearestInteractable.transform.GetComponent<Corpse>().BlessTheCorpse(inventory);
            }

            else if (nearestInteractable.GetComponent<LadderInteract>())
            {
                nearestInteractable.transform.GetComponent<LadderInteract>().PlaceLadder();
            }

            else if (nearestInteractable.GetComponent<Door>())
            {
                nearestInteractable.transform.GetComponent<Door>().InteractDoor();
            }
            else if (nearestInteractable.GetComponent<HouseScript>())
            {
                nearestInteractable.transform.GetComponent<HouseScript>().KnockOnDoor();
            }
            else if (nearestInteractable.GetComponent<Transport>())
            {
                nearestInteractable.transform.GetComponent<Transport>().InteractTranport();
            }
            else if (nearestInteractable.GetComponent<ManananggalLowerBody>())
            {
                InteractableManager.Instance.RemoveIndicator(this.transform, nearestInteractable.transform);
                nearestInteractable.transform.GetComponent<ManananggalLowerBody>().InteractLowerBody(inventory);
            }
            else if (nearestInteractable.GetComponent<MangkukulamTotem>())
            {
                InteractableManager.Instance.RemoveIndicator(this.transform, nearestInteractable.transform);
                nearestInteractable.transform.GetComponent<MangkukulamTotem>().InteractTotem();
            }
            else
            {
            }

        }
        else
        {

        }

    }
    

    public void ToggleInventory()
    {
        if (uiInventory.GetIsOpen() == true)
        {
            uiInventory.Hide();
            uiInventory.SetIsOpen(false);
            playerMovement.canMove = true;
            OnToggleInventory?.Invoke(this, EventArgs.Empty);
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (uiInventory.GetIsOpen() == false)
        {
            uiInventory.Show();
            uiInventory.SetIsOpen(true);
            playerMovement.canMove = false;
            OnToggleInventory?.Invoke(this, EventArgs.Empty);
            Time.timeScale = 0.1f;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UseItem(Item item)
    {
        switch (item.interactable.itemType)
        {
            case Item.ItemType.RejuvinatingOil:
                Debug.Log("Healed with oil");
                this.gameObject.GetComponent<HealthScript>().HealUnit(this.gameObject.GetComponent<HealthScript>().maxHealth * .25f);
                inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;

            case Item.ItemType.Ash:
                Debug.Log("Use Ash");
               // inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;

            case Item.ItemType.Garlic:
                Debug.Log("Use Garlic");
                //inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;

            case Item.ItemType.Herb:
                Debug.Log("Use HealthPotion");
                GetComponent<HealthScript>().HealUnit(50);
                inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;
        }
    }


    private void TestHide()
    {
        //ItemPromtUI.Instance.Hide();
    }

    public UI_Inventory ReturnUIInventory()
    {
        return uiInventory;
    }

    public Inventory GetInventory()
    {
        return inventory;
    }

    private IEnumerator PickupAnimation()
    {
        anim.SetBool("HasPickupItem", true);
        yield return new WaitForSeconds(0.5f);
        anim.SetBool("HasPickupItem", false);
    }

}
