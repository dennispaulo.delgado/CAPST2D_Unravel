﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObject/InteractableType")]
public class InteractableTypeSO : ScriptableObject
{
    public string nameString;
    public Item.ItemType itemType;
    [TextAreaAttribute(15,20)]
    public string description;
    public Transform prefab;
    public Sprite sprite2D;
    public bool questItem;
    public bool isStackable;
    [Header("ForBatteryOnly")]
    public float maxBatCharge;
    public float curBatCharge;
    [Header("For Key Only")]
    public int keyID;

}
