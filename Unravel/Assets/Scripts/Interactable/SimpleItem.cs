﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleItem : MonoBehaviour
{
    public Item itemToGet;

    private GameObject parent;

    public UnityEvent OnGotSimpleItem;
    private void Start()
    {
        parent = this.transform.parent.gameObject;   
    }

    public void GotSimpleItem()
    {
        transform.parent.gameObject.GetComponent<ObjectDataScript>().ObjectCollected();
        OnGotSimpleItem.Invoke();
        Destroy(parent);
    }

    public Item ReturnItem()
    {
        return itemToGet;
    }

    public void GotTheTravelHouseKey()
    {
        SaveManager.instance.stageData.playerHasTravelGuideKey = true;
    }

    public void GotJuansHouseKey()
    {
        SaveManager.instance.stageData.playerHasJuanHouseKey = true;
    }

}
