﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/InteractableTypeList")]
public class InteractableTypeListSO : ScriptableObject
{
    public List<InteractableTypeSO> list;
}
