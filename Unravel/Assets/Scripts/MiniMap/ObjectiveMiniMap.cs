﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveMiniMap : MonoBehaviour
{
    public int objID;
    public GameObject miniMapIcon;
    // Start is called before the first frame update
    void Start()
    {
        ObjectiveEvent.instance.addedObjective += StartCoroutine;
        ObjectiveEvent.instance.onQuestArrived += SomeObjectiveCompleted;
        GameEvents.instance.clearListeners += ClearListeners;
        StartCoroutine("DelayedStart");
    }

    public void ClearListeners()
    {
        ObjectiveEvent.instance.addedObjective -= StartCoroutine;
        ObjectiveEvent.instance.onQuestArrived -= SomeObjectiveCompleted;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(1f);
        DisplayMiniMapIcon();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void StartCoroutine()
    {
        StartCoroutine("CheckDisplayMiniMapIcon");
    }

    IEnumerator CheckDisplayMiniMapIcon()
    {
        Debug.Log("Starting Routine");
        yield return new WaitForSeconds(.3f);
        DisplayMiniMapIcon();
    }

    private void SomeObjectiveCompleted(int qID)
    {
        if(qID == objID)
        {
            miniMapIcon.SetActive(false);
        }
    }
    private void OnDestroy()
    {
        ObjectiveEvent.instance.addedObjective -= StartCoroutine;
        ObjectiveEvent.instance.onQuestArrived -= SomeObjectiveCompleted;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    public void StopDisplayMiniMapIcon()
    {
        miniMapIcon.SetActive(false);
    }

    private void DisplayMiniMapIcon()
    {

        for (int i = 0; i < ObjectiveHandler.instance.curObjectives.Count; i++)
        {
            if (ObjectiveHandler.instance.curObjectives[i].objID == objID)
            {
                miniMapIcon.SetActive(true);
                ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.MapUpdated);
                return;
            }
        }
        miniMapIcon.SetActive(false);
    }


}
