﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSoundTrigger : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject chaseScript;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonMovement>())
        {
            chaseScript.GetComponent<ChaseSound>().SetIsActive(true);
        }
    }


    
}
