﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingScritp : MonoBehaviour
{
    public Item item;
    public Item item2;
    public Item item3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Keypad1))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(item);
        }
        if (Input.GetKeyUp(KeyCode.Keypad2))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(item2);
        }
        if (Input.GetKeyUp(KeyCode.Keypad3))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(item3);
        }
        if (Input.GetKeyUp(KeyCode.Keypad4))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.HiddenItem);
        }
        if (Input.GetKeyUp(KeyCode.Keypad5))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.LoreUpdated);
        }
        if (Input.GetKeyUp(KeyCode.Keypad6))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.MonsterDiscovered);
        }
        if (Input.GetKeyUp(KeyCode.Keypad7))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.NewItem);
        }
        if (Input.GetKeyUp(KeyCode.Keypad8))
        {
            ActivityFeed.instance.AddFeedToActivityFeed(JournalFeedType.ObservationGain);
        }


        if (Input.GetKeyUp(KeyCode.PageUp))
        {
            CombatEvents.instance.OnMangkukulamStun();
        }
        if (Input.GetKeyUp(KeyCode.PageDown))
        {
            CombatEvents.instance.OnMangkukulamUnstun();
        }


        if (Input.GetKeyUp(KeyCode.RightControl))
        {
            CombatEvents.instance.MangkukulamCastingRitualProgress(100);
        }
        /*
       if(Input.GetKeyUp(KeyCode.E))
       {
           ObjectiveEvent.instance.OnQuestArrivedS(1);// for checking quest Id
       }
       if (Input.GetKeyUp(KeyCode.Q))
       {
           ObjectiveEvent.instance.OnCheckItemType(Item.ItemType.Salt);
       }

        if (Input.GetKeyUp(KeyCode.Backslash))
        {
            ObjectiveEvent.instance.curObjectives.Add(new Objective(slot2));
        }
        if (Input.GetKeyUp(KeyCode.Backspace))
        {
            ObjectiveEvent.instance.curObjectives.Add(new Objective(slot));
        }
        */
    }
}
