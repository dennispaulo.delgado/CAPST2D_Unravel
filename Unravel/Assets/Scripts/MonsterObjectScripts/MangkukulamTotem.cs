﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MangkukulamTotem : MonoBehaviour
{
    public GameObject protectiveField;
    public GameObject explosionPrefab;
    [Header("3D Properties")]
    public List<GameObject> objectsToDestroy;
    private bool isDestroyed = false;
    private bool isProtected = true;

    void Start()
    {
        CombatEvents.instance.onMangkukulamStunned += DeactivateProtectiveField;
        CombatEvents.instance.onMangkukulamUnstunned += ActivateProtectiveField;
    }

    void Update()
    {
        
    }

    public bool ReturnIsDestroyed()
    {
        return isDestroyed;
    }

    private void DeactivateProtectiveField()
    {
        isProtected = false;
        protectiveField.SetActive(false);
    }
    private void ActivateProtectiveField()
    {
        if (!isDestroyed)
        {
            isProtected = true;
            protectiveField.SetActive(true);
        }
    }

    public bool CheckIfDestroyed()
    {
        return isDestroyed;
    }

    public void InteractTotem()
    {
        if (!isProtected)
        {
            if (!isDestroyed)
            {
                TotemDestroyed();
            }
        }
    }
    
    public void ResetTotem()
    {
        CombatEvents.instance.onMangkukulamStunned += DeactivateProtectiveField;
        CombatEvents.instance.onMangkukulamUnstunned +  = ActivateProtectiveField;
        isDestroyed = false;
        protectiveField.SetActive(true);
        isProtected = true;
        CombatEvents.instance.OnMangkukulamUnstun();
        foreach (GameObject o in objectsToDestroy)
        {
            o.SetActive(true);
        }
    }

    private void TotemDestroyed()
    {
        CombatEvents.instance.onMangkukulamStunned -= DeactivateProtectiveField;
        CombatEvents.instance.onMangkukulamUnstunned -= ActivateProtectiveField;
        isDestroyed = true;
        CombatEvents.instance.OnMangkukulamUnstun();
        CombatEvents.instance.OnTotemDestroyed();
        isProtected = false;
        protectiveField.SetActive(false);
        foreach (GameObject o in objectsToDestroy)
        {
            o.SetActive(false);
        }
        Instantiate(explosionPrefab, this.transform.position, transform.rotation);
    }

    public void CheatTotemDestroy()
    {
        TotemDestroyed();
    }

}
