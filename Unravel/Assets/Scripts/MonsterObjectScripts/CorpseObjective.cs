﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CorpseObjective : MonoBehaviour
{
    public static EventHandler OnObjectiveComplete;

    public UnityEvent onCompletedQuest;

    public List<Corpse> corpses;
    public int questId;
    public bool isComplete = false;

    public ObjectiveSlot nextSlot;
    void Start()
    {
        ObjectiveEvent.instance.evalCorpseBlessed += EvaluateCorpseObjective;
        GameEvents.instance.clearListeners += ClearListeners;
    }

    void Update()
    {
        
    }
    public void EvaluateCorpseObjective()
    {
        float cBlessed = 0;
        for(int c = 0;c<corpses.Count;c++)
        {
            if(corpses[c].isBlessed)
            {
                cBlessed++;
            }
        }
        //Debug.Log("CorpseBlessed : " + cBlessed + " >= " + " Count : " + corpses.Count);
        if (cBlessed >= corpses.Count)
        {
            OnObjectiveComplete?.Invoke(this, EventArgs.Empty);
            CompletedObjective();
        }
    }

    public void ClearListeners()
    {
        ObjectiveEvent.instance.evalCorpseBlessed -= EvaluateCorpseObjective;
        GameEvents.instance.clearListeners -= ClearListeners;
    }
    public void CompletedObjective()
    {
        ObjectiveEvent.instance.OnQuestArrivedS(3332);
        SaveManager.instance.stageData.playerStopAswang = true;
        onCompletedQuest.Invoke();
        ObjectiveEvent.instance.OnQuestArrivedS(questId);
        if (nextSlot != null)
        {
            ObjectiveHandler.instance.AddObjective(nextSlot);
        }
        Debug.Log("CompletedObjective");
        isComplete = true;
    }
}
