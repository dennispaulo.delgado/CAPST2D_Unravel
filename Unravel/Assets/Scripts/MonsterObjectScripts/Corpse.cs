﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour
{
    public GameObject blessedLight;
    [SerializeField] private ParticleSystem visualFeedbackl;
    [SerializeField] private AudioClip clip;

    private AudioSource audioSource;
    public bool isBlessed = false;
    private void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
    }

    public void BlessTheCorpse(Inventory pInv)
    {
        if (!isBlessed)
        {
            for (int i = 0; i < pInv.GetInventoryList().Count; i++)
            {
                if (pInv.GetInventoryList()[i].interactable.itemType == Item.ItemType.TearsOfBathala)
                {
                    isBlessed = true;
                    ObjectiveEvent.instance.EvaluateCorpseBlessed();
                    blessedLight.SetActive(true);
                    CreateVisualFeedback();
                    return;
                }
            }
            Debug.Log("NoTears");
        }
    }

    private void CreateVisualFeedback()
    {
        Debug.Log("VisualFeedbacks");
        audioSource.clip = clip;
        audioSource.PlayOneShot(audioSource.clip);
        GameObject gameObject = Instantiate(visualFeedbackl.gameObject, this.transform.position, visualFeedbackl.gameObject.transform.rotation);
        //StartCoroutine(VisualFeedbackVisible(gameObject));
    }

}
