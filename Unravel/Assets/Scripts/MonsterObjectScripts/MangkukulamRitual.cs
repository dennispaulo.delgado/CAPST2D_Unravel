﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class MangkukulamRitual : MonoBehaviour
{
    public UnityEvent OnRitualDestroyed;
    public bool ritualIsRuined = false;

    public List<MangkukulamTotem> totems;
    private int numberOfTotems;
    // Start is called before the first frame update
    void Start()
    {
        CountTotems();
        CombatEvents.instance.onTotemDestroyed += CheckTotems;
    }


    private void CountTotems()
    {
        numberOfTotems = totems.Count;
    }

    private void CheckTotems()
    {
        int counter = 0;
        foreach(MangkukulamTotem t in totems)
        {
            if(t.CheckIfDestroyed())
            {
                counter++;
            }
        }
        Debug.Log(counter + " >= " + numberOfTotems);
        if(counter >= numberOfTotems)
        {
            RitualIsDestroyed();
        }
    }

    public void ResetRitual()
    {
        foreach(MangkukulamTotem t in totems)
        {
            t.ResetTotem();
        }
    }

    private void RitualIsDestroyed()
    {
        CombatEvents.instance.OnMangkukulamUnstun();
        Debug.Log("RitualIsDestroyed");
        OnRitualDestroyed.Invoke();
        ritualIsRuined = true;
        CombatEvents.instance.onTotemDestroyed -= CheckTotems;
        this.gameObject.GetComponent<ObjectiveMiniMap>().ClearListeners();
    }

    public void ClearTotems()
    {
        foreach(MangkukulamTotem t in totems)
        {
            t.CheatTotemDestroy();
        }
    }

}
