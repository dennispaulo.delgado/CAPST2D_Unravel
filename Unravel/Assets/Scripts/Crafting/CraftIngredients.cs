﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CraftIngredients
{
    public Item.ItemType ingredient;
    public int amount;
}
