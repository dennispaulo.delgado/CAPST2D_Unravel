﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightChecker : MonoBehaviour
{
    [SerializeField] private Puzzle puzzleObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ThirdPersonMovement>())
        {
            puzzleObject.SetRight(true);
        }
    }

    

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<ThirdPersonMovement>())
        {
            puzzleObject.SetRight(false);
            puzzleObject.SetIsHolding(false);
        }
    }
}
