﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{

    [SerializeField] private ParticleSystem visualFeedbackl;
    [SerializeField] private AudioClip soundFeeback;

    private AudioSource audioSource;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Puzzle>())
        {
            if(other.GetComponent<Puzzle>().GetPuzzleType().types == PuzzleType.PuzzleTypes.PressureBox)
            {
                this.gameObject.GetComponent<PuzzleObject>().PuzzleIsCompleted();
                CreateVisualFeedback();

            }
        }
        
    }


    private void CreateVisualFeedback()
    {
        GameObject gameObject = Instantiate(visualFeedbackl.gameObject, this.transform.position, visualFeedbackl.gameObject.transform.rotation);
        audioSource.clip = soundFeeback;
        audioSource.PlayOneShot(audioSource.clip);

        //StartCoroutine(VisualFeedbackVisible(gameObject));

        

        
    }

    //private IEnumerator VisualFeedbackVisible(GameObject gameObject)
    //{
    //    //CreateVisualFeedback();
    //    yield return new WaitForSeconds(5f);
    //    Destroy(gameObject);
    //}


}
