﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceState
{
    public enum State
    {
        Front,
        Right,
        Left,
        Back
    }
}

public class Puzzle : MonoBehaviour
{
    public class OnPuzzleTypeShelfArgs : EventArgs
    {
        public float verticalMovement;
        public float horizontalMovement;
        public float walkMovement;
        public bool canSprint;
    }

    public class OnPuzzleTypePressureArgs : EventArgs
    {
        public float verticalMovement;
        public float horizontalMovement;
        public float walkMovement;
        public bool isPushPull;
        public bool isSliding;
        public bool canSprint;
    }
   

    public static event EventHandler<OnPuzzleTypeShelfArgs> OnPuzzleTypeShelf;
    public static event EventHandler<OnPuzzleTypePressureArgs> OnPuzzleTypePressure;
    
    

    [SerializeField] private EPuzzleLogicSO puzzleType;
    [SerializeField] private bool isHolding = false;
    
    [SerializeField] private PuzzleInteract player;
    private Vector3 puzzleObjectDirection;
    
    private float horizontal;
    private float vertical;
    private float horizontalDrag;
    private float verticalDrag;
    private float interactRange = 1f;
    
    [SerializeField] private bool isFront;
    [SerializeField] private bool isRight;
    [SerializeField] private bool isLeft;
    [SerializeField] private bool isPushPull;
    [SerializeField] private bool isSliding;
    private AudioSource audioSource;
    [SerializeField] private AudioClip pushPullSFX;
    private bool hasPlayed = false;


    private void Awake()
    {
        player = FindObjectOfType<PuzzleInteract>();
        audioSource = GetComponent<AudioSource>();
        puzzleObjectDirection = this.gameObject.transform.position;
        
    }

    private void Start()
    {
        PuzzleInteract.OnPuzzleInteract += PuzzleInteract_OnPuzzleInteract;
        GameEvents.instance.clearListeners += ClearListeners;
    }

    private void PuzzleInteract_OnPuzzleInteract(object sender, PuzzleInteract.OnPuzzleInteractArgs e)
    {
        
        isHolding = CheckInteraction(this.gameObject, e.puzzleObject, e.isHolding);
        Debug.Log(isHolding);
       
    }

    public bool ReturnIsHolding()
    {
        return isHolding;
    }

    public void SetIsHolding(bool value)
    {
        isHolding = value;
    }

    public bool GetIsLeft()
    {
        return isLeft;
    }

    public bool GetIsRight()
    {
        return isRight;
    }

    private void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        



        //Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;


        if (isHolding)
        {
            if(horizontal != 0 || vertical != 0)
            {
                PlaySound();
            }
            else
            {
                hasPlayed = false;
                audioSource.Stop();
            }
            
            if (isPushPull)
            {
                
                isPushPull = false;
            }
            //else
            //{
            //    audioSource.Stop();
            //}

            if (isSliding)
            {
                
                isSliding = false;
            }
            //else
            //{
            //    audioSource.Stop();
            //}
            switch(isLeft || isRight)
            {
                case true:
                    {
                        Debug.Log("isLeft or isRight");
                        break;
                    }
                case false:
                    {
                        if (puzzleType.types == PuzzleType.PuzzleTypes.Shelf)
                        {
                            OnPuzzleTypeShelf?.Invoke(this, new OnPuzzleTypeShelfArgs { walkMovement = 0.3f, verticalMovement = 0, horizontalMovement = 0.3f, canSprint = false });
                            foreach (var puzzleLogic in puzzleType.logicTypesList)
                            {
                                if (puzzleLogic == PuzzleLogicType.LogicTypes.HorizontalDrag)
                                {
                                    switch (isFront)
                                    {
                                        case true:
                                            {
                                                if (Input.GetKey(KeyCode.A))
                                                {
                                                    
                                                    horizontalDrag = 2f;
                                                    transform.Translate(new Vector3(-horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                }

                                                
                                                if (Input.GetKey(KeyCode.D))
                                                {
                                                    
                                                    horizontalDrag = 2f;
                                                    transform.Translate(new Vector3(-horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                }

                                                
                                                break;
                                            }

                                        case false:
                                            {
                                                if (Input.GetKey(KeyCode.A))
                                                {
                                                    horizontalDrag = 2f;
                                                    transform.Translate(new Vector3(horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                }

                                                if (Input.GetKey(KeyCode.D))
                                                {
                                                    horizontalDrag = 2f;
                                                    transform.Translate(new Vector3(horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                }
                                                break;
                                            }
                                    }


                                }
                            }
                        }

                        if (puzzleType.types == PuzzleType.PuzzleTypes.PressureBox)
                        {
                            OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 0.2f, horizontalMovement = 0.2f, verticalMovement = 0.01f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });

                            foreach (var puzzleLogic in puzzleType.logicTypesList)
                            {
                                if (puzzleLogic == PuzzleLogicType.LogicTypes.Pushable)
                                {

                                    if (Input.GetKey(KeyCode.W) && !isSliding)
                                    {
                                        isPushPull = true;
                                        if (isPushPull)
                                        {
                                            OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = .7f, horizontalMovement = 0.0f, verticalMovement = 5f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                        }

                                        switch (isFront)
                                        {
                                            case true:
                                                {
                                                    verticalDrag = 2.5f;
                                                    transform.Translate(new Vector3(0, 0, -vertical).normalized * verticalDrag * Time.deltaTime);
                                                    break;
                                                }
                                            case false:
                                                {
                                                    verticalDrag = 2.5f;
                                                    transform.Translate(new Vector3(0, 0, vertical).normalized * verticalDrag * Time.deltaTime);
                                                    break;
                                                }
                                            
                                        }

                                    }

                                    if(Input.GetKeyUp(KeyCode.W) && isPushPull)
                                    {
                                        isPushPull = false;
                                        if (!isPushPull)
                                        {
                                            OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 1, horizontalMovement = 0.2f, verticalMovement = 0.01f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                        }


                                    }
                                }

                                if (puzzleLogic == PuzzleLogicType.LogicTypes.Pullable)
                                {
                                    if (Input.GetKey(KeyCode.S) && !isSliding)
                                    {
                                        isPushPull = true;
                                        if (isPushPull)
                                        {
                                            OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 0.2f, horizontalMovement = 0.0f, verticalMovement = 5f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                        }
                                        switch (isFront)
                                        {
                                            case true:
                                                {
                                                    verticalDrag = 1.5f;
                                                    transform.Translate(new Vector3(0, 0, -vertical).normalized * verticalDrag * Time.deltaTime);
                                                    break;
                                                }
                                            case false:
                                                {
                                                    verticalDrag = 1.5f;
                                                    transform.Translate(new Vector3(0, 0, vertical).normalized * verticalDrag * Time.deltaTime);
                                                    break;
                                                }
                                        }

                                    }
                                    if(Input.GetKeyUp(KeyCode.S) && isPushPull)
                                    {
                                        isPushPull = false;
                                        if (!isPushPull)
                                        {
                                            OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 1, horizontalMovement = 0.2f, verticalMovement = 0.01f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                        }
                                    }
                                }


                                if (Input.GetKey(KeyCode.A) && !isPushPull)
                                {
                                    isSliding = true;
                                    if (isSliding)
                                    {
                                        OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 0.2f, horizontalMovement = 0.2f, verticalMovement = 0.0f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                    }
                                    switch (isFront)
                                    {
                                        case true:
                                            {
                                                horizontalDrag = .5f;
                                                transform.Translate(new Vector3(-horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                break;
                                            }
                                        case false:
                                            {
                                                horizontalDrag = .5f;
                                                transform.Translate(new Vector3(horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                break;
                                            }
                                    }

                                }

                                if(Input.GetKeyUp(KeyCode.A) && isSliding)
                                {
                                    isSliding = false;
                                    if (!isSliding)
                                    {
                                        OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 0.2f, horizontalMovement = 0.2f, verticalMovement = 3f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                    }
                                }

                                if (Input.GetKey(KeyCode.D) && !isPushPull)
                                {
                                    isSliding = true;
                                    if (!isSliding)
                                    {
                                        OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 0.2f, horizontalMovement = 0.2f, verticalMovement = 0.0f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                    }
                                    switch (isFront)
                                    {
                                        case true:
                                            {
                                                horizontalDrag = .5f;
                                                transform.Translate(new Vector3(-horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                break;
                                            }
                                        case false:
                                            {
                                                horizontalDrag = .5f;
                                                transform.Translate(new Vector3(horizontal, 0, 0).normalized * horizontalDrag * Time.deltaTime);
                                                break;
                                            }
                                    }

                                }
                                if(Input.GetKeyUp(KeyCode.D) && !isPushPull)
                                {
                                    isSliding = false;
                                    if (!isSliding)
                                    {
                                        OnPuzzleTypePressure?.Invoke(this, new OnPuzzleTypePressureArgs { walkMovement = 0.2f, horizontalMovement = 0.2f, verticalMovement = 3f, canSprint = false, isPushPull = isPushPull, isSliding = isSliding });
                                    }
                                }
                            }
                        }
                        break;
                    }
            }



        }
        else
        {
            audioSource.Stop();
        }

        
        
        
        
    }

    public void ClearListeners()
    {
        PuzzleInteract.OnPuzzleInteract -= PuzzleInteract_OnPuzzleInteract;
        GameEvents.instance.clearListeners -= ClearListeners;
    }


    public EPuzzleLogicSO GetPuzzleType()
    {
        return puzzleType;
    }

    public bool CheckInteraction(GameObject thisObject, GameObject interactObject, bool value)
    {
        if (thisObject == interactObject &&  value == true )
            return true;
        else
            return false;
    }


    private void PlaySound()  
    {
        audioSource.clip = pushPullSFX;

        if (!hasPlayed)
        {
            audioSource.Play();
            hasPlayed = true;
        }

    }


   



    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.GetComponent<ThirdPersonMovement>())
    //    {
    //        isFront = true;
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.GetComponent<ThirdPersonMovement>())
    //    {
    //        isFront = false;
    //    }
    //}

    public bool GetFront()
    {
        return isFront;
    }
    
    public void SetFront(bool value)
    {
        isFront = value;
    }

    public bool GetRight()
    {
        return isRight;
    }

    public void SetRight(bool value)
    {
        isRight = value;
    }

    public bool GetLeft()
    {
        return isLeft;
    }

    public void SetLeft(bool value)
    {
        isLeft = value;
    }
   


}
