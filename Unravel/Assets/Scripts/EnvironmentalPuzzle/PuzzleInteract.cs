﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleInteract : MonoBehaviour
{
    public class OnPuzzleInteractArgs : EventArgs
    {
        public GameObject puzzleObject;
        public bool isHolding;
    }

    public class OnReleaseArgs : EventArgs
    {
        public float speed;
        public bool canSprint;
    }


    public static EventHandler<OnPuzzleInteractArgs> OnPuzzleInteract;
    public static EventHandler<OnReleaseArgs> OnRelease;

    [SerializeField] private GameObject puzzleDetected;
    [SerializeField] private float interactableLength = 2f;
    [SerializeField] private LayerMask interactableLayer;
    [SerializeField] private Animator anim;
    
       
    [SerializeField] KeyCode inputKey;



    // Start is called before the first frame update
    void Start()
    {
        
    }

  

    // Update is called once per frame
    void Update()
    {
       
       

        if (Input.GetKeyDown(inputKey))
        {
            puzzleDetected = ClosestPuzzleInteraction();

            
            

           
           
            Debug.Log(puzzleDetected);

        }

        if (Input.GetKeyUp(inputKey))
        {
            puzzleDetected = null;
            anim.SetBool("IsPushPull", false);
            OnPuzzleInteract?.Invoke(this, new OnPuzzleInteractArgs { puzzleObject = puzzleDetected, isHolding = false });
            OnRelease?.Invoke(this, new OnReleaseArgs { speed = 1,canSprint = true }); 
        }

        if(puzzleDetected != null)
        {
            if(!puzzleDetected.GetComponent<Puzzle>().GetIsLeft() || !puzzleDetected.GetComponent<Puzzle>().GetIsRight())
            {
                Debug.Log(!puzzleDetected.GetComponent<Puzzle>().GetIsLeft() || !puzzleDetected.GetComponent<Puzzle>().GetIsRight());
            }
            if (OnPuzzleInteract != null)
            {
                
                 anim.SetBool("IsPushPull", true);
                 OnPuzzleInteract?.Invoke(this, new OnPuzzleInteractArgs { puzzleObject = puzzleDetected, isHolding = true });
                
               
            }

            var distance = Vector3.Distance(puzzleDetected.transform.position, this.gameObject.transform.position);

            if(distance > 5)
            {
                anim.SetBool("IsPushPull", false);
                OnPuzzleInteract?.Invoke(this, new OnPuzzleInteractArgs { puzzleObject = puzzleDetected, isHolding = false });
            }

            Debug.Log("Holding Object");
            
          
        }

        


       

        
    }

    private GameObject ClosestPuzzleInteraction()
    {
        Collider[] puzzleColliders = Physics.OverlapSphere(this.transform.position, interactableLength, interactableLayer);
        if (puzzleColliders.Length != 0)
        {
            GameObject tempNear = puzzleColliders[0].gameObject;
            for (int i = 1; i < puzzleColliders.Length; i++)
            {
               
                if (CheckDistance(this.transform, puzzleColliders[i].transform) < CheckDistance(this.transform, tempNear.transform))
                {
                    tempNear = puzzleColliders[i].gameObject;
                }
            }

            Debug.Log(tempNear);
            return tempNear;
        }
        return null;
       
    }

    private float CheckDistance(Transform object1, Transform object2)
    {
        return Vector3.Distance(object1.position, object2.position);
    }
}
