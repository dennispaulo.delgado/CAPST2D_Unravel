﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "EnviromentalPuzzle" , menuName = "ScriptableObject/EnviromentalPuzzleSO")]
public class EPuzzleLogicSO : ScriptableObject
{
    public PuzzleType.PuzzleTypes types;
    public List<PuzzleLogicType.LogicTypes> logicTypesList;
    public float dragSpeed = .5f;
    public bool isPullable;
    public bool isPushable;
    public bool dragLeft;
    public bool dragRight;
}


[Serializable]
public class PuzzleType
{
    public enum PuzzleTypes
    {
        Shelf,
        PressureBox
    };

}


[Serializable]
public class PuzzleLogicType
{
    public enum LogicTypes
    {
        Pushable,
        Pullable,
        HorizontalDrag
    };
}
