﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPuzzle : MonoBehaviour
{
    public GameObject clearEffect;
    public AudioClip clip;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PuzzleHit()
    {
        audioSource.clip = clip;
        audioSource.PlayOneShot(audioSource.clip);

        Instantiate(clearEffect, this.transform.position, clearEffect.transform.rotation);
        
        this.GetComponent<PuzzleObject>().PuzzleIsCompleted();
        Destroy(this.gameObject);
    }

}
