﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontChecker : MonoBehaviour
{
    [SerializeField] private Puzzle puzzleObject;   

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ThirdPersonMovement>())
        {
            puzzleObject.SetFront(true);
        }
    }

   


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<ThirdPersonMovement>())
        {
            puzzleObject.SetFront(false);
            
        }
    }
}
