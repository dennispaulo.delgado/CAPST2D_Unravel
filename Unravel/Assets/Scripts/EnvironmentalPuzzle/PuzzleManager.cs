﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PuzzleManager : MonoBehaviour
{
    public UnityEvent OnSetPuzzleComplete;
    public UnityEvent OnSavePuzzleComplete;
    private int puzzleCount;
    public List<PuzzleObject> puzzleToComplete;
    public bool isComplete = false;
    private int instanceC = 1;
    private float counter = 2;
    [Header("WhenObjectiveComplete")]
    public int objIDWhenComplete;
    [Header("Datas")]
    private PuzzleData puzData = new PuzzleData();
    void Start()
    {
        GameEvents.instance.onPuzzleComplete += CheckCompletedPuzzles;
        GameEvents.instance.clearListeners += ClearListeners;
        puzzleCount = puzzleToComplete.Count;
        StartCoroutine("DelayedStart");
    }

    public void ClearListeners()
    {
        GameEvents.instance.onPuzzleComplete -= CheckCompletedPuzzles;
        GameEvents.instance.clearListeners -= ClearListeners;
    }

    private void CheckPuzzleData()
    {
        if (CheckIfPuzzleHasData())
        {
        }
        else
        {
            CreateNewPuzzleData();
        }

        if (puzData.hasBeenSolved || isComplete)
        {
            OnSavePuzzleComplete.Invoke();
        }
    }

    private void Update()
    {
        if(counter<=0)
        {
            if(instanceC == 1)
            {
                //CheckPuzzleData();
                instanceC = 0;
            }
        }
        else
        {
            counter -= Time.deltaTime;
        }
    }

    IEnumerator DelayedStart()
    {
        yield return new WaitForSeconds(.3f);
        CheckPuzzleData();
    }
    private void CreateNewPuzzleData()
    {
        puzData = new PuzzleData();
        puzData.pName = this.gameObject.name;
        puzData.hasBeenSolved = false;
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManagerScript.instance.ReturnStageData().stageName)
            {
                SaveManager.instance.playerData.stageData[i].dPuzzle.Add(puzData);
            }
        }
    }

    private bool CheckIfPuzzleHasData()
    {
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManager.GetActiveScene().name)
            {
                for (int o = 0; o < SaveManager.instance.playerData.stageData[i].dPuzzle.Count; o++)
                {
                    if (SaveManager.instance.playerData.stageData[i].dPuzzle[o].pName == this.gameObject.name)
                    {
                        puzData = SaveManager.instance.playerData.stageData[i].dPuzzle[o];
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public void CheckCompletedPuzzles()
    {
        int counter = 0;
        for(int i = 0; i< puzzleToComplete.Count;i++)
        {
            if(puzzleToComplete[i].CheckIfComplete())
            {
                counter++;
            }
        }    
        if(counter == puzzleToComplete.Count)
        {
            PuzzleListCompleted();
        }
    }

    private void PuzzleListCompleted()
    {
        puzData.hasBeenSolved = true;
        SavePuzzleData();
        ObjectiveEvent.instance.OnQuestArrivedS(objIDWhenComplete);
        OnSetPuzzleComplete.Invoke();
        isComplete = true;
    }

    public void SolveBlackMistCheat()
    {
        PuzzleListCompleted();
    }

    private void SavePuzzleData()
    {
        for (int i = 0; i < SaveManager.instance.playerData.stageData.Count; i++)
        {
            if (SaveManager.instance.playerData.stageData[i].stageName == SceneManagerScript.instance.ReturnStageData().stageName)
            {
                for (int o = 0; o < SaveManager.instance.playerData.stageData[i].dPuzzle.Count; o++)
                {
                    if (SaveManager.instance.playerData.stageData[i].dPuzzle[o].pName == this.gameObject.name)
                    {
                        SaveManager.instance.playerData.stageData[i].dPuzzle[o] = puzData;
                    }
                }
            }
        }
    }

}
