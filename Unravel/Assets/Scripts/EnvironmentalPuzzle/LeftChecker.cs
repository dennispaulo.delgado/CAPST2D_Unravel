﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftChecker : MonoBehaviour
{
    [SerializeField] private Puzzle puzzleObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ThirdPersonMovement>())
        {
            puzzleObject.SetLeft(true);
        }
    }

    

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<ThirdPersonMovement>())
        {
            puzzleObject.SetLeft(false);
            puzzleObject.SetIsHolding(false);
        }
    }
}
