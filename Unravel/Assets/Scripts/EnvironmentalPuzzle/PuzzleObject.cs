﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleObject : MonoBehaviour
{
    public bool isComplete = false;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void PuzzleIsCompleted()
    {
        isComplete = true;
        GameEvents.instance.OnCheckPuzzleComplete();
        this.gameObject.GetComponent<ObjectiveMiniMap>().StopDisplayMiniMapIcon();
    }

    public bool CheckIfComplete()
    {
        return isComplete;
    }
}
